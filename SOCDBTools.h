// SOCDBTools.h: interface for the CSOCDBTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCDBTOOLS_H__4F1B9F21_1556_4FF9_8AA9_8071DC66138E__INCLUDED_)
#define AFX_SOCDBTOOLS_H__4F1B9F21_1556_4FF9_8AA9_8071DC66138E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class CSOCDBTools : public CObject  
{
private:
	CString m_strError;
public:
	BOOL AddDSN(CString sDsn, CString sServer, CString sDatabase);
	CString GetErrorString();
	//
	//文件中的SQL语句以GO分割，每个GO必须占单独一行
	//
	CSOCDBTools();
	virtual ~CSOCDBTools();

};

#endif // !defined(AFX_SOCDBTOOLS_H__4F1B9F21_1556_4FF9_8AA9_8071DC66138E__INCLUDED_)
