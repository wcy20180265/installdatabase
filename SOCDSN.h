// SOCDSN.h: interface for the CSOCDSN class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCDSN_H__A1D33E8D_9759_4744_BAB7_8AD1CBC4FEE6__INCLUDED_)
#define AFX_SOCDSN_H__A1D33E8D_9759_4744_BAB7_8AD1CBC4FEE6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "afxdb.h"
class CSOCDSN : public CObject  
{
private:
	CDatabase	m_db;
	CString m_strError;
public:
	BOOL AddDSNToControlPanel(CString sDsn, CString sServer, CString sDatabase);
	CString GetErrorString();

public:
	CDatabase* GetDatabase();
	void CloseDSN();
	BOOL OpenDSN(CString dsn,CString uid,CString pwd);
	CSOCDSN();
	virtual ~CSOCDSN();

};

#endif // !defined(AFX_SOCDSN_H__A1D33E8D_9759_4744_BAB7_8AD1CBC4FEE6__INCLUDED_)
