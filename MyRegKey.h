// MyRegKey.h: interface for the CMyRegKey class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYREGKEY_H__85D77564_DA47_413E_9F04_47F683BBCFFF__INCLUDED_)
#define AFX_MYREGKEY_H__85D77564_DA47_413E_9F04_47F683BBCFFF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//#include "soc2cdef.h"
class  CMyRegKey  
{
protected:
	HKEY m_hKey;
	CString m_sPath;
public:
	CMyRegKey();
	virtual ~CMyRegKey();
private:
	CString FormartLastError(DWORD Error);
public:
	// 定义打开和关闭注册表的成员函数：
	LONG RegOpen(HKEY hKeyRoot,LPCTSTR pszPath);
	void RegClose();

	// 利用函数重载实现对注册表键值（串值，二进制值，DWORD值 ） 的读和写：

	LONG RegRead (LPCTSTR pszKey,DWORD& dwVal);
	LONG RegRead (LPCTSTR pszKey,CString& sVal);
	LONG RegRead (LPCTSTR pszKey,BYTE *pData,DWORD& dwLength);

	LONG RegWrite (LPCTSTR pszKey,DWORD dwVal);
	LONG RegWrite (LPCTSTR pszKey,LPCTSTR pszVal);
	LONG RegWrite (LPCTSTR pszKey,const BYTE *pData,DWORD dwLength);

};

#endif // !defined(AFX_MYREGKEY_H__85D77564_DA47_413E_9F04_47F683BBCFFF__INCLUDED_)
