// SQLFileParser.h: interface for the CSQLFileParser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SQLFILEPARSER_H__C19F8565_8AEF_4BC7_A225_F496C9D78F82__INCLUDED_)
#define AFX_SQLFILEPARSER_H__C19F8565_8AEF_4BC7_A225_F496C9D78F82__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
class CSQLFileParser : public CObject  
{
private:
	CString m_strError;
public:
	CSQLFileParser();
	virtual ~CSQLFileParser();
public:
	CString GetErrorString();
	//文件中的SQL语句以GO分割，每个GO必须占单独一行
	BOOL ParseFileToArray(CString strFile, CStringArray *pArray);

};

#endif // !defined(AFX_SQLFILEPARSER_H__C19F8565_8AEF_4BC7_A225_F496C9D78F82__INCLUDED_)
