// SQLFilesInstaller.h: interface for the CSQLFilesInstaller class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SQLFILESINSTALLER_H__6C1C09F6_8DCC_4077_843F_D5B0A8AAAFBC__INCLUDED_)
#define AFX_SQLFILESINSTALLER_H__6C1C09F6_8DCC_4077_843F_D5B0A8AAAFBC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "afxdb.h"
#include "socdsn.h"
//
#define  SFI_ERRORCONTINUE	1 //执行SQL语句遇到错误继续
#define  SFI_ERRORBREAK		2 

typedef struct _INSDBPARAM
{
	HWND hWndDlg;
//	HWND hWndProgress;
	CSOCDSN* pDSN;
	CString  strInstallPath;

}INSDBPARAM,*PINSDBPARAM;
typedef struct 
{
	HWND hWndDlg;
	HWND hWndDisableClose;
	CSOCDSN* pDSN;
	CString  strInstallPath;

}INSCONTEXT;

class CSQLFilesInstaller : public CObject  
{
private:
	INSCONTEXT* m_pCtx;
	BOOL m_bContinueWhenError;////执行SQL语句遇到错误停止执行还是继续
	void WriteLog(CString sError);
	BOOL ExecuteSQLArray(CStringArray* pAry);
	CDatabase* m_pDB;
	CStringArray m_aryFiles;
	CString m_strLogFile;
public:
	CSQLFilesInstaller(INSCONTEXT* pCtx,BOOL bContinueWhenError=FALSE);
	virtual ~CSQLFilesInstaller();
public:
	void SetLogFile(CString sFile);
	virtual void BeforeExecute(CString& strSql);
	BOOL Install();
	void SetDatabase(CDatabase*  pDB);
	void RemoveAllFiles();
	void AddFile(CString sFile);
};

/*
//导入数据用的
class CSQLDataFilesInstaller : public CSQLFilesInstaller  
{
public:
	CSQLDataFilesInstaller();
	virtual ~CSQLDataFilesInstaller();
	
};

//创建数据库的
class CSQLDatabaseFilesInstaller : public CSQLFilesInstaller  
{
public:
	CSQLDatabaseFilesInstaller();
	virtual ~CSQLDatabaseFilesInstaller();
	
};
//创建表、视图
class CSQLTableFilesInstaller : public CSQLFilesInstaller  
{
public:
	CSQLTableFilesInstaller();
	virtual ~CSQLTableFilesInstaller();

};
*/


#endif // !defined(AFX_SQLFILESINSTALLER_H__6C1C09F6_8DCC_4077_843F_D5B0A8AAAFBC__INCLUDED_)


