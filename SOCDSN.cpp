// SOCDSN.cpp: implementation of the CSOCDSN class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "installdatabase.h"
#include "SOCDSN.h"
#include "odbcinst.h"
#pragma comment(lib,"odbccp32")

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSOCDSN::CSOCDSN()
{

}

CSOCDSN::~CSOCDSN()
{
	CloseDSN();
}

//在控制面板里创建一个ODBC
BOOL CSOCDSN::AddDSNToControlPanel(CString sDsn, CString sServer, CString sDatabase)
{
	char szDesc[1024];
	int mlen;
	sprintf(szDesc,"DSN=%s? SERVER=%s? DATABASE=%s??",sDsn,sServer,sDatabase);
	mlen = strlen(szDesc);
	for(int i=0; i<mlen; i++)
	{
		if (szDesc[i] == '?')
			szDesc[i] = '\0';
	}
	return SQLConfigDataSource(NULL,ODBC_ADD_DSN, _T("SQL Server"),(LPCSTR)szDesc);
	
}
CString CSOCDSN::GetErrorString()
{
	return m_strError;
}

BOOL CSOCDSN::OpenDSN(CString dsn, CString uid, CString pwd)
{
	/////////////////////////////////////
	CString sql,m_sError;

	CloseDSN();
	sql.Format(_T("ODBC;DSN=%s;UID=%s;PWD=%s"),dsn,uid,pwd);
	try
	{
		m_db.SetLoginTimeout(0);
		if(!m_db.Open(sql))
		{
			m_sError=_T("OpenDatabase() failed");
			return FALSE;
		}
		m_db.SetQueryTimeout(0);
	}
	catch(CDBException* e)
	{
		m_sError=e->m_strError;
		e->Delete();
		
		return FALSE;
		
	}
	
	return TRUE;
}

void CSOCDSN::CloseDSN()
{
	if(m_db.IsOpen())
		m_db.Close();
}

CDatabase* CSOCDSN::GetDatabase()
{
	return &m_db;
}
