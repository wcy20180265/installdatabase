// MyRegKey.cpp: implementation of the CMyRegKey class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "CDsp2.h"
#include "MyRegKey.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyRegKey::CMyRegKey()
{
	m_hKey = NULL;
}

CMyRegKey::~CMyRegKey()
{
	RegClose();
}
LONG CMyRegKey::RegOpen(HKEY hKeyRoot,LPCTSTR pszPath)
{

	DWORD dw;
    LONG ret;
	m_sPath = pszPath;

	ret = RegCreateKeyEx(hKeyRoot,pszPath,0L,NULL,REG_OPTION_VOLATILE,KEY_ALL_ACCESS,NULL,&m_hKey,&dw);

    return ret;
}

void CMyRegKey::RegClose()
{
	if(m_hKey)
	{
	RegCloseKey (m_hKey);
	m_hKey = NULL;

	}
}

CString CMyRegKey::FormartLastError(DWORD Error)
{
   LPVOID lpMsgBuf; 
    FormatMessage( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM | 
        FORMAT_MESSAGE_IGNORE_INSERTS, 
        NULL, 
        GetLastError(), 
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language 
        (TCHAR *) &lpMsgBuf, 
        0, 
        NULL 
        ); 
    TCHAR *p=(TCHAR*)lpMsgBuf;
    CString str=p;
    LocalFree( lpMsgBuf ); 
    return str;
}
LONG CMyRegKey::RegWrite(LPCTSTR pszKey,DWORD dwVal)
{
	return RegSetValueEx(m_hKey,pszKey,0L,REG_DWORD,(CONST BYTE *)&dwVal,sizeof(DWORD));
}

LONG CMyRegKey::RegWrite(LPCTSTR pszKey,LPCTSTR pszData)
{

	VERIFY(m_hKey);
	VERIFY(pszKey);
	VERIFY(pszData);
	DWORD dw;
	//dw=wcslen(pszData)+1;
	dw=(wcslen(pszData)+1)*2-1;
	return RegSetValueEx(m_hKey,pszKey,0L,REG_SZ,(CONST BYTE *)pszData,dw);

}

LONG CMyRegKey::RegWrite(LPCTSTR pszKey,const BYTE *pData,DWORD dwLength)
{

	VERIFY(m_hKey);
	VERIFY(pszKey);
	return RegSetValueEx(m_hKey,pszKey,0L,REG_BINARY,pData,dwLength);

}
LONG CMyRegKey::RegRead (LPCTSTR pszKey,DWORD& dwVal)
{
	VERIFY(m_hKey);
	VERIFY(pszKey);

	DWORD dwType;
	DWORD dwSize = sizeof (DWORD);
	DWORD dwDest;

	dwType=REG_DWORD;

	LONG LRet = RegQueryValueEx(m_hKey,pszKey,NULL,&dwType,(BYTE *)&dwDest,&dwSize);

	if(LRet==ERROR_SUCCESS)
		dwVal = dwDest;
	return LRet;

}

LONG CMyRegKey::RegRead (LPCTSTR pszKey,CString& sVal)
{
	VERIFY(m_hKey);
	VERIFY(pszKey);

	DWORD dwType;
	DWORD dwSize;
	wchar_t string[2000];
	string[0]='\0';

	dwSize = 2000;
	dwType=REG_SZ;
	LONG IReturn = RegQueryValueEx(m_hKey,(LPCWSTR)pszKey,NULL,&dwType,(BYTE *)string,&dwSize);
	if(IReturn==ERROR_SUCCESS)
		sVal = string;
	return IReturn;

}

LONG CMyRegKey::RegRead (LPCTSTR pszKey,BYTE * pData,DWORD& dwLen)
{
	VERIFY(m_hKey);
	VERIFY(pszKey);

	DWORD dwType;
	dwType=REG_DWORD;

	return RegQueryValueEx(m_hKey,(LPCWSTR)pszKey,NULL,&dwType,pData,&dwLen);

}
