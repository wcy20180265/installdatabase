// installdatabaseDlg.h : header file
//

#if !defined(AFX_INSTALLDATABASEDLG_H__DA997149_712F_4084_B1D1_F06D912460C7__INCLUDED_)
#define AFX_INSTALLDATABASEDLG_H__DA997149_712F_4084_B1D1_F06D912460C7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "SOCDSN.h"
/////////////////////////////////////////////////////////////////////////////
// CInstalldatabaseDlg dialog
#include "SQLFilesInstaller.h"

class CInstalldatabaseDlg : public CDialog
{
// Construction
private:

public:
	LRESULT OnThreadProgress(WPARAM wParam,LPARAM lParam);
	static UINT CreateDatabaseThread(LPVOID pParam);
	CInstalldatabaseDlg(CWnd* pParent = NULL);	// standard constructor
	CWinThread* m_pThread;
	CSOCDSN		m_dsn;
// Dialog Data
	//{{AFX_DATA(CInstalldatabaseDlg)
	enum { IDD = IDD_INSTALLDATABASE_DIALOG };
	CStatic	m_static1;
	CProgressCtrl	m_progress1;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInstalldatabaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CInstalldatabaseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTALLDATABASEDLG_H__DA997149_712F_4084_B1D1_F06D912460C7__INCLUDED_)
