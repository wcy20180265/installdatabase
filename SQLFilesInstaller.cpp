// SQLFilesInstaller.cpp: implementation of the CSQLFilesInstaller class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "installdatabase.h"
#include "SQLFilesInstaller.h"

#include "SQLFileParser.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSQLFilesInstaller::CSQLFilesInstaller(INSCONTEXT* pCtx,BOOL bContinueWhenError)
{
	m_bContinueWhenError=bContinueWhenError;
	m_pCtx=pCtx;
}

CSQLFilesInstaller::~CSQLFilesInstaller()
{
	RemoveAllFiles();
}






/*

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSQLDataFilesInstaller::CSQLDataFilesInstaller()
{
	
}

CSQLDataFilesInstaller::~CSQLDataFilesInstaller()
{
	
}





//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSQLDatabaseFilesInstaller::CSQLDatabaseFilesInstaller()
{
	
}

CSQLDatabaseFilesInstaller::~CSQLDatabaseFilesInstaller()
{
	
}

//////////////////////////////////////////////////////////////////////
// CSQLTableFilesInstaller Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSQLTableFilesInstaller::CSQLTableFilesInstaller()
{

}

CSQLTableFilesInstaller::~CSQLTableFilesInstaller()
{

}
*/
void CSQLFilesInstaller::AddFile(CString sFile)
{
	m_aryFiles.Add(sFile);
}

void CSQLFilesInstaller::RemoveAllFiles()
{
	m_aryFiles.RemoveAll();
}

void CSQLFilesInstaller::SetDatabase(CDatabase *pDB)
{
	m_pDB=pDB;
}

BOOL CSQLFilesInstaller::Install()
{
	int i;
	CString sFile;
	CStringArray arySql;
	CSQLFileParser parser;
	INSMSGSTRU pm;
	BOOL ret=TRUE;

	pm.flag=3;
	pm.msg="正在执行脚本，禁止退出";
	if(::IsWindow(m_pCtx->hWndDisableClose))
		::SendMessage(m_pCtx->hWndDisableClose,WM_USER_THREAD_PROGRESS,(WPARAM)&pm,NULL);

	for(i=0;i<m_aryFiles.GetSize();i++)
	{
		sFile=m_aryFiles.GetAt(i);

		if(parser.ParseFileToArray(sFile,&arySql))
		{
			if(!ExecuteSQLArray(&arySql))
			{
				arySql.RemoveAll();
				{
					ret == FALSE;
					break;
				}
			}
		}
		else
		{
			WriteLog(parser.GetErrorString());
			arySql.RemoveAll();
			{
				ret == FALSE;
				break;
			}
		}

		arySql.RemoveAll();

	}
	
	pm.flag=2;
	pm.pos=0;
	if(::IsWindow(m_pCtx->hWndDlg))
		::SendMessage(m_pCtx->hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&pm,NULL);
	
	pm.flag=4;
	pm.msg="";
	if(::IsWindow(m_pCtx->hWndDisableClose))
		::SendMessage(m_pCtx->hWndDisableClose,WM_USER_THREAD_PROGRESS,(WPARAM)&pm,NULL);

	return ret;
}

BOOL CSQLFilesInstaller::ExecuteSQLArray(CStringArray *pAry)
{
	CString sql,err;
	int size=pAry->GetSize();
	INSMSGSTRU pm;

	pm.flag=1;
	pm.low=0;
	pm.high=size;
	if(::IsWindow(m_pCtx->hWndDlg))
		::SendMessage(m_pCtx->hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&pm,NULL);

	for(int i=0;i<size;i++)
	{
		pm.flag=2;
		pm.pos=i;
		try
		{
			sql=pAry->GetAt(i);
			if(sql.GetLength()>=3 && sql.Left(3).CompareNoCase(_T("set"))==0)
			{
			}
			else
			{
				BeforeExecute(sql);
				m_pDB->ExecuteSQL(sql);
				if(::IsWindow(m_pCtx->hWndDlg))
					::SendMessage(m_pCtx->hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&pm,NULL);
			}
		}
		catch(CDBException *e)
		{
			err.Format(_T("执行如下语句出错："));
			err+=_T("\r\n");
			err+=sql;
			err+=_T("\r\n");
			WriteLog(err);
			//::AfxMessageBox(err);
			err=e->m_strError;
			err+=_T("\r\n");
			//e->ReportError();
			WriteLog(err);
	//		e->ReportError();
			e->Delete();


			if(!m_bContinueWhenError)
			{
				return FALSE;
			}

		}
	}
	return TRUE;
}



void CSQLFilesInstaller::BeforeExecute(CString& strSql)
{

}

void CSQLFilesInstaller::SetLogFile(CString sFile)
{
	m_strLogFile=sFile;
}
void CSQLFilesInstaller::WriteLog(CString sError)
{
	CFileFind   finder; 
	CStdioFile file;
	try
	{
		m_strLogFile.TrimRight();

		if(m_strLogFile.GetLength()==0)
		{
			m_strLogFile=_T("C:\\CSQLFilesInstaller.txt");
			file.Open(m_strLogFile,CFile::modeCreate|CFile::modeWrite);
		}
		else
		{
			if(finder.FindFile(m_strLogFile))
				file.Open(m_strLogFile,CFile::modeWrite);
			else
				file.Open(m_strLogFile,CFile::modeCreate|CFile::modeWrite);

		}

		file.WriteString(sError);
		file.Close();
	}
	catch(CFileException* e)
	{
		e->Delete();
	}
}