IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'sapdbtest')
	DROP DATABASE [sapdbtest]
GO

CREATE DATABASE [sapdbtest]  ON (NAME = N'sapdbtest_Data', FILENAME = N'D:\tools\Microsoft SQL Server\MSSQL\Data\sapdbtest_Data.MDF' , SIZE = 10, FILEGROWTH = 10%) LOG ON (NAME = N'sapdbtest_Log', FILENAME = N'D:\tools\Microsoft SQL Server\MSSQL\Data\sapdbtest_Log.LDF' , SIZE = 1, FILEGROWTH = 10%)
 COLLATE Chinese_PRC_CI_AS
GO

exec sp_dboption N'sapdbtest', N'autoclose', N'false'
GO

exec sp_dboption N'sapdbtest', N'bulkcopy', N'false'
GO

exec sp_dboption N'sapdbtest', N'trunc. log', N'true'
GO

exec sp_dboption N'sapdbtest', N'torn page detection', N'false'
GO

exec sp_dboption N'sapdbtest', N'read only', N'false'
GO

exec sp_dboption N'sapdbtest', N'dbo use', N'false'
GO

exec sp_dboption N'sapdbtest', N'single', N'false'
GO

exec sp_dboption N'sapdbtest', N'autoshrink', N'true'
GO

exec sp_dboption N'sapdbtest', N'ANSI null default', N'false'
GO

exec sp_dboption N'sapdbtest', N'recursive triggers', N'false'
GO

exec sp_dboption N'sapdbtest', N'ANSI nulls', N'false'
GO

exec sp_dboption N'sapdbtest', N'concat null yields null', N'false'
GO

exec sp_dboption N'sapdbtest', N'cursor close on commit', N'false'
GO

exec sp_dboption N'sapdbtest', N'default to local cursor', N'false'
GO

exec sp_dboption N'sapdbtest', N'quoted identifier', N'false'
GO

exec sp_dboption N'sapdbtest', N'ANSI warnings', N'false'
GO

exec sp_dboption N'sapdbtest', N'auto create statistics', N'true'
GO

exec sp_dboption N'sapdbtest', N'auto update statistics', N'true'
GO

if( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) )
	exec sp_dboption N'sapdbtest', N'db chaining', N'false'
GO

use [sapdb]
GO

if not exists (select * from dbo.sysusers where name = N'bwuser')
	EXEC sp_grantdbaccess N'bwuser'
GO

if not exists (select * from dbo.sysusers where name = N'guest' and hasdbaccess = 1)
	EXEC sp_grantdbaccess N'guest'
GO

if not exists (select * from dbo.sysusers where name = N'System1')
	EXEC sp_grantdbaccess N'System1'
GO

if not exists (select * from dbo.sysusers where name = N'myrole')
	EXEC sp_addrole N'myrole'
GO

exec sp_addrolemember N'myrole', N'System1'
GO

