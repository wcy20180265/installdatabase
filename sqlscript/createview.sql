SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.f_kna1
AS
SELECT mandt, kunnr, name1, name2, land1, regio, cityc, pstlz, telf1, telf2, telfx, telbx, 
      ort01, sortl, vkgrps
FROM dbo.kna1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mard
AS
SELECT mandt, matnr, werks, lgort, labst, umlme, insme, einme, speme, retme, vmlab, 
      vmuml, lfgja, lfmon, ersda, lgpbe, lvorm
FROM dbo.mard


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mpmod_bc
AS
SELECT dbo.mpmod.mandt, dbo.mpmod.modnr, dbo.mpmod.barcode, dbo.mpmod.mtart, 
      dbo.t134.szgrp, dbo.t134.crgrp, dbo.t134.bctyp, dbo.mpmod.stext, dbo.t134.sepor, 
      dbo.mpmod.ltext
FROM dbo.mpmod INNER JOIN
      dbo.t134 ON dbo.mpmod.mandt = dbo.t134.mandt AND 
      dbo.mpmod.mtart = dbo.t134.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_debia
AS
SELECT mandt, pstlz, sortl, mcod1, mcod3, kunnr
FROM dbo.kna1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_kreda
AS
SELECT mandt, lifnr, pstlz, sortl, name1, name2, ort01
FROM dbo.lfa1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1a
AS
SELECT dbo.mara.mandt, dbo.mara.bismt, dbo.makt.maktg, dbo.makt.spras, 
      dbo.mara.matnr
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1c
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.makt.spras, dbo.mara.matnr
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1e
AS
SELECT dbo.mara.mandt, dbo.mara.kunnr, dbo.makt.maktg, dbo.makt.spras, 
      dbo.mara.matnr
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1h
AS
SELECT dbo.mara.mandt, dbo.mara.prdha, dbo.mvke.vkorg, dbo.mvke.vtweg, 
      dbo.mara.matnr, dbo.makt.spras, dbo.makt.maktg
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr INNER JOIN
      dbo.mvke ON dbo.mara.mandt = dbo.mvke.mandt AND 
      dbo.mara.matnr = dbo.mvke.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1j
AS
SELECT dbo.mara.mandt, dbo.mara.attyp, dbo.mara.matkl, dbo.makt.maktg, 
      dbo.makt.spras, dbo.mara.matnr
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1l
AS
SELECT dbo.mara.mandt, dbo.mara.matkl, dbo.makt.maktg, dbo.makt.spras, 
      dbo.mara.matnr
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1m
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.makt.spras, dbo.mara.matnr
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1p
AS
SELECT dbo.mvke.mandt, dbo.mvke.prodh, dbo.mvke.vkorg, dbo.mvke.vtweg, 
      dbo.mvke.matnr, dbo.makt.spras, dbo.makt.maktg
FROM dbo.mvke INNER JOIN
      dbo.makt ON dbo.mvke.mandt = dbo.makt.mandt AND 
      dbo.mvke.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1s
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.makt.spras, dbo.mvke.vkorg, 
      dbo.mvke.vtweg, dbo.makt.matnr
FROM dbo.makt INNER JOIN
      dbo.mvke ON dbo.makt.mandt = dbo.mvke.mandt AND 
      dbo.makt.matnr = dbo.mvke.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1t
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.maktg, dbo.mara.mtart, 
      dbo.makt.spras
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1w
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.makt.spras, dbo.makt.matnr, 
      dbo.marc.werks
FROM dbo.makt INNER JOIN
      dbo.marc ON dbo.makt.mandt = dbo.marc.mandt AND 
      dbo.makt.matnr = dbo.marc.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_matlt
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.spras, dbo.makt.maktg, 
      dbo.mara.mtart, dbo.mara.matkl
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_rl01
AS
SELECT mandt, rolnr, roltx, ernam, erdat, prdef
FROM dbo.rl01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_r
AS
SELECT mandt, bukrs, butxt, ort01, opvar
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_s
AS
SELECT mandt, bukrs, ktopl, ktop2, butxt, ort01
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_x
AS
SELECT mandt, bukrs, butxt, ort01, kkber, xkkbi
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_y
AS
SELECT mandt, bukrs, butxt, ort01, rcomp, dlflg
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_authy
AS
SELECT dbo.authy.autnr, dbo.authy.agpnr, dbo.authy.auttx, dbo.augrp.agptx, 
      dbo.authy.autvl
FROM dbo.augrp INNER JOIN
      dbo.authy ON dbo.augrp.agpnr = dbo.authy.agpnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_kna1
AS
SELECT mandt, kunnr, ktokd, name1, name2, land1, regio, cityc, pstlz, stras, bbsnr, telf1, 
      telf2, telfx, telbx, spras, loevm, erdat, ernam, kukla, anred, idnum, intro, parant, ort01, 
      sortl, vkgrps, sperr, nodel, pfach, pstl2, teltx, telx1
FROM dbo.kna1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lfa1a
AS
SELECT mandt, lifnr, name1
FROM dbo.lfa1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lfb1
AS
SELECT mandt, lifnr, bukrs, erdat, ernam, sperr, loevm, zuawa, akont, begru, vzskz, zwels, 
      xverr, zahls, zterm, eikto, zsabe, kverm, fdgrv, busab, lnrze, lnrzb, zindt, zinrt, datlz, 
      xdezv, webtr, kultg, reprf, togru, hbkid, xpore, qsznr, qszdt, qsskz, blnkz, mindk, altkn, 
      zgrup, mgrup, uzawe, qsrec, qsbgr, qland, xedip, frgrp, togrr, pernr, tlfxs, intad, xlfzb, 
      guzte, gricd, gridt, xausz, cerdt, confs, updat, uptim, nodel
FROM dbo.lfb1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpd1
AS
SELECT mandt, sznmr, sztxt, szgrp
FROM dbo.lpd1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mara_a
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.spras, dbo.mara.matkl, 
      dbo.mara.mtart, dbo.makt.maktx, dbo.makt.maktg
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mara_v
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.mara.mtart, dbo.t134.vmtpo
FROM dbo.mara INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND dbo.mara.mtart = dbo.t134.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mard
AS
SELECT dbo.mard.mandt, dbo.makt.spras, dbo.mard.matnr, dbo.mard.werks, 
      dbo.mard.lgort, dbo.makt.maktx, dbo.t001w.name1, dbo.t001w.name2, 
      dbo.t001l.lgobe, dbo.mard.labst, dbo.mard.umlme, dbo.mard.insme, dbo.mard.einme, 
      dbo.mard.speme, dbo.mard.retme, dbo.mard.vmlab, dbo.mard.vmuml, dbo.mard.lfgja, 
      dbo.mard.lfmon, dbo.mard.ersda, dbo.mard.lgpbe, dbo.mard.lvorm
FROM dbo.mard INNER JOIN
      dbo.mara ON dbo.mard.mandt = dbo.mara.mandt AND 
      dbo.mard.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mbew_a
AS
SELECT mandt, matnr, bwkey, bwtar, lvorm, lbkum, salk3, vprsv, verpr, stprs, peinh, bklas, 
      salkv
FROM dbo.mbew

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_rolau
AS
SELECT dbo.rolau.mandt, dbo.rolau.rolnr, dbo.rolau.autnr, dbo.role.roltx, 
      dbo.authy.auttx
FROM dbo.rolau INNER JOIN
      dbo.role ON dbo.rolau.mandt = dbo.role.mandt AND 
      dbo.rolau.rolnr = dbo.role.rolnr INNER JOIN
      dbo.authy ON dbo.rolau.autnr = dbo.authy.autnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_syscolumns
AS
SELECT name, id, xtype, typestat, xusertype, length, xprec, xscale, colid, xoffset, bitpos, 
      reserved, colstat, cdefault, [domain], number, colorder, autoval, offset, collationid, 
      [language], status, type, usertype, printfmt, prec, scale, iscomputed, isoutparam, 
      isnullable, [collation], tdscollation
FROM dbo.syscolumns

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_sysobjects
AS
SELECT name, xtype
FROM dbo.sysobjects

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001_fm
AS
SELECT mandt, bukrs, butxt, ort01, fikrs
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001k
AS
SELECT dbo.t001k.mandt, dbo.t001k.bwkey, dbo.t001w.name1, dbo.t001k.bukrs, 
      dbo.t001k.bwmod
FROM dbo.t001k INNER JOIN
      dbo.t001w ON dbo.t001k.mandt = dbo.t001w.mandt AND 
      dbo.t001k.bwkey = dbo.t001w.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001k_lk
AS
SELECT dbo.t001w.mandt, dbo.t001w.werks, dbo.t001w.bwkey, dbo.t001k.bukrs, 
      dbo.t001w.name1
FROM dbo.t001w INNER JOIN
      dbo.t001k ON dbo.t001w.mandt = dbo.t001k.mandt AND 
      dbo.t001w.bwkey = dbo.t001k.bwkey

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001l
AS
SELECT mandt, werks, lgort, lgobe, dlflg
FROM dbo.t001l

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001w1
AS
SELECT mandt, werks, name1, name2, stras, pfach, pstlz, ort01, dlflg, bwkey
FROM dbo.t001w

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001w_i
AS
SELECT mandt, werks, name1, iwerk
FROM dbo.t001w

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t002
AS
SELECT dbo.t002.spras, dbo.t002t.sptxt, dbo.t002.laiso, dbo.t002t.sprasl
FROM dbo.t002 INNER JOIN
      dbo.t002t ON dbo.t002.spras = dbo.t002t.sprsl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t005
AS
SELECT dbo.t005.mandt, dbo.t005t.spras, dbo.t005.land1, dbo.t005t.landx, dbo.t005.sprasl, 
      dbo.t005t.natio, dbo.t005t.landx50, dbo.t005t.natio50, dbo.t005.curha, dbo.t005.xdezp, 
      dbo.t005.datfm
FROM dbo.t005 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND dbo.t005.land1 = dbo.t005t.land1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t006
AS
SELECT dbo.t006.mandt, dbo.t006.msehi, dbo.t006a.spras, dbo.t006a.mseht, 
      dbo.t006a.msehl, dbo.t006.dimid
FROM dbo.t006 INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t023
AS
SELECT dbo.t023.mandt, dbo.t023.matkl, dbo.t023t.spras, dbo.t023t.wgbez, 
      dbo.t023t.wgbez60
FROM dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND dbo.t023.matkl = dbo.t023t.matkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t024e
AS
SELECT mandt, ekorg, ekotx, dlflg
FROM dbo.t024e

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tb03
AS
SELECT mandt, sanmr, satxt
FROM dbo.tb03

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vtcurc
AS
SELECT dbo.tcurc.mandt, dbo.tcurc.waers, dbo.tcurt.spras, dbo.tcurc.isocd, dbo.tcurt.ltext, 
      dbo.tcurt.ktext
FROM dbo.tcurc INNER JOIN
      dbo.tcurt ON dbo.tcurc.mandt = dbo.tcurt.mandt AND dbo.tcurc.waers = dbo.tcurt.waers

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkgr
AS
SELECT dbo.tvkgr.mandt, dbo.tvkgr.vkgrp, dbo.tvgrt.spras, dbo.tvgrt.vkgrpx, 
      dbo.tvkgr.dlflg
FROM dbo.tvgrt INNER JOIN
      dbo.tvkgr ON dbo.tvgrt.mandt = dbo.tvkgr.mandt AND dbo.tvgrt.vkgrp = dbo.tvkgr.vkgrp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvko_lk
AS
SELECT mandt, vkorg, bukrs, dlflg, asflg
FROM dbo.tvko

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usr02
AS
SELECT mandt, bname, gltgv, gltgb, aname, erdat, trdat, ltime, bcode
FROM dbo.usr02

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usrau
AS
SELECT dbo.usrol.mandt, dbo.usrol.bname, dbo.authy.autnr, dbo.authy.autvl, 
      dbo.usrol.rolnr, dbo.authy.auttx, dbo.authy.agpnr
FROM dbo.usrol INNER JOIN
      dbo.rolau ON dbo.usrol.mandt = dbo.rolau.mandt AND 
      dbo.usrol.rolnr = dbo.rolau.rolnr INNER JOIN
      dbo.authy ON dbo.rolau.autnr = dbo.authy.autnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usrol
AS
SELECT dbo.usrol.mandt, dbo.usrol.bname, dbo.usrol.rolnr, dbo.role.roltx
FROM dbo.usrol INNER JOIN
      dbo.role ON dbo.usrol.mandt = dbo.role.mandt AND dbo.usrol.rolnr = dbo.role.rolnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usrrol
AS
SELECT dbo.usrrol.mandt, dbo.usrrol.bname, dbo.usrrol.rolnr, dbo.rl01.roltx
FROM dbo.rl01 INNER JOIN
      dbo.usrrol ON dbo.rl01.mandt = dbo.usrrol.mandt AND dbo.rl01.rolnr = dbo.usrrol.rolnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usrroltc
AS
SELECT dbo.usrrol.mandt, dbo.usrrol.bname, dbo.usrrol.rolnr, dbo.rltc.tcode
FROM dbo.usrrol INNER JOIN
      dbo.rltc ON dbo.usrrol.mandt = dbo.rltc.mandt AND dbo.usrrol.rolnr = dbo.rltc.rolnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbap
AS
SELECT mandt, vbeln, posnr, matnr, arktx, waerk, vrkme, meins, netwr, netpr, kpein, kmein, 
      kwmeng, pstyv, kdmat, uepos, matkl, spart, werks, lgort, vkgrp, j_1btxsdc, erdat, 
      ernam, erzet, aedat, mwsbp
FROM dbo.vbap

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbap_ap
AS
SELECT mandt, vbeln, posnr, pstyv, fkrel
FROM dbo.vbap

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbap_l
AS
SELECT mandt, vbeln, posnr, matnr, arktx, kwmeng, vrkme, werks, lgort, charg, kdmat, 
      pstyv, posar, lfrel, fkrel, waerk, netwr, netpr, meins, spart, matkl, erdat, ernam, erzet, 
      aedat
FROM dbo.vbap

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vipkh
AS
SELECT mandt, kunnr, ktokd, name1, sex, stras, pstlz, telf1, telf2, telbx, birthday, rcvsms, 
      rcvemail, vipcard, idnum, sortl, vkgrps, ernam
FROM dbo.kna1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpd31
AS
SELECT dbo.lpd31.mandt, dbo.lpd31.szgrp, dbo.lpd31.sznmr, dbo.lpd3.szgrpx, 
      dbo.lpd1.sztxt
FROM dbo.lpd31 INNER JOIN
      dbo.lpd3 ON dbo.lpd31.mandt = dbo.lpd3.mandt AND 
      dbo.lpd31.szgrp = dbo.lpd3.szgrp INNER JOIN
      dbo.lpd1 ON dbo.lpd31.mandt = dbo.lpd1.mandt AND 
      dbo.lpd31.sznmr = dbo.lpd1.sznmr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.d_mara
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.spras, dbo.mara.mtart, 
      dbo.makt.maktx, dbo.t134t.mtbez
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.makt.spras = dbo.t134t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.h_skb1_pca
AS
SELECT dbo.skb1.mandt, dbo.skb1.bukrs, dbo.skb1.saknr, dbo.skat.spras, dbo.skat.txt20, 
      dbo.skat.txt50, dbo.ska1.xbilk, dbo.skb1.mitkz, dbo.ska1.ktopl, dbo.skb1.xloeb, 
      dbo.skb1.xspeb
FROM dbo.skb1 INNER JOIN
      dbo.skat ON dbo.skb1.mandt = dbo.skat.mandt AND 
      dbo.skb1.saknr = dbo.skat.saknr INNER JOIN
      dbo.ska1 ON dbo.skb1.mandt = dbo.ska1.mandt AND 
      dbo.skat.ktopl = dbo.ska1.ktopl AND dbo.skat.mandt = dbo.ska1.mandt AND 
      dbo.skb1.saknr = dbo.ska1.saknr INNER JOIN
      dbo.t001 ON dbo.ska1.ktopl = dbo.t001.ktopl AND 
      dbo.ska1.mandt = dbo.t001.mandt AND dbo.skb1.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.h_t001k
AS
SELECT dbo.t001k.mandt, dbo.t001k.bwkey, dbo.t001.bukrs, dbo.t001.butxt
FROM dbo.t001k INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.h_t156
AS
SELECT dbo.t156.mandt, dbo.t156.bwart, dbo.t156t.spras, dbo.t156t.btext
FROM dbo.t156 INNER JOIN
      dbo.t156t ON dbo.t156.mandt = dbo.t156t.mandt AND dbo.t156.bwart = dbo.t156t.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_bkpf1
AS
SELECT dbo.bkpf.mandt, dbo.t003t.spras, dbo.bkpf.bukrs, dbo.bkpf.blart, dbo.t003t.ltext, 
      dbo.bkpf.belnr, dbo.bkpf.gjahr, dbo.bkpf.bldat, dbo.bkpf.budat, dbo.bkpf.cpudt, 
      dbo.bkpf.monat, dbo.bkpf.cputm, dbo.bkpf.upddt, dbo.bkpf.aedat, dbo.bkpf.wwert, 
      dbo.bkpf.usnam, dbo.bkpf.tcode, dbo.bkpf.bvorg, dbo.bkpf.xblnr, dbo.bkpf.dbblg, 
      dbo.bkpf.stblg, dbo.bkpf.stjah, dbo.bkpf.bktxt, dbo.bkpf.waers, dbo.bkpf.kursf, 
      dbo.bkpf.kzwrs, dbo.bkpf.kzkrs, dbo.bkpf.bstat, dbo.bkpf.xnetb, dbo.bkpf.frath, 
      dbo.bkpf.xrueb, dbo.bkpf.glvor, dbo.bkpf.grpid, dbo.bkpf.dokid, dbo.bkpf.arcid, 
      dbo.bkpf.iblar, dbo.bkpf.awtyp, dbo.bkpf.awkey, dbo.bkpf.fikrs, dbo.bkpf.hwaer, 
      dbo.bkpf.hwae2, dbo.bkpf.hwae3, dbo.bkpf.kurs2, dbo.bkpf.kurs3, dbo.bkpf.basw2, 
      dbo.bkpf.basw3, dbo.bkpf.umrd2, dbo.bkpf.umrd3, dbo.bkpf.xstov, dbo.bkpf.stodt, 
      dbo.bkpf.xmwst, dbo.bkpf.curt2, dbo.bkpf.curt3, dbo.bkpf.kuty2, dbo.bkpf.kuty3, 
      dbo.bkpf.xsnet, dbo.bkpf.ausbk, dbo.bkpf.xusvr, dbo.bkpf.duefl, dbo.bkpf.awsys, 
      dbo.bkpf.txkrs, dbo.bkpf.lotkz, dbo.bkpf.xwvof, dbo.bkpf.stgrd, dbo.bkpf.ppnam, 
      dbo.bkpf.brnch, dbo.bkpf.numpg, dbo.bkpf.adisc
FROM dbo.bkpf INNER JOIN
      dbo.t003 ON dbo.bkpf.mandt = dbo.t003.mandt AND 
      dbo.bkpf.blart = dbo.t003.blart INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND dbo.t003.blart = dbo.t003t.blart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_city
AS
SELECT dbo.t005g.mandt, dbo.t005h.spras, dbo.t005g.land1, dbo.t005g.regio, 
      dbo.t005g.cityc, dbo.t005t.landx, dbo.t005u.blandx, dbo.t005h.citycx
FROM dbo.t005t INNER JOIN
      dbo.t005 ON dbo.t005t.mandt = dbo.t005.mandt AND 
      dbo.t005t.land1 = dbo.t005.land1 INNER JOIN
      dbo.t005s INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND 
      dbo.t005s.bland = dbo.t005u.bland INNER JOIN
      dbo.t005h INNER JOIN
      dbo.t005g ON dbo.t005h.mandt = dbo.t005g.mandt AND 
      dbo.t005h.land1 = dbo.t005g.land1 AND dbo.t005h.regio = dbo.t005g.regio AND 
      dbo.t005h.cityc = dbo.t005g.cityc ON dbo.t005u.spras = dbo.t005h.spras AND 
      dbo.t005s.mandt = dbo.t005h.mandt AND dbo.t005s.land1 = dbo.t005h.land1 AND 
      dbo.t005s.bland = dbo.t005h.regio ON dbo.t005t.spras = dbo.t005u.spras AND 
      dbo.t005.mandt = dbo.t005h.mandt AND dbo.t005.land1 = dbo.t005h.land1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_ekko
AS
SELECT dbo.ekko.mandt, dbo.ekko.ebeln, dbo.ekko.bukrs, dbo.ekko.bstyp, dbo.ekko.bsart, 
      dbo.ekko.aedat, dbo.ekko.ernam, dbo.ekko.pincr, dbo.ekko.lifnr, dbo.ekko.ekorg, 
      dbo.ekko.ekgrp, dbo.ekko.waers, dbo.lfa1.anred, dbo.lfa1.name1, dbo.lfa1.land1, 
      dbo.lfa1.sortl, dbo.lfa1.regio, dbo.lfa1.cityc, dbo.lfa1.ort01, dbo.lfa1.stras, 
      dbo.lfa1.telbx, dbo.lfa1.telf1, dbo.lfa1.telf2, dbo.lfa1.telfx, dbo.lfa1.teltx, dbo.lfa1.telx1, 
      dbo.ekko.spras, dbo.ekko.zterm
FROM dbo.ekko INNER JOIN
      dbo.lfa1 ON dbo.ekko.mandt = dbo.lfa1.mandt AND dbo.ekko.lifnr = dbo.lfa1.lifnr 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_glt0
AS
SELECT dbo.glt0.mandt, dbo.skat.spras, dbo.glt0.bukrs, dbo.t001.butxt, dbo.glt0.rldnr, 
      dbo.glt0.rrcty, dbo.glt0.rvers, dbo.glt0.ryear, dbo.glt0.racct, dbo.skat.txt20, 
      dbo.glt0.rbusa, dbo.glt0.rtcur, dbo.glt0.drcrk, dbo.glt0.rpmax, dbo.glt0.tslvt, 
      dbo.glt0.tsl01, dbo.glt0.tsl02, dbo.glt0.tsl03, dbo.glt0.tsl04, dbo.glt0.tsl05, 
      dbo.glt0.tsl06, dbo.glt0.tsl07, dbo.glt0.tsl08, dbo.glt0.tsl09, dbo.glt0.tsl10, 
      dbo.glt0.tsl11, dbo.glt0.tsl12, dbo.glt0.tsl13, dbo.glt0.tsl14, dbo.glt0.tsl15, 
      dbo.glt0.tsl16, dbo.glt0.hslvt, dbo.glt0.hsl01, dbo.glt0.hsl02, dbo.glt0.hsl03, 
      dbo.glt0.hsl04, dbo.glt0.hsl05, dbo.glt0.hsl06, dbo.glt0.hsl07, dbo.glt0.hsl08, 
      dbo.glt0.hsl09, dbo.glt0.hsl10, dbo.glt0.hsl11, dbo.glt0.hsl12, dbo.glt0.hsl13, 
      dbo.glt0.hsl14, dbo.glt0.hsl15, dbo.glt0.hsl16, dbo.glt0.cspred, dbo.glt0.kslvt, 
      dbo.glt0.ksl01, dbo.glt0.ksl02, dbo.glt0.ksl03, dbo.glt0.ksl04, dbo.glt0.ksl05, 
      dbo.glt0.ksl06, dbo.glt0.ksl07, dbo.glt0.ksl08, dbo.glt0.ksl09, dbo.glt0.ksl10, 
      dbo.glt0.ksl11, dbo.glt0.ksl12, dbo.glt0.ksl13, dbo.glt0.ksl14, dbo.glt0.ksl15, 
      dbo.glt0.ksl16
FROM dbo.glt0 INNER JOIN
      dbo.skat ON dbo.glt0.mandt = dbo.skat.mandt AND 
      dbo.glt0.racct = dbo.skat.saknr INNER JOIN
      dbo.t001 ON dbo.glt0.mandt = dbo.t001.mandt AND 
      dbo.glt0.bukrs = dbo.t001.bukrs AND dbo.skat.ktopl = dbo.t001.ktopl


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_hz_knc11
AS
SELECT dbo.knc1.mandt, dbo.knc1.bukrs, dbo.knc1.kunnr, dbo.kna1.name1, 
      dbo.t001.waers, dbo.knc1.gjahr, dbo.knc1.erdat, dbo.knc1.usnam, dbo.knc1.um01s, 
      dbo.knc1.um01h, dbo.knc1.um02s, dbo.knc1.um02h, dbo.knc1.um03s, 
      dbo.knc1.um03h, dbo.knc1.um04s, dbo.knc1.um04h, dbo.knc1.um05s, 
      dbo.knc1.um05h, dbo.knc1.um06s, dbo.knc1.um06h, dbo.knc1.um07s, 
      dbo.knc1.um07h, dbo.knc1.um08s, dbo.knc1.um08h, dbo.knc1.um09s, 
      dbo.knc1.um09h, dbo.knc1.um10s, dbo.knc1.um10h, dbo.knc1.um11s, 
      dbo.knc1.um11h, dbo.knc1.um12s, dbo.knc1.um12h, dbo.t001.butxt
FROM dbo.knc1 INNER JOIN
      dbo.t001 ON dbo.knc1.mandt = dbo.t001.mandt INNER JOIN
      dbo.kna1 ON dbo.knc1.mandt = dbo.kna1.mandt AND dbo.knc1.kunnr = dbo.kna1.kunnr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_hz_vbap1
AS
SELECT dbo.vbap.mandt, dbo.vbap.vbeln, dbo.vbap.matnr, dbo.vbap.arktx, 
      dbo.vbap.vrkme, dbo.vbap.meins, dbo.vbap.kwmeng, dbo.vbap.netwr, 
      dbo.vbap.waerk, dbo.vbap.erdat, dbo.vbap.pstyv, dbo.tvap.shkzg
FROM dbo.vbap INNER JOIN
      dbo.tvap ON dbo.vbap.mandt = dbo.tvap.mandt AND dbo.vbap.pstyv = dbo.tvap.pstyv

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_ikpf
AS
SELECT dbo.ikpf.mandt, dbo.ikpf.gjahr, dbo.ikpf.werks, dbo.ikpf.lgort, dbo.t001w.name1, 
      dbo.t001l.lgobe, dbo.ikpf.iblnr, dbo.ikpf.vgart, dbo.ikpf.sobkz, dbo.ikpf.bldat, 
      dbo.ikpf.gidat, dbo.ikpf.zldat, dbo.ikpf.budat, dbo.ikpf.monat, dbo.ikpf.usnam, 
      dbo.ikpf.sperr, dbo.ikpf.zstat, dbo.ikpf.dstat, dbo.ikpf.xblni, dbo.ikpf.lstat, 
      dbo.ikpf.xbufi, dbo.ikpf.keord, dbo.ikpf.ordng, dbo.ikpf.invnu
FROM dbo.ikpf INNER JOIN
      dbo.t001w ON dbo.ikpf.mandt = dbo.t001w.mandt AND 
      dbo.ikpf.werks = dbo.t001w.werks INNER JOIN
      dbo.t001l ON dbo.ikpf.werks = dbo.t001l.werks AND 
      dbo.ikpf.mandt = dbo.t001l.mandt AND dbo.ikpf.lgort = dbo.t001l.lgort

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_kna1_def
AS
SELECT dbo.knvv.mandt, dbo.knvv.kunnr, dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart, 
      dbo.kna1.ktokd, dbo.kna1.name1, dbo.t077d.deflg, dbo.knvv.pltyp, dbo.knb1.bukrs, 
      dbo.knb1.akont, dbo.kna1.anred, dbo.kna1.name2, dbo.kna1.land1, dbo.kna1.regio, 
      dbo.kna1.cityc, dbo.kna1.pstlz, dbo.kna1.stras, dbo.kna1.bbsnr, dbo.kna1.telf1, 
      dbo.kna1.telf2, dbo.kna1.telfx, dbo.kna1.telbx, dbo.kna1.loevm, dbo.kna1.sortl, 
      dbo.knvv.vkbur, dbo.knvv.vkgrp, dbo.knvv.waers, dbo.knvv.klabc, 
      dbo.knvv.ktgrd
FROM dbo.kna1 INNER JOIN
      dbo.t077d ON dbo.kna1.mandt = dbo.t077d.mandt AND 
      dbo.kna1.ktokd = dbo.t077d.ktokd INNER JOIN
      dbo.knvv ON dbo.kna1.mandt = dbo.knvv.mandt AND 
      dbo.kna1.kunnr = dbo.knvv.kunnr INNER JOIN
      dbo.knb1 ON dbo.kna1.mandt = dbo.knb1.mandt AND dbo.kna1.kunnr = dbo.knb1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_kna1_jftyp
AS
SELECT dbo.kna1.mandt, dbo.kna1.kunnr, dbo.kna1.ktokd, dbo.t077dmp.type, 
      dbo.t077dmp.pltyp, dbo.t077dmp.jfnmr
FROM dbo.kna1 INNER JOIN
      dbo.t077dmp ON dbo.kna1.mandt = dbo.t077dmp.mandt AND 
      dbo.kna1.ktokd = dbo.t077dmp.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_knc3mp
AS
SELECT dbo.knc3mp.mandt, dbo.knc3mp.kunnr, dbo.kna1.name1, dbo.knc3mp.knjine, 
      dbo.knc3mp.xfjine, dbo.knc3mp.xfjf, dbo.knc3mp.knrp
FROM dbo.knc3mp INNER JOIN
      dbo.kna1 ON dbo.knc3mp.mandt = dbo.kna1.mandt AND 
      dbo.knc3mp.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_knc3mpp
AS
SELECT dbo.knc3mpp.mandt, dbo.knc3mpp.kunnr, dbo.kna1.name1, dbo.knc3mpp.jf, 
      dbo.knc3mpp.rp, dbo.knc3mpp.resan, dbo.knc3mpp.stext, dbo.knc3mpp.vbeln, 
      dbo.knc3mpp.ernam, dbo.knc3mpp.erdat, dbo.knc3mpp.ertim
FROM dbo.kna1 INNER JOIN
      dbo.knc3mpp ON dbo.kna1.mandt = dbo.knc3mpp.mandt AND 
      dbo.kna1.kunnr = dbo.knc3mpp.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_knvv
AS
SELECT dbo.knvv.mandt, dbo.t005t.spras, dbo.knvv.kunnr, dbo.kna1.name1, 
      dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart, dbo.knvv.vkbur, dbo.knvv.vkgrp, 
      dbo.knvv.waers, dbo.knvv.pltyp, dbo.kna1.land1, dbo.kna1.regio, dbo.kna1.cityc, 
      dbo.kna1.pstlz, dbo.kna1.stras, dbo.kna1.telf1, dbo.kna1.telf2, dbo.kna1.telfx, 
      dbo.kna1.telbx, dbo.kna1.erdat, dbo.kna1.ernam, dbo.t005t.landx, dbo.t005u.blandx, 
      dbo.kna1.ort01
FROM dbo.t005 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND 
      dbo.t005.land1 = dbo.t005t.land1 INNER JOIN
      dbo.knvv INNER JOIN
      dbo.kna1 ON dbo.knvv.mandt = dbo.kna1.mandt AND 
      dbo.knvv.kunnr = dbo.kna1.kunnr ON dbo.t005.mandt = dbo.kna1.mandt AND 
      dbo.t005.land1 = dbo.kna1.land1 INNER JOIN
      dbo.t005u ON dbo.kna1.mandt = dbo.t005u.mandt AND 
      dbo.kna1.land1 = dbo.t005u.land1 AND dbo.kna1.regio = dbo.t005u.bland AND 
      dbo.t005t.spras = dbo.t005u.spras INNER JOIN
      dbo.t005s ON dbo.t005u.mandt = dbo.t005s.mandt AND 
      dbo.t005u.land1 = dbo.t005s.land1 AND dbo.t005u.bland = dbo.t005s.bland

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_knvv_189i
AS
SELECT dbo.knvv.mandt, dbo.t189t.spras, dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart, 
      dbo.knvv.kunnr, dbo.kna1.name1, dbo.lp189i.matnr, dbo.mpmod.stext, dbo.knvv.pltyp, 
      dbo.lp189i.zeile, dbo.lp189i.frdat, dbo.lp189i.todat, dbo.lp189i.mengf, 
      dbo.lp189i.mengt, dbo.lp189i.meinh, dbo.lp189i.peinh, dbo.lp189i.price, 
      dbo.lp189i.waers, dbo.t189t.ptext, dbo.mpmod.xfjf, dbo.mpmod.rpsy
FROM dbo.lp189i INNER JOIN
      dbo.knvv ON dbo.lp189i.mandt = dbo.knvv.mandt AND 
      dbo.lp189i.pltyp = dbo.knvv.pltyp INNER JOIN
      dbo.mpmod ON dbo.lp189i.mandt = dbo.mpmod.mandt INNER JOIN
      dbo.kna1 ON dbo.knvv.mandt = dbo.kna1.mandt AND 
      dbo.knvv.kunnr = dbo.kna1.kunnr INNER JOIN
      dbo.t189 ON dbo.lp189i.mandt = dbo.t189.mandt AND 
      dbo.knvv.pltyp = dbo.t189.pltyp INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND 
      dbo.t189.pltyp = dbo.t189t.pltyp INNER JOIN
      dbo.mara ON dbo.lp189i.mandt = dbo.mara.mandt AND 
      dbo.lp189i.matnr = dbo.mara.matnr AND 
      dbo.mpmod.modnr = dbo.mara.mdnmr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr AND dbo.t189t.spras = dbo.makt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_likp
AS
SELECT dbo.likp.mandt, dbo.likp.vbeln, dbo.likp.lfart, dbo.likp.vkorg, dbo.likp.vkbur, 
      dbo.likp.kunnr, dbo.kna1.name1, dbo.likp.kunag, dbo.likp.waerk, dbo.likp.lfdat, 
      dbo.likp.wadat, dbo.likp.bldat, dbo.likp.ernam, dbo.likp.erzet, dbo.likp.erdat
FROM dbo.likp INNER JOIN
      dbo.kna1 ON dbo.likp.mandt = dbo.kna1.mandt AND dbo.likp.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lp189
AS
SELECT dbo.lp189.mandt, dbo.makt.spras, dbo.lp189.pltyp, dbo.lp189.matnr, 
      dbo.makt.maktx, dbo.t189t.ptext, dbo.lp189.zeile, dbo.lp189.erdat, dbo.lp189.ernam, 
      dbo.mara.meins
FROM dbo.lp189 INNER JOIN
      dbo.mara ON dbo.lp189.mandt = dbo.mara.mandt AND 
      dbo.lp189.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t189 ON dbo.lp189.mandt = dbo.t189.mandt AND 
      dbo.lp189.pltyp = dbo.t189.pltyp INNER JOIN
      dbo.t189t ON dbo.t189.pltyp = dbo.t189t.pltyp AND 
      dbo.t189.mandt = dbo.t189t.mandt AND dbo.makt.spras = dbo.t189t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lp189i
AS
SELECT dbo.lp189i.mandt, dbo.makt.spras, dbo.lp189i.matnr, dbo.makt.maktx, 
      dbo.lp189i.pltyp, dbo.lp189i.zeile, dbo.lp189i.frdat, dbo.lp189i.todat, 
      dbo.lp189i.mengf, dbo.lp189i.mengt, dbo.lp189i.meinh, dbo.lp189i.peinh, 
      dbo.lp189i.price, dbo.lp189i.waers, dbo.lp189i.priot, dbo.lp189i.erdat, 
      dbo.lp189i.ernam, dbo.mara.meins
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lp189i ON dbo.mara.mandt = dbo.lp189i.mandt AND 
      dbo.mara.matnr = dbo.lp189i.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lp23
AS
SELECT dbo.t023.mandt, dbo.t023t.spras, dbo.t023.matkl, dbo.t023t.wgbez, 
      dbo.lp23.rgb
FROM dbo.t023t INNER JOIN
      dbo.t023 ON dbo.t023t.mandt = dbo.t023.mandt AND 
      dbo.t023t.matkl = dbo.t023.matkl INNER JOIN
      dbo.lp23 ON dbo.t023t.mandt = dbo.lp23.mandt AND dbo.t023t.matkl = dbo.lp23.matkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd2
AS
SELECT dbo.lpd2.mandt, dbo.t005t.spras, dbo.lpd2.kunnr, dbo.kna1.land1, dbo.kna1.regio, 
      dbo.t005t.landx, dbo.t005u.blandx, dbo.kna1.name1, dbo.kna1.ort01, dbo.lpd2.gjahr, 
      dbo.lpd2.waers, dbo.lpd2.hsl01, dbo.lpd2.hsl02, dbo.lpd2.hsl03, dbo.lpd2.hsl04, 
      dbo.lpd2.hsl05, dbo.lpd2.hsl06, dbo.lpd2.hsl07, dbo.lpd2.hsl08, dbo.lpd2.hsl09, 
      dbo.lpd2.hsl10, dbo.lpd2.hsl11, dbo.lpd2.hsl12, dbo.lpd2.jf, dbo.lpd2.ljf
FROM dbo.t005s INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND 
      dbo.t005s.bland = dbo.t005u.bland INNER JOIN
      dbo.t005t INNER JOIN
      dbo.t005 ON dbo.t005t.mandt = dbo.t005.mandt AND 
      dbo.t005t.land1 = dbo.t005.land1 INNER JOIN
      dbo.lpd2 INNER JOIN
      dbo.kna1 ON dbo.lpd2.mandt = dbo.kna1.mandt AND 
      dbo.lpd2.kunnr = dbo.kna1.kunnr ON dbo.t005.land1 = dbo.kna1.land1 AND 
      dbo.t005.mandt = dbo.kna1.mandt ON dbo.t005s.mandt = dbo.kna1.mandt AND 
      dbo.t005s.land1 = dbo.kna1.land1 AND dbo.t005s.bland = dbo.kna1.regio AND 
      dbo.t005u.spras = dbo.t005t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd5
AS
SELECT dbo.t134t.mandt, dbo.t134t.spras, dbo.lpd4.clsnr, dbo.t134t.mtart, dbo.lpd4.clstxt, 
      dbo.t134t.mtbez, dbo.lpd4.szgrp, dbo.t134.kkref, dbo.t134.kzvpr, dbo.lpd4.nrflg, 
      dbo.lpd4.sepor, dbo.lpd4.meins, dbo.lpd5.spart, dbo.lpd5.nrcur
FROM dbo.lpd5 INNER JOIN
      dbo.t134t ON dbo.lpd5.mandt = dbo.t134t.mandt AND 
      dbo.lpd5.mtart = dbo.t134t.mtart INNER JOIN
      dbo.lpd4 ON dbo.lpd5.mandt = dbo.lpd4.mandt AND 
      dbo.lpd5.clsnr = dbo.lpd4.clsnr INNER JOIN
      dbo.t134 ON dbo.t134t.mandt = dbo.t134.mandt AND dbo.t134t.mtart = dbo.t134.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd6
AS
SELECT dbo.lpd6.mandt, dbo.lpd6.mdnmr, dbo.lpd6.mtart, dbo.lpd6.mdtxt, dbo.lpd6.meins, 
      dbo.lpd6.ktgrm, dbo.lpd6.mtpos, dbo.lpd6.vprsv, dbo.lpd6.bklas, dbo.lpd6.kkref, 
      dbo.lpd6.spart, dbo.lpd6.xmcng, dbo.lpd6.xchpf, dbo.lpd6.chengben, dbo.lpd6.ernam, 
      dbo.lpd6.erdat, dbo.lpd4.clsnr, dbo.lpd4.clstxt, dbo.lpd4.szgrp, dbo.lpd4.hide, 
      dbo.lpd4.nrflgs, dbo.lpd4.nrflg, dbo.lpd4.sepor, dbo.lpd5.icon
FROM dbo.lpd5 INNER JOIN
      dbo.lpd4 ON dbo.lpd5.mandt = dbo.lpd4.mandt AND 
      dbo.lpd5.clsnr = dbo.lpd4.clsnr INNER JOIN
      dbo.lpd6 ON dbo.lpd5.mandt = dbo.lpd6.mandt AND dbo.lpd5.mtart = dbo.lpd6.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd6c
AS
SELECT dbo.lpd6c.mandt, dbo.t023t.spras, dbo.lpd6c.mdnmr, dbo.lpd6c.matkl, 
      dbo.t023t.wgbez, dbo.lpd6c.chkfg, dbo.lp23.rgb
FROM dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.lpd6c ON dbo.t023.mandt = dbo.lpd6c.mandt AND 
      dbo.t023.matkl = dbo.lpd6c.matkl INNER JOIN
      dbo.lp23 ON dbo.lpd6c.mandt = dbo.lp23.mandt AND dbo.lpd6c.matkl = dbo.lp23.matkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd6p
AS
SELECT dbo.lpd6p.mandt, dbo.t189t.spras, dbo.lpd6p.mdnmr, dbo.lpd6p.pltyp, 
      dbo.t189t.ptext, dbo.lpd6p.price
FROM dbo.lpd6p INNER JOIN
      dbo.t189 ON dbo.lpd6p.mandt = dbo.t189.mandt AND 
      dbo.lpd6p.pltyp = dbo.t189.pltyp INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND dbo.t189.pltyp = dbo.t189t.pltyp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd6s
AS
SELECT dbo.lpd6s.mandt, dbo.lpd6s.mdnmr, dbo.lpd6s.sznmr, dbo.lpd1.sztxt, 
      dbo.lpd6s.chkfg
FROM dbo.lpd6s INNER JOIN
      dbo.lpd1 ON dbo.lpd6s.mandt = dbo.lpd1.mandt AND 
      dbo.lpd6s.sznmr = dbo.lpd1.sznmr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpd6v
AS
SELECT dbo.lpd6v.mandt, dbo.tvtwt.spras, dbo.lpd6v.mdnmr, dbo.lpd6v.vtweg, 
      dbo.tvtwt.vtwegx, dbo.lpd6v.chkfg
FROM dbo.lpd6v INNER JOIN
      dbo.tvtw ON dbo.lpd6v.mandt = dbo.tvtw.mandt AND 
      dbo.lpd6v.vtweg = dbo.tvtw.vtweg INNER JOIN
      dbo.tvtwt ON dbo.tvtw.mandt = dbo.tvtwt.mandt AND dbo.tvtw.vtweg = dbo.tvtwt.vtweg

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpmb1b315
AS
SELECT dbo.mseg.mandt, dbo.makt.spras, dbo.mseg.line_id, dbo.mseg.parent_id, 
      dbo.mseg.mblnr, dbo.mseg.mjahr, dbo.mseg.zeile, dbo.mseg.bwart, dbo.mseg.matnr, 
      dbo.mseg.werks, dbo.mseg.lgort, dbo.mseg.xauto, dbo.mseg.insmk, dbo.mseg.sobkz, 
      dbo.mseg.shkzg, dbo.mseg.sgtxt, dbo.mseg.elikz, dbo.mseg.bprme, 
      dbo.mseg.bpmng, dbo.mseg.erfme, dbo.mseg.erfmg, dbo.mseg.menge, 
      dbo.mseg.bwtar, dbo.mseg.dmbtr, dbo.mseg.waers, dbo.mara.mdnmr, 
      dbo.makt.maktx, dbo.mara.matkl, dbo.t023t.wgbez, dbo.mara.size1, dbo.lpd1.sztxt, 
      dbo.mara.meins, dbo.t134t.mtbez, dbo.mara.mtart, dbo.lp23.rgb, dbo.lpd5.icon
FROM dbo.t134t INNER JOIN
      dbo.t134 ON dbo.t134t.mandt = dbo.t134.mandt AND 
      dbo.t134t.mtart = dbo.t134.mtart INNER JOIN
      dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.t023 ON dbo.mara.mandt = dbo.t023.mandt AND 
      dbo.mara.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.makt.spras = dbo.t023t.spras AND 
      dbo.t023.mandt = dbo.t023t.mandt AND dbo.t023.matkl = dbo.t023t.matkl ON 
      dbo.t134t.spras = dbo.makt.spras AND dbo.t134.mandt = dbo.mara.mandt AND 
      dbo.t134.mtart = dbo.mara.mtart INNER JOIN
      dbo.lp23 ON dbo.t023.mandt = dbo.lp23.mandt AND 
      dbo.t023.matkl = dbo.lp23.matkl INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND 
      dbo.mara.mtart = dbo.lpd5.mtart INNER JOIN
      dbo.mseg ON dbo.mara.mandt = dbo.mseg.mandt AND 
      dbo.mara.matnr = dbo.mseg.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpr1
AS
SELECT dbo.vbak.mandt, dbo.tvakt.spras, dbo.vbak.vbeln, dbo.vbak.auart, dbo.tvakt.auartx, 
      dbo.vbak.vkorg, dbo.vbak.vtweg, dbo.vbak.spart, dbo.vbak.vkbur, dbo.vbak.vkgrp, 
      dbo.vbak.kunnr, dbo.kna1.name1, dbo.vbak.bstnk, dbo.vbak.bsark, dbo.vbak.bstdk, 
      dbo.vbak.waerk, dbo.vbak.autlf, dbo.vbak.vdatu, dbo.vbak.bname, dbo.vbak.telf1, 
      dbo.vbak.netwr, dbo.vbak.audat, dbo.vbak.lifsk, dbo.vbak.faksk, dbo.vbak.kokrs, 
      dbo.vbak.cmwae, dbo.vbak.vbtyp, dbo.vbak.augru, dbo.vbak.kurst, dbo.vbak.kkber, 
      dbo.vbak.lfstk, dbo.vbak.lfgsk, dbo.vbak.erzet, dbo.vbak.erdat, dbo.vbak.ernam, 
      dbo.vbak.aedat, dbo.vbak.vgbel, dbo.vbak.vgtyp, dbo.tvkot.vkorgx, dbo.tvtwt.vtwegx, 
      dbo.tspat.spartx
FROM dbo.vbak INNER JOIN
      dbo.kna1 ON dbo.vbak.mandt = dbo.kna1.mandt AND 
      dbo.vbak.kunnr = dbo.kna1.kunnr INNER JOIN
      dbo.tvak ON dbo.vbak.mandt = dbo.tvak.mandt AND 
      dbo.vbak.auart = dbo.tvak.auart INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvko ON dbo.vbak.mandt = dbo.tvko.mandt AND 
      dbo.vbak.vkorg = dbo.tvko.vkorg INNER JOIN
      dbo.tvkot ON dbo.tvko.mandt = dbo.tvkot.mandt AND 
      dbo.tvko.vkorg = dbo.tvkot.vkorg AND dbo.tvakt.spras = dbo.tvkot.spras INNER JOIN
      dbo.tvtw ON dbo.vbak.mandt = dbo.tvtw.mandt AND 
      dbo.vbak.vtweg = dbo.tvtw.vtweg INNER JOIN
      dbo.tvtwt ON dbo.tvtw.mandt = dbo.tvtwt.mandt AND 
      dbo.tvtw.vtweg = dbo.tvtwt.vtweg AND dbo.tvkot.spras = dbo.tvtwt.spras INNER JOIN
      dbo.tspa ON dbo.vbak.mandt = dbo.tspa.mandt AND 
      dbo.vbak.spart = dbo.tspa.spart INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND 
      dbo.tspa.spart = dbo.tspat.spart AND dbo.tvkot.spras = dbo.tspat.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpr2
AS
SELECT dbo.likp.mandt, dbo.likp.vbeln, dbo.likp.lfart, dbo.tvlkt.spras, dbo.tvlkt.vtext, 
      dbo.likp.vkorg, dbo.tvkot.vkorgx, dbo.likp.vkbur, dbo.likp.kunnr, dbo.kna1.name1, 
      dbo.likp.waerk, dbo.likp.bldat, dbo.likp.wadat, dbo.likp.ernam, dbo.likp.lfdat, 
      dbo.likp.erzet, dbo.likp.erdat, dbo.likp.vbtyp, dbo.likp.vstel
FROM dbo.likp INNER JOIN
      dbo.tvlk ON dbo.likp.mandt = dbo.tvlk.mandt AND 
      dbo.likp.lfart = dbo.tvlk.lfart INNER JOIN
      dbo.tvlkt ON dbo.tvlk.mandt = dbo.tvlkt.mandt AND 
      dbo.tvlk.lfart = dbo.tvlkt.lfart INNER JOIN
      dbo.tvko ON dbo.likp.mandt = dbo.tvko.mandt AND 
      dbo.likp.vkorg = dbo.tvko.vkorg INNER JOIN
      dbo.tvkot ON dbo.tvko.mandt = dbo.tvkot.mandt AND 
      dbo.tvko.vkorg = dbo.tvkot.vkorg AND dbo.tvlkt.spras = dbo.tvkot.spras INNER JOIN
      dbo.kna1 ON dbo.likp.mandt = dbo.kna1.mandt AND dbo.likp.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpr3
AS
SELECT dbo.mkpf.mandt, dbo.t003t.spras, dbo.mkpf.mblnr, dbo.mkpf.mjahr, dbo.mkpf.blart, 
      dbo.t003t.ltext, dbo.mkpf.vgart, dbo.mkpf.blaum, dbo.mkpf.bldat, dbo.mkpf.budat, 
      dbo.mkpf.cpudt, dbo.mkpf.cputm, dbo.mkpf.aedat, dbo.mkpf.usnam, dbo.mkpf.tcode, 
      dbo.mkpf.xblnr, dbo.mkpf.bktxt, dbo.mkpf.frath, dbo.mkpf.wever, dbo.mkpf.frbnr, 
      dbo.mkpf.xabln, dbo.mkpf.bla2d, dbo.mkpf.awsys, dbo.mkpf.tcode2, dbo.mkpf.bfwms, 
      dbo.mkpf.exnum, dbo.t003.xkoam
FROM dbo.mkpf INNER JOIN
      dbo.t003 ON dbo.mkpf.mandt = dbo.t003.mandt AND 
      dbo.mkpf.blart = dbo.t003.blart INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND dbo.t003.blart = dbo.t003t.blart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpr4
AS
SELECT dbo.vbrk.mandt, dbo.tvfkt.spras, dbo.vbrk.fkart, dbo.tvfkt.vtext, dbo.vbrk.vbeln, 
      dbo.vbrk.fktyp, dbo.vbrk.vbtyp, dbo.vbrk.waerk, dbo.vbrk.vkorg, dbo.vbrk.vtweg, 
      dbo.vbrk.fkdat, dbo.vbrk.belnr, dbo.vbrk.gjahr, dbo.vbrk.pltyp, dbo.vbrk.bukrs, 
      dbo.vbrk.ernam, dbo.vbrk.erzet, dbo.vbrk.erdat, dbo.vbrk.kunrg, dbo.vbrk.kunag, 
      dbo.vbrk.spart, dbo.kna1.name1, dbo.tvkot.vkorgx, dbo.tvtwt.vtwegx, 
      dbo.tspat.spartx
FROM dbo.vbrk INNER JOIN
      dbo.tvfk ON dbo.vbrk.mandt = dbo.tvfk.mandt AND 
      dbo.vbrk.fkart = dbo.tvfk.fkart INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND 
      dbo.tvfk.fkart = dbo.tvfkt.fkart INNER JOIN
      dbo.kna1 ON dbo.vbrk.mandt = dbo.kna1.mandt AND 
      dbo.vbrk.kunrg = dbo.kna1.kunnr INNER JOIN
      dbo.tvko ON dbo.vbrk.mandt = dbo.tvko.mandt AND 
      dbo.vbrk.vkorg = dbo.tvko.vkorg INNER JOIN
      dbo.tvkot ON dbo.tvko.mandt = dbo.tvkot.mandt AND 
      dbo.tvko.vkorg = dbo.tvkot.vkorg AND dbo.tvfkt.spras = dbo.tvkot.spras INNER JOIN
      dbo.tvtw ON dbo.vbrk.mandt = dbo.tvtw.mandt AND 
      dbo.vbrk.vtweg = dbo.tvtw.vtweg INNER JOIN
      dbo.tvtwt ON dbo.tvtw.mandt = dbo.tvtwt.mandt AND 
      dbo.tvtw.vtweg = dbo.tvtwt.vtweg AND dbo.tvkot.spras = dbo.tvtwt.spras INNER JOIN
      dbo.tspa ON dbo.vbrk.mandt = dbo.tspa.mandt AND 
      dbo.vbrk.spart = dbo.tspa.spart INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND 
      dbo.tvtwt.spras = dbo.tspat.spras AND dbo.tspa.spart = dbo.tspat.spart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_lpr5
AS
SELECT dbo.vbap.mandt, dbo.t006a.spras, dbo.vbap.vbeln, dbo.vbap.posnr, 
      dbo.vbap.pstyv, dbo.vbap.vgtyp, dbo.vbap.matnr, dbo.vbap.arktx, dbo.vbap.kwmeng, 
      dbo.vbap.vrkme, dbo.t006a.mseht, dbo.vbap.meins, dbo.vbap.umzin, 
      dbo.vbap.umziz, dbo.vbap.netpr, dbo.vbap.netwr, dbo.vbap.waerk, dbo.vbap.kpein, 
      dbo.vbap.kmein, dbo.vbap.matkl, dbo.vbap.charg, dbo.vbap.spart, dbo.vbap.werks, 
      dbo.vbap.lgort, dbo.vbap.vkgrp, dbo.vbap.lfrel, dbo.vbap.fkrel, dbo.vbap.kzvbr, 
      dbo.vbap.kdmat, dbo.vbap.j_1btxsdc, dbo.vbap.mwsbp, dbo.vbap.erdat, 
      dbo.vbap.ernam, dbo.vbap.erzet, dbo.vbap.aedat, dbo.vbap.lfsta, dbo.vbap.lfgsa, 
      dbo.vbap.sktof, dbo.vbap.lsmeng1, dbo.vbap.klmeng1, dbo.vbap.lsmeng, 
      dbo.vbap.smeng, dbo.vbap.klmeng, dbo.vbap.vkgrp2, dbo.vbap.koupd, 
      dbo.vbap.smeng1, dbo.vbap.zmeng, dbo.vbap.gsber, dbo.vbap.shkzg, 
      dbo.vbap.bwtar, dbo.vbap.xchpf, dbo.vbap.xchar, dbo.vbap.lfmng, 
      dbo.vbap.sobkz
FROM dbo.vbap INNER JOIN
      dbo.t006 ON dbo.vbap.mandt = dbo.t006.mandt AND 
      dbo.vbap.vrkme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lprm01
AS
SELECT dbo.mbew.mandt, dbo.makt.spras, dbo.mbew.matnr, dbo.makt.maktx, 
      dbo.t134t.mtbez, dbo.t023t.wgbez, dbo.mbew.bwkey, dbo.mbew.bwtar, 
      dbo.mbew.lbkum, dbo.mbew.salk3, dbo.mbew.vprsv, dbo.mbew.verpr, 
      dbo.mbew.peinh, dbo.mbew.stprs, dbo.mbew.bklas, dbo.mbew.salkv, 
      dbo.mbew.vmkum, dbo.mbew.vmsal, dbo.mbew.vmvpr, dbo.mbew.vmver, 
      dbo.mbew.vmstp, dbo.mbew.vmpei, dbo.mbew.vmbkl, dbo.mbew.vmsav, 
      dbo.mbew.vjkum, dbo.mbew.vjsal, dbo.mbew.vjvpr, dbo.mbew.vjver, dbo.mbew.vjstp, 
      dbo.mbew.vjpei, dbo.mbew.vjbkl, dbo.mbew.vjsav, dbo.mbew.lfgja, dbo.mbew.lfmon, 
      dbo.mbew.bwtty, dbo.mbew.stprv, dbo.mbew.laepr, dbo.mbew.zkprs, dbo.mbew.zkdat, 
      dbo.mbew.bwprs, dbo.mbew.bwprh, dbo.mbew.vjbws, dbo.mbew.vjbwh, 
      dbo.mbew.vvjsl, dbo.mbew.vvjlb, dbo.mbew.vvmlb, dbo.mbew.vvsal, 
      dbo.mbew.zplpr, dbo.mbew.zplp1, dbo.mbew.zplp2, dbo.mbew.zplp3, 
      dbo.mbew.zpld1, dbo.mbew.zpld2, dbo.mbew.zpld3, dbo.mbew.pperz, 
      dbo.mbew.pperl, dbo.mbew.pperv, dbo.mbew.kalkz, dbo.mbew.kalkl, dbo.mbew.kalkv, 
      dbo.mbew.kalsc, dbo.mbew.xlifo, dbo.mbew.mypol, dbo.mbew.bwph1, 
      dbo.mbew.bwps1, dbo.mbew.abwkz, dbo.mbew.pstat, dbo.mbew.kaln1, 
      dbo.mbew.kalnr, dbo.mbew.bwva1, dbo.mbew.bwva2, dbo.mbew.bwva3, 
      dbo.mbew.vers1, dbo.mbew.vers2, dbo.mbew.vers3, dbo.mbew.hrkft, 
      dbo.mbew.kosgr, dbo.mbew.pprdz, dbo.mbew.pprdl, dbo.mbew.pprdv, 
      dbo.mbew.pdatz, dbo.mbew.pdatl, dbo.mbew.pdatv, dbo.mbew.ekalr, dbo.mbew.vplpr, 
      dbo.mbew.mlmaa, dbo.mbew.mlast, dbo.mbew.lplpr, dbo.mbew.vksal, 
      dbo.mbew.hkmat, dbo.mbew.sperw, dbo.mbew.kziwl, dbo.mbew.wlinl, 
      dbo.mbew.abciw, dbo.mbew.bwspa, dbo.mbew.lplpx, dbo.mbew.vplpx, 
      dbo.mbew.fplpx, dbo.mbew.lbwst, dbo.mbew.vbwst, dbo.mbew.fbwst, 
      dbo.mbew.eklas, dbo.mbew.qklas, dbo.mbew.mtuse, dbo.mbew.mtorg, 
      dbo.mbew.ownpr, dbo.mbew.xbewm, dbo.mbew.bwpei, dbo.mbew.mbrue, 
      dbo.mara.mtart, dbo.mara.matkl, dbo.mara.meins, dbo.mara.spart, dbo.t006a.mseht, 
      dbo.t001.butxt, dbo.t001.bukrs, dbo.t001.waers
FROM dbo.t001 INNER JOIN
      dbo.t001k ON dbo.t001.mandt = dbo.t001k.mandt AND 
      dbo.t001.bukrs = dbo.t001k.bukrs INNER JOIN
      dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mbew INNER JOIN
      dbo.mara ON dbo.mbew.mandt = dbo.mara.mandt AND 
      dbo.mbew.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.makt.spras = dbo.t134t.spras ON 
      dbo.t023t.spras = dbo.makt.spras AND dbo.t023.matkl = dbo.mara.matkl AND 
      dbo.t023.mandt = dbo.mara.mandt INNER JOIN
      dbo.t006 ON dbo.mara.mandt = dbo.t006.mandt AND 
      dbo.mara.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND dbo.t023t.spras = dbo.t006a.spras ON 
      dbo.t001k.mandt = dbo.mbew.mandt AND dbo.t001k.bwkey = dbo.mbew.bwkey

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lprm02
AS
SELECT dbo.mard.mandt, dbo.makt.spras, dbo.mard.matnr, dbo.mard.werks, 
      dbo.makt.maktx, dbo.makt.maktg, dbo.t134t.mtbez, dbo.t023t.wgbez, dbo.mard.lgort, 
      dbo.mard.labst, dbo.t001w.name1, dbo.t001l.lgobe, dbo.mard.umlme, 
      dbo.mard.insme, dbo.mard.einme, dbo.mard.speme, dbo.mard.retme, 
      dbo.mard.vmlab, dbo.mard.vmuml, dbo.mard.lfgja, dbo.mard.lfmon, dbo.mard.ersda, 
      dbo.mard.lgpbe, dbo.mard.lvorm, dbo.mard.pstat, dbo.mard.sperr, dbo.mard.vmins, 
      dbo.mard.vmein, dbo.mard.vmspe, dbo.mard.vmret, dbo.mard.kzill, dbo.mard.kzilq, 
      dbo.mard.kzile, dbo.mard.kzils, dbo.mard.kzvll, dbo.mard.kzvlq, dbo.mard.kzvle, 
      dbo.mard.kzvls, dbo.mard.diskz, dbo.mard.lsobs, dbo.mard.lminb, dbo.mard.lbstf, 
      dbo.mard.herkl, dbo.mard.exppg, dbo.mard.exver, dbo.mard.klabs, dbo.mard.kinsm, 
      dbo.mard.keinm, dbo.mard.kspem, dbo.mard.dlinl, dbo.mard.prctl, dbo.mard.vklab, 
      dbo.mard.vkuml, dbo.mard.lwmkb, dbo.mard.bskrf, dbo.mard.mdrue, dbo.mard.mdjin, 
      dbo.mara.mtart, dbo.mara.matkl, dbo.mara.meins, dbo.mara.spart, dbo.mara.mfrnr, 
      dbo.mara.mfrpn, dbo.mara.bismt, dbo.t006a.mseht
FROM dbo.t006a INNER JOIN
      dbo.t006 ON dbo.t006a.mandt = dbo.t006.mandt AND 
      dbo.t006a.msehi = dbo.t006.msehi INNER JOIN
      dbo.t023t INNER JOIN
      dbo.t023 ON dbo.t023t.mandt = dbo.t023.mandt AND 
      dbo.t023t.matkl = dbo.t023.matkl INNER JOIN
      dbo.t134t INNER JOIN
      dbo.t134 ON dbo.t134t.mandt = dbo.t134.mandt AND 
      dbo.t134t.mtart = dbo.t134.mtart INNER JOIN
      dbo.mard INNER JOIN
      dbo.mara ON dbo.mard.mandt = dbo.mara.mandt AND 
      dbo.mard.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr ON dbo.t134t.spras = dbo.makt.spras AND 
      dbo.t134.mandt = dbo.mara.mandt AND dbo.t134.mtart = dbo.mara.mtart ON 
      dbo.t023t.spras = dbo.makt.spras AND dbo.t023.mandt = dbo.mara.mandt AND 
      dbo.t023.matkl = dbo.mara.matkl INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort ON 
      dbo.t006.mandt = dbo.mara.mandt AND dbo.t006.msehi = dbo.mara.meins AND 
      dbo.t006a.spras = dbo.t134t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lprp01
AS
SELECT dbo.ekko.mandt, dbo.t161t.spras, dbo.ekko.bstyp, dbo.ekko.bsart, dbo.t161t.batxt, 
      dbo.ekko.ebeln, dbo.ekko.bukrs, dbo.ekko.aedat, dbo.ekko.ernam, dbo.ekko.lifnr, 
      dbo.ekko.zterm, dbo.ekko.ekorg, dbo.ekko.ekgrp, dbo.ekko.waers, dbo.lfa1.name1, 
      dbo.t024e.ekotx, dbo.t001.butxt, dbo.lfa1.land1, dbo.t005t.landx
FROM dbo.t005t INNER JOIN
      dbo.t005 ON dbo.t005t.mandt = dbo.t005.mandt AND 
      dbo.t005t.land1 = dbo.t005.land1 INNER JOIN
      dbo.ekko INNER JOIN
      dbo.t161 ON dbo.ekko.mandt = dbo.t161.mandt AND 
      dbo.ekko.bstyp = dbo.t161.bstyp AND dbo.ekko.bsart = dbo.t161.bsart INNER JOIN
      dbo.t161t ON dbo.t161.mandt = dbo.t161t.mandt AND 
      dbo.t161.bstyp = dbo.t161t.bstyp AND dbo.t161.bsart = dbo.t161t.bsart INNER JOIN
      dbo.lfa1 ON dbo.ekko.mandt = dbo.lfa1.mandt AND 
      dbo.ekko.lifnr = dbo.lfa1.lifnr INNER JOIN
      dbo.t024e ON dbo.ekko.mandt = dbo.t024e.mandt AND 
      dbo.ekko.ekorg = dbo.t024e.ekorg INNER JOIN
      dbo.t001 ON dbo.ekko.mandt = dbo.t001.mandt AND 
      dbo.ekko.bukrs = dbo.t001.bukrs ON dbo.t005.mandt = dbo.lfa1.mandt AND 
      dbo.t005.land1 = dbo.lfa1.land1 AND dbo.t005t.spras = dbo.t161t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lptcob
AS
SELECT dbo.tstct.spras, dbo.lptcob.tcode, dbo.lptcob.objct, dbo.tstct.ttext, 
      dbo.tobjt.ttext AS otext
FROM dbo.lptcob INNER JOIN
      dbo.tobj ON dbo.lptcob.objct = dbo.tobj.objct INNER JOIN
      dbo.tstc ON dbo.lptcob.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode INNER JOIN
      dbo.tobjt ON dbo.tobj.objct = dbo.tobjt.object AND dbo.tstct.spras = dbo.tobjt.langu

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_lpvb01
AS
SELECT dbo.vbap.mandt, dbo.tvkmt.spras, dbo.vbap.vbeln, dbo.vbap.posnr, 
      dbo.vbap.pstyv, dbo.vbap.vgtyp, dbo.vbap.ktgrm, dbo.tvkmt.vtext, dbo.vbap.erdat, 
      dbo.vbap.shkzg, dbo.vbap.matnr, dbo.vbap.arktx, dbo.vbap.vrkme, dbo.vbap.meins, 
      dbo.vbap.umziz, dbo.vbap.umzin, dbo.vbap.kwmeng, dbo.vbap.netpr, dbo.vbap.netwr, 
      dbo.vbap.waerk, dbo.vbap.kpein, dbo.vbap.kmein, dbo.vbap.charg, dbo.vbap.matkl, 
      dbo.vbap.spart, dbo.vbap.werks, dbo.vbap.lgort, dbo.vbap.ernam, dbo.vbap.erzet, 
      dbo.vbap.aedat, dbo.mara.mfrnr, dbo.vbap.vkbur
FROM dbo.vbap INNER JOIN
      dbo.mara ON dbo.vbap.mandt = dbo.mara.mandt AND 
      dbo.vbap.matnr = dbo.mara.matnr INNER JOIN
      dbo.tvkm ON dbo.vbap.mandt = dbo.tvkm.mandt AND 
      dbo.vbap.ktgrm = dbo.tvkm.ktgrm INNER JOIN
      dbo.tvkmt ON dbo.tvkm.mandt = dbo.tvkmt.mandt AND 
      dbo.tvkm.ktgrm = dbo.tvkmt.ktgrm

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mara
AS
SELECT dbo.mara.mandt, dbo.makt.spras, dbo.mara.matnr, dbo.makt.maktx, 
      dbo.t134t.mtbez, dbo.lpd4.clstxt, dbo.lpd1.sztxt, dbo.t023t.wgbez, dbo.mara.mtart, 
      dbo.lpd4.clsnr, dbo.mara.matkl, dbo.lpd1.sznmr, dbo.mara.mdnmr, dbo.mara.meins, 
      dbo.lp23.rgb, dbo.lpd5.icon
FROM dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND 
      dbo.mara.mtart = dbo.lpd5.mtart INNER JOIN
      dbo.t134t ON dbo.lpd5.mandt = dbo.t134t.mandt AND 
      dbo.makt.spras = dbo.t134t.spras AND dbo.lpd5.mtart = dbo.t134t.mtart INNER JOIN
      dbo.lpd4 ON dbo.lpd5.mandt = dbo.lpd4.mandt AND 
      dbo.lpd5.clsnr = dbo.lpd4.clsnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr ON dbo.t023.mandt = dbo.mara.mandt AND 
      dbo.t023.matkl = dbo.mara.matkl AND dbo.t023t.spras = dbo.makt.spras INNER JOIN
      dbo.lp23 ON dbo.t023.mandt = dbo.lp23.mandt AND dbo.t023.matkl = dbo.lp23.matkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mara23
AS
SELECT dbo.mara.mandt, dbo.t023t.spras, dbo.mara.matnr, dbo.t023.matkl, 
      dbo.t023t.wgbez, dbo.mpmod.stext
FROM dbo.mara INNER JOIN
      dbo.t023 ON dbo.mara.mandt = dbo.t023.mandt AND 
      dbo.mara.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mara_cs
AS
SELECT mandt, matnr, mdnmr, crnmr, size1
FROM dbo.mara

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mara_l
AS
SELECT dbo.mara.mandt, dbo.makt.spras, dbo.mara.matnr, dbo.makt.maktx, 
      dbo.t134t.mtbez, dbo.lpd4.clstxt, dbo.lpd1.sztxt, dbo.t023t.wgbez, dbo.mara.mtart, 
      dbo.lpd4.clsnr, dbo.mara.matkl, dbo.lpd1.sznmr, dbo.mara.mdnmr, dbo.mara.meins, 
      dbo.lp23.rgb, dbo.lpd5.icon, dbo.lfa1.lifnr, dbo.lfa1.anred, dbo.lfa1.name1, 
      dbo.lfa1.land1, dbo.lfa1.sortl, dbo.lfa1.regio, dbo.lfa1.cityc, dbo.lfa1.ort01, 
      dbo.lfa1.pstlz, dbo.lfa1.pfach, dbo.lfa1.pstl2, dbo.lfa1.stras, dbo.lfa1.telbx, 
      dbo.lfa1.telf1, dbo.lfa1.telf2, dbo.lfa1.telfx, dbo.lfa1.teltx, dbo.lfa1.telx1
FROM dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND 
      dbo.mara.mtart = dbo.lpd5.mtart INNER JOIN
      dbo.t134t ON dbo.lpd5.mandt = dbo.t134t.mandt AND 
      dbo.makt.spras = dbo.t134t.spras AND dbo.lpd5.mtart = dbo.t134t.mtart INNER JOIN
      dbo.lpd4 ON dbo.lpd5.mandt = dbo.lpd4.mandt AND 
      dbo.lpd5.clsnr = dbo.lpd4.clsnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr ON dbo.t023.mandt = dbo.mara.mandt AND 
      dbo.t023.matkl = dbo.mara.matkl AND dbo.t023t.spras = dbo.makt.spras INNER JOIN
      dbo.lp23 ON dbo.t023.mandt = dbo.lp23.mandt AND 
      dbo.t023.matkl = dbo.lp23.matkl INNER JOIN
      dbo.lfa1 ON dbo.mara.mandt = dbo.lfa1.mandt AND dbo.mara.mfrnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mara_mp
AS
SELECT dbo.mara.mandt, dbo.t006a.spras, dbo.mara.matnr, dbo.mara.crnmr, 
      dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.mara.size1, dbo.lpd1.sztxt, dbo.mara.mdnmr, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mpmod.ltext, dbo.mara.meins, 
      dbo.t006a.mseht, dbo.mpmod.ktgrm, dbo.mpmod.mtpos, dbo.mpmod.vprsv, 
      dbo.mpmod.kkref, dbo.mpmod.bklas, dbo.mara.spart, dbo.mpmod.xmcng, 
      dbo.mpmod.xchpf, dbo.mpmod.pricpo, dbo.mpmod.pricsd, dbo.mara.matkl, 
      dbo.mpmod.eisbe, dbo.mpmod.mwskz, dbo.mpmod.lifnr, dbo.t134t.mtbez, 
      dbo.mara.mtart, dbo.mpmod.xfjf, dbo.mpmod.tjdtt, dbo.mpmod.tjdtf, dbo.mpmod.tjjf, 
      dbo.mpmod.tjhy, dbo.mpmod.tjpt, dbo.mpmod.tjflg, dbo.mpmod.rpsy, 
      dbo.mpmod.verpr, dbo.mpmod.xsbdj, dbo.mpmod.alwgj, dbo.mpmod.alwzk, 
      dbo.mpmod.alwjf, dbo.mpmod.eisae, dbo.mpmod.erdat, dbo.mpmod.ernam, 
      dbo.mpmod.bdnmr
FROM dbo.mara INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND 
      dbo.mara.mandt = dbo.lpd1.mandt INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.t006 ON dbo.mara.mandt = dbo.t006.mandt AND 
      dbo.mara.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.t006a.spras = dbo.t134t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mard_mp
AS
SELECT dbo.mard.mandt, dbo.mard.matnr, dbo.mard.werks, dbo.mard.lgort, 
      dbo.mpmod.modnr, dbo.mara.crnmr, dbo.mara.size1, dbo.mpmod.stext, 
      dbo.mard.labst, dbo.mard.umlme, dbo.mard.insme, dbo.mard.einme, 
      dbo.mard.speme, dbo.mard.retme, dbo.mpmod.barcode, dbo.mpmod.ltext, 
      dbo.mpmod.xfjf, dbo.mpmod.rpsy, dbo.mpmod.tjflg, dbo.mpmod.tjpt, dbo.mpmod.tjhy, 
      dbo.mpmod.tjjf, dbo.mpmod.tjdtf, dbo.mpmod.tjdtt, dbo.mpmod.verpr, 
      dbo.mpmod.xsbdj, dbo.mpmod.alwgj, dbo.mpmod.alwzk, dbo.mpmod.alwjf, 
      dbo.mpmod.eisae, dbo.mpmod.erdat, dbo.mpmod.ernam, dbo.mpmod.eisbe, 
      dbo.mpmod.mwskz, dbo.mpmod.bdnmr, dbo.mpmod.matkl, dbo.mpmod.waers, 
      dbo.mpmod.pricpo, dbo.mpmod.pricsd
FROM dbo.mara INNER JOIN
      dbo.mard ON dbo.mara.mandt = dbo.mard.mandt AND 
      dbo.mara.matnr = dbo.mard.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mardbew_mp
AS
SELECT dbo.mard.mandt, dbo.t006a.spras, dbo.mpmod.modnr, dbo.mard.matnr, 
      dbo.mard.labst, dbo.mard.umlme, dbo.mbew.verpr, dbo.mbew.peinh, dbo.mbew.salk3, 
      dbo.mbew.lbkum, dbo.t006a.mseht, dbo.mpmod.stext, dbo.lpd1.sztxt, dbo.mpd1.crtxt, 
      dbo.t001l.lgobe, dbo.mard.werks, dbo.mard.lgort, dbo.mbew.bwkey, dbo.mbew.bwtar, 
      dbo.mara.size1, dbo.mara.crnmr, dbo.mpmod.mtart, dbo.mara.meins, dbo.mara.matkl, 
      dbo.mpmod.ltext, dbo.mpd1.rgb, dbo.mpmod.waers
FROM dbo.mard INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.mbew ON dbo.t001w.bwkey = dbo.mbew.bwkey AND 
      dbo.mard.mandt = dbo.mbew.mandt AND 
      dbo.mard.matnr = dbo.mbew.matnr INNER JOIN
      dbo.mara ON dbo.mard.mandt = dbo.mara.mandt AND 
      dbo.mard.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mdnmr = dbo.mpmod.modnr AND 
      dbo.mara.mandt = dbo.mpmod.mandt INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mara.mandt = dbo.t006.mandt AND 
      dbo.mara.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mardbew_np
AS
SELECT dbo.mard.mandt, dbo.t006a.spras, dbo.mpmod.modnr, dbo.mard.matnr, 
      dbo.mard.labst, dbo.mard.umlme, dbo.mbew.verpr, dbo.mbew.peinh, dbo.mbew.salk3, 
      dbo.mbew.lbkum, dbo.t006a.mseht, dbo.mpmod.stext, dbo.lpd1.sztxt, dbo.mpd1.crtxt, 
      dbo.t001l.lgobe, dbo.mard.werks, dbo.mard.lgort, dbo.mbew.bwkey, dbo.mbew.bwtar, 
      dbo.mara.size1, dbo.mara.crnmr, dbo.mpmod.mtart, dbo.mara.meins, dbo.mara.matkl, 
      dbo.mpmod.ltext, dbo.mpd1.rgb, dbo.mpmod.waers
FROM dbo.mard INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.mbew ON dbo.t001w.bwkey = dbo.mbew.bwkey AND 
      dbo.mard.mandt = dbo.mbew.mandt AND 
      dbo.mard.matnr = dbo.mbew.matnr INNER JOIN
      dbo.mpmod ON dbo.mard.mandt = dbo.mpmod.mandt INNER JOIN
      dbo.mara ON dbo.mard.mandt = dbo.mara.mandt AND 
      dbo.mard.matnr = dbo.mara.matnr AND 
      dbo.mpmod.modnr = dbo.mara.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mara.mandt = dbo.t006.mandt AND 
      dbo.mara.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_me22
AS
SELECT dbo.mseg.mandt, dbo.makt.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bwart, dbo.mseg.matnr, dbo.makt.maktx, dbo.mseg.werks, 
      dbo.mseg.lgort, dbo.mseg.umwrk, dbo.mseg.umlgo, dbo.mseg.charg, 
      dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, dbo.mseg.menge, 
      dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, dbo.mseg.bpmng, 
      dbo.mseg.bprme, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.lfbja, dbo.mseg.lfbnr, 
      dbo.mseg.lfpos, dbo.mseg.elikz, dbo.mseg.sjahr, dbo.mseg.smbln, dbo.mseg.smblp, 
      dbo.mseg.sgtxt, dbo.mseg.bukrs, dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.mwskz, 
      dbo.mseg.bldat, dbo.mseg.budat, dbo.t156.kzwes, dbo.t156.xstbw
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr INNER JOIN
      dbo.mseg ON dbo.mara.mandt = dbo.mseg.mandt AND 
      dbo.mara.matnr = dbo.mseg.matnr INNER JOIN
      dbo.t156 ON dbo.mseg.mandt = dbo.t156.mandt AND dbo.mseg.bwart = dbo.t156.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_bkpf
AS
SELECT dbo.mkpf.mandt, dbo.t003t.spras, dbo.mkpf.blart, dbo.t003t.ltext, dbo.mkpf.mblnr, 
      dbo.mkpf.mjahr, dbo.mkpf.vgart, dbo.mkpf.blaum, dbo.mkpf.bldat, dbo.mkpf.budat, 
      dbo.mkpf.cpudt, dbo.mkpf.cputm, dbo.mkpf.aedat, dbo.mkpf.usnam, dbo.mkpf.tcode, 
      dbo.mkpf.xblnr, dbo.mkpf.bktxt, dbo.mkpf.frath, dbo.mkpf.frbnr, dbo.mkpf.wever, 
      dbo.mkpf.xabln, dbo.mkpf.awsys, dbo.mkpf.bla2d, dbo.mkpf.tcode2, dbo.mkpf.bfwms, 
      dbo.mkpf.exnum
FROM dbo.mkpf INNER JOIN
      dbo.t003 ON dbo.mkpf.mandt = dbo.t003.mandt AND 
      dbo.mkpf.blart = dbo.t003.blart INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND dbo.t003.blart = dbo.t003t.blart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mmbe
AS
SELECT dbo.mard.mandt, dbo.makt.spras, dbo.mard.matnr, dbo.mard.werks, 
      dbo.mard.lgort, dbo.makt.maktx, dbo.makt.maktg, dbo.t001l.lgobe, dbo.mara.meins, 
      dbo.mard.labst, dbo.mard.umlme, dbo.mard.insme, dbo.mard.einme, 
      dbo.mard.speme, dbo.mard.retme, dbo.mard.vmlab, dbo.mard.vmuml, dbo.mard.lfgja, 
      dbo.mard.lfmon, dbo.mard.ersda, dbo.mard.lgpbe, dbo.mard.pstat, dbo.mard.sperr, 
      dbo.mard.vmins, dbo.mard.vmein, dbo.mard.vmspe, dbo.mard.vmret, dbo.mard.kzill, 
      dbo.mard.kzilq, dbo.mard.kzile, dbo.mard.kzils, dbo.mard.kzvll, dbo.mard.kzvlq, 
      dbo.mard.kzvle, dbo.mard.kzvls, dbo.mard.diskz, dbo.mard.lsobs, dbo.mard.lminb, 
      dbo.mard.lbstf, dbo.mard.herkl, dbo.mard.exppg, dbo.mard.exver, dbo.mard.klabs, 
      dbo.mard.keinm, dbo.mard.kinsm, dbo.mard.kspem, dbo.mard.dlinl, dbo.mard.prctl, 
      dbo.mard.vklab, dbo.mard.vkuml, dbo.mard.lwmkb, dbo.mard.bskrf, dbo.mard.mdrue, 
      dbo.mard.mdjin
FROM dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr INNER JOIN
      dbo.t001l INNER JOIN
      dbo.mard ON dbo.t001l.mandt = dbo.mard.mandt AND 
      dbo.t001l.werks = dbo.mard.werks AND dbo.t001l.lgort = dbo.mard.lgort ON 
      dbo.mara.mandt = dbo.mard.mandt AND dbo.mara.matnr = dbo.mard.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_bsid
AS
SELECT dbo.bsid.mandt, dbo.t003t.spras, dbo.bsid.bukrs, dbo.bsid.kunnr, dbo.bsid.gjahr, 
      dbo.t003t.ltext, dbo.bsid.belnr, dbo.bsid.buzei, dbo.bsid.budat, dbo.bsid.bldat, 
      dbo.bsid.waers, dbo.bsid.xblnr, dbo.bsid.blart, dbo.bsid.monat, dbo.bsid.shkzg, 
      dbo.bsid.wrbtr, dbo.bsid.wmwst, dbo.bsid.saknr, dbo.bsid.hkont, dbo.bsid.sgtxt, 
      dbo.bsid.rebzg, dbo.kna1.name1, dbo.bkpf.usnam, dbo.bkpf.cpudt, dbo.bkpf.cputm, 
      dbo.bsid.umsks, dbo.bsid.umskz, dbo.bsid.augdt, dbo.bsid.zuonr, dbo.bsid.augbl, 
      dbo.bsid.zumsk, dbo.bsid.bschl, dbo.bsid.gsber, dbo.bsid.mwskz, dbo.bsid.dmbtr, 
      dbo.bsid.mwsts, dbo.bsid.xanet, dbo.bsid.bdiff, dbo.bsid.bdif2, dbo.bsid.aufnr, 
      dbo.bsid.zfbdt, dbo.bsid.zterm, dbo.bsid.zbd1t
FROM dbo.bkpf INNER JOIN
      dbo.bsid ON dbo.bkpf.mandt = dbo.bsid.mandt AND 
      dbo.bkpf.bukrs = dbo.bsid.bukrs AND dbo.bkpf.belnr = dbo.bsid.belnr AND 
      dbo.bkpf.gjahr = dbo.bsid.gjahr INNER JOIN
      dbo.t003t INNER JOIN
      dbo.t003 ON dbo.t003t.mandt = dbo.t003.mandt AND 
      dbo.t003t.blart = dbo.t003.blart ON dbo.bkpf.mandt = dbo.t003.mandt AND 
      dbo.bkpf.blart = dbo.t003.blart INNER JOIN
      dbo.kna1 ON dbo.bsid.mandt = dbo.kna1.mandt AND dbo.bsid.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_bsik
AS
SELECT dbo.bsik.mandt, dbo.t003t.spras, dbo.bsik.bukrs, dbo.bsik.lifnr, dbo.bsik.gjahr, 
      dbo.t003t.ltext, dbo.bsik.belnr, dbo.bsik.buzei, dbo.bsik.budat, dbo.bsik.bldat, 
      dbo.bsik.waers, dbo.bsik.xblnr, dbo.bsik.blart, dbo.bsik.monat, dbo.bsik.shkzg, 
      dbo.bsik.wrbtr, dbo.bsik.wmwst, dbo.bsik.saknr, dbo.bsik.hkont, dbo.bsik.sgtxt, 
      dbo.bsik.rebzg, dbo.lfa1.name1, dbo.bkpf.usnam, dbo.bkpf.cpudt, dbo.bkpf.cputm, 
      dbo.bsik.umsks, dbo.bsik.umskz, dbo.bsik.augdt, dbo.bsik.augbl, dbo.bsik.zuonr, 
      dbo.bsik.bschl, dbo.bsik.zumsk, dbo.bsik.gsber, dbo.bsik.mwskz, dbo.bsik.dmbtr, 
      dbo.bsik.xanet, dbo.bsik.mwsts, dbo.bsik.ebeln, dbo.bsik.ebelp, dbo.bsik.fkont
FROM dbo.bkpf INNER JOIN
      dbo.bsik ON dbo.bkpf.mandt = dbo.bsik.mandt AND 
      dbo.bkpf.bukrs = dbo.bsik.bukrs AND dbo.bkpf.belnr = dbo.bsik.belnr AND 
      dbo.bkpf.gjahr = dbo.bsik.gjahr INNER JOIN
      dbo.t003t INNER JOIN
      dbo.t003 ON dbo.t003t.mandt = dbo.t003.mandt AND 
      dbo.t003t.blart = dbo.t003.blart ON dbo.bkpf.mandt = dbo.t003.mandt AND 
      dbo.bkpf.blart = dbo.t003.blart INNER JOIN
      dbo.lfa1 ON dbo.bsik.mandt = dbo.lfa1.mandt AND dbo.bsik.lifnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_tvb_bsis
AS
SELECT dbo.bsis.mandt, dbo.t003t.spras, dbo.bsis.hkont, dbo.skat.txt50, dbo.bsis.gjahr, 
      dbo.bsis.belnr, dbo.bsis.budat, dbo.bsis.bldat, dbo.bsis.waers, dbo.bsis.monat, 
      dbo.bsis.dmbtr, dbo.bsis.wrbtr, dbo.bsis.sgtxt, dbo.t003t.ltext, dbo.bsis.blart, 
      dbo.bsis.shkzg, dbo.bsis.buzei
FROM dbo.bsis INNER JOIN
      dbo.t003 ON dbo.bsis.mandt = dbo.t003.mandt AND 
      dbo.bsis.blart = dbo.t003.blart INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND 
      dbo.t003.blart = dbo.t003t.blart INNER JOIN
      dbo.skat ON dbo.bsis.mandt = dbo.skat.mandt AND 
      dbo.t003t.spras = dbo.skat.spras AND dbo.bsis.hkont = dbo.skat.saknr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_ekko
AS
SELECT dbo.ekko.mandt, dbo.ekko.ebeln, dbo.ekko.bukrs, dbo.ekko.bsart, dbo.ekko.lifnr, 
      dbo.lfa1.name1, dbo.ekko.aedat, dbo.ekko.ernam, dbo.ekko.ekorg, 
      dbo.ekko.ekgrp
FROM dbo.ekko INNER JOIN
      dbo.lfa1 ON dbo.ekko.mandt = dbo.lfa1.mandt AND dbo.ekko.lifnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_ekpo
AS
SELECT dbo.ekpo.mandt, dbo.t006a.spras, dbo.ekpo.ebeln, dbo.ekpo.ebelp, 
      dbo.mara.mdnmr, dbo.ekpo.txz01, dbo.ekpo.menge, dbo.ekpo.meins, dbo.ekpo.matnr, 
      dbo.ekpo.netpr, dbo.ekpo.peinh, dbo.ekpo.netwr, dbo.mpmod.stext, dbo.ekpo.bukrs, 
      dbo.ekpo.werks, dbo.ekpo.lgort, dbo.mara.crnmr, dbo.mara.size1, dbo.mpd1.crtxt, 
      dbo.mpd1.rgb, dbo.lpd1.sztxt, dbo.t001l.lgobe, dbo.t006a.mseht, 
      dbo.ekpo.aedat
FROM dbo.ekpo INNER JOIN
      dbo.mara ON dbo.ekpo.mandt = dbo.mara.mandt AND 
      dbo.ekpo.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.t001l ON dbo.ekpo.mandt = dbo.t001l.mandt AND 
      dbo.ekpo.werks = dbo.t001l.werks AND dbo.ekpo.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.ekpo.mandt = dbo.t006.mandt AND 
      dbo.ekpo.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mp_kccb
AS
SELECT dbo.mseg.mandt, dbo.t156t.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bwart, dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, 
      dbo.mseg.erfmg, dbo.mseg.lifnr, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.kunnr, 
      dbo.mseg.kdauf, dbo.mseg.kdpos, dbo.mseg.werks, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mseg.lgort, dbo.t001l.lgobe, dbo.mseg.erfme, dbo.t006a.mseht, 
      dbo.mpmod.modnr, dbo.mseg.matnr, dbo.mpmod.stext, dbo.mpd1.crnmr, 
      dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sznmr, dbo.lpd1.sztxt
FROM dbo.mseg INNER JOIN
      dbo.t156t ON dbo.mseg.mandt = dbo.t156t.mandt AND 
      dbo.mseg.bwart = dbo.t156t.bwart AND dbo.mseg.sobkz = dbo.t156t.sobkz AND 
      dbo.mseg.kzbew = dbo.t156t.kzbew AND dbo.mseg.kzvbr = dbo.t156t.kzvbr AND 
      dbo.mseg.kzzug = dbo.t156t.kzzug INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t156t.spras = dbo.t006a.spras INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND dbo.mara.mandt = dbo.lpd1.mandt


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_kna1
AS
SELECT dbo.kna1.mandt, dbo.t005t.spras, dbo.kna1.kunnr, dbo.kna1.ktokd, dbo.kna1.anred, 
      dbo.kna1.name1, dbo.kna1.name2, dbo.kna1.land1, dbo.kna1.regio, dbo.t005t.landx, 
      dbo.t005u.blandx, dbo.kna1.cityc, dbo.kna1.pstlz, dbo.kna1.stras, dbo.kna1.bbsnr, 
      dbo.kna1.telf1, dbo.kna1.telf2, dbo.kna1.telfx, dbo.kna1.telbx, 
      dbo.kna1.spras AS sprask, dbo.kna1.erdat, dbo.kna1.ernam, dbo.kna1.rcvsms, 
      dbo.kna1.rcvemail, dbo.kna1.intro, dbo.kna1.ort01, dbo.kna1.sortl, dbo.kna1.vkgrps, 
      dbo.t077dmp.type, dbo.kna1.sex, dbo.kna1mp.birthday, dbo.kna1mp.bthday, 
      dbo.kna1mp.vipcard, dbo.kna1mp.vipcardp, dbo.kna1mp.sinuc, dbo.kna1mp.qq, 
      dbo.kna1mp.alww, dbo.kna1mp.ltext, dbo.kna1mp.idnum, dbo.kna1mp.validf, 
      dbo.kna1mp.validt, dbo.kna1mp.pwd
FROM dbo.kna1 INNER JOIN
      dbo.t005 ON dbo.kna1.mandt = dbo.t005.mandt AND 
      dbo.kna1.land1 = dbo.t005.land1 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND 
      dbo.t005.land1 = dbo.t005t.land1 INNER JOIN
      dbo.t005s ON dbo.kna1.mandt = dbo.t005s.mandt AND 
      dbo.kna1.land1 = dbo.t005s.land1 AND dbo.kna1.regio = dbo.t005s.bland INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND dbo.t005s.bland = dbo.t005u.bland AND 
      dbo.t005t.spras = dbo.t005u.spras INNER JOIN
      dbo.t077dmp ON dbo.kna1.mandt = dbo.t077dmp.mandt AND 
      dbo.kna1.ktokd = dbo.t077dmp.ktokd INNER JOIN
      dbo.kna1mp ON dbo.kna1.mandt = dbo.kna1mp.mandt AND 
      dbo.kna1.kunnr = dbo.kna1mp.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_knc1
AS
SELECT dbo.knc1.mandt, dbo.knc1.kunnr, dbo.kna1.name1, dbo.knc1.bukrs, dbo.knc1.gjahr, 
      dbo.knc1.erdat, dbo.knc1.usnam, dbo.knc1.umsav, dbo.knc1.um01s, dbo.knc1.um01h, 
      dbo.knc1.um01u, dbo.knc1.um02s, dbo.knc1.um02h, dbo.knc1.um02u, 
      dbo.knc1.um03s, dbo.knc1.um03h, dbo.knc1.um03u, dbo.knc1.um04s, 
      dbo.knc1.um04h, dbo.knc1.um04u, dbo.knc1.um05s, dbo.knc1.um05h, 
      dbo.knc1.um05u, dbo.knc1.um06s, dbo.knc1.um06h, dbo.knc1.um06u, 
      dbo.knc1.um07s, dbo.knc1.um07h, dbo.knc1.um07u, dbo.knc1.um08s, 
      dbo.knc1.um08h, dbo.knc1.um08u, dbo.knc1.um09s, dbo.knc1.um09h, 
      dbo.knc1.um09u, dbo.knc1.um10s, dbo.knc1.um10h, dbo.knc1.um10u, 
      dbo.knc1.um11s, dbo.knc1.um11h, dbo.knc1.um11u, dbo.knc1.um12s, 
      dbo.knc1.um12h, dbo.knc1.um12u, dbo.knc1.babzg, dbo.knc1.uabzg, dbo.knc1.kzins, 
      dbo.knc1.kumag
FROM dbo.knc1 INNER JOIN
      dbo.kna1 ON dbo.knc1.mandt = dbo.kna1.mandt AND dbo.knc1.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_knc3
AS
SELECT dbo.knc3.mandt, dbo.t074t.spras, dbo.knc3.kunnr, dbo.kna1.name1, 
      dbo.knc3.bukrs, dbo.knc3.gjahr, dbo.knc3.shbkz, dbo.knc3.saldv, dbo.knc3.solll, 
      dbo.knc3.habnl, dbo.t074t.ktext, dbo.t074t.ltext, dbo.t074u.umsks, 
      dbo.t074u.koart
FROM dbo.kna1 INNER JOIN
      dbo.knc3 ON dbo.kna1.mandt = dbo.knc3.mandt AND 
      dbo.kna1.kunnr = dbo.knc3.kunnr INNER JOIN
      dbo.t074u ON dbo.knc3.mandt = dbo.t074u.mandt AND 
      dbo.knc3.shbkz = dbo.t074u.umskz INNER JOIN
      dbo.t074t ON dbo.t074u.mandt = dbo.t074t.mandt AND 
      dbo.t074u.koart = dbo.t074t.koart AND dbo.t074u.umskz = dbo.t074t.shbkz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_lfc1
AS
SELECT dbo.lfc1.mandt, dbo.lfc1.lifnr, dbo.lfa1.name1, dbo.lfc1.bukrs, dbo.lfc1.gjahr, 
      dbo.lfc1.erdat, dbo.lfc1.usnam, dbo.lfc1.umsav, dbo.lfc1.um01s, dbo.lfc1.um01h, 
      dbo.lfc1.um01u, dbo.lfc1.um02s, dbo.lfc1.um02h, dbo.lfc1.um02u, dbo.lfc1.um03s, 
      dbo.lfc1.um03h, dbo.lfc1.um03u, dbo.lfc1.um04s, dbo.lfc1.um04h, dbo.lfc1.um04u, 
      dbo.lfc1.um05s, dbo.lfc1.um05h, dbo.lfc1.um05u, dbo.lfc1.um06s, dbo.lfc1.um06h, 
      dbo.lfc1.um06u, dbo.lfc1.um07s, dbo.lfc1.um07h, dbo.lfc1.um07u, dbo.lfc1.um08s, 
      dbo.lfc1.um08h, dbo.lfc1.um08u, dbo.lfc1.um09s, dbo.lfc1.um09h, dbo.lfc1.um09u, 
      dbo.lfc1.um10s, dbo.lfc1.um10h, dbo.lfc1.um10u, dbo.lfc1.um11s, dbo.lfc1.um11h, 
      dbo.lfc1.um11u, dbo.lfc1.um12s, dbo.lfc1.um12h, dbo.lfc1.um12u
FROM dbo.lfc1 INNER JOIN
      dbo.lfa1 ON dbo.lfc1.mandt = dbo.lfa1.mandt AND dbo.lfc1.lifnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_lp189i
AS
SELECT dbo.lp189i.mandt, dbo.lp189i.pltyp, dbo.lp189i.matnr, dbo.mpmod.stext, 
      dbo.mpmod.modnr, dbo.lp189i.zeile, dbo.lp189i.frdat, dbo.lp189i.todat, 
      dbo.lp189i.mengf, dbo.lp189i.mengt, dbo.lp189i.meinh, dbo.lp189i.peinh, 
      dbo.lp189i.price, dbo.lp189i.waers, dbo.lp189i.priot, dbo.lp189i.erdat, 
      dbo.lp189i.ernam
FROM dbo.mara INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.lp189i ON dbo.mara.mandt = dbo.lp189i.mandt AND 
      dbo.mara.matnr = dbo.lp189i.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE VIEW dbo.l_mp_mard
AS
SELECT dbo.mard.mandt, dbo.t006a.spras, dbo.mard.matnr, dbo.mard.werks, 
      dbo.mard.lgort, dbo.mpmod.modnr, dbo.mpmod.stext, dbo.mpmod.eisbe, 
      dbo.mard.labst, dbo.mard.umlme, dbo.t001l.lgobe, dbo.mpmod.barcode, 
      dbo.lpd1.sztxt, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sznmr, dbo.mpd1.crnmr, 
      dbo.t006a.mseht
FROM dbo.mara INNER JOIN
      dbo.mard ON dbo.mara.mandt = dbo.mard.mandt AND 
      dbo.mara.matnr = dbo.mard.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.t006 ON dbo.mpmod.mandt = dbo.t006.mandt AND 
      dbo.mpmod.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_mard_mbew
AS
SELECT dbo.mard.mandt, dbo.mard.matnr, dbo.mard.werks, dbo.mard.lgort, 
      dbo.mbew.lbkum, dbo.mbew.salk3, dbo.mard.labst, dbo.mard.umlme
FROM dbo.mard INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t001w ON dbo.t001l.mandt = dbo.t001w.mandt AND 
      dbo.t001l.werks = dbo.t001w.werks INNER JOIN
      dbo.t001k ON dbo.t001w.mandt = dbo.t001k.mandt AND 
      dbo.t001w.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.mbew ON dbo.t001k.mandt = dbo.mbew.mandt AND 
      dbo.t001k.bwkey = dbo.mbew.bwkey AND dbo.mard.matnr = dbo.mbew.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_mbew
AS
SELECT dbo.mbew.mandt, dbo.t006a.spras, dbo.mpmod.modnr, dbo.mpmod.stext, 
      dbo.lpd1.sznmr, dbo.lpd1.sztxt, dbo.t006a.mseht, dbo.mbew.matnr, dbo.mbew.lbkum, 
      dbo.mbew.salk3, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.mara.mtart, dbo.t134t.mtbez, 
      dbo.t001.waers
FROM dbo.t006 INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.mbew INNER JOIN
      dbo.mara ON dbo.mbew.mandt = dbo.mara.mandt AND 
      dbo.mbew.matnr = dbo.mara.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.crnmr = dbo.mpd1.crnmr AND 
      dbo.mara.mandt = dbo.mpd1.mandt INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr ON dbo.t006.mandt = dbo.mara.mandt AND 
      dbo.t006.msehi = dbo.mara.meins INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.t006a.spras = dbo.t134t.spras INNER JOIN
      dbo.t001k ON dbo.mbew.mandt = dbo.t001k.mandt AND 
      dbo.mbew.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_mseg
AS
SELECT dbo.mseg.mandt, dbo.t156t.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bwart, dbo.t156t.btext, dbo.mpmod.modnr, 
      dbo.mseg.matnr, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sztxt, dbo.mseg.lgort, 
      dbo.t001l.lgobe, dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, 
      dbo.mseg.menge, dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, 
      dbo.t006a.mseht, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.sgtxt, dbo.mseg.bukrs, 
      dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.umwrk, dbo.mseg.umlgo, 
      dbo.mseg.charg, dbo.mseg.werks, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mara.crnmr, dbo.mara.size1, 
      dbo.mara.mtart
FROM dbo.mseg INNER JOIN
      dbo.t156t ON dbo.mseg.mandt = dbo.t156t.mandt AND 
      dbo.mseg.bwart = dbo.t156t.bwart AND dbo.mseg.sobkz = dbo.t156t.sobkz AND 
      dbo.mseg.kzbew = dbo.t156t.kzbew AND dbo.mseg.kzvbr = dbo.t156t.kzvbr AND 
      dbo.mseg.kzzug = dbo.t156t.kzzug INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t156t.spras = dbo.t006a.spras INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND dbo.mara.mandt = dbo.lpd1.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_mseg_lfa1
AS
SELECT dbo.mseg.mandt, dbo.t006a.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.lifnr, dbo.lfa1.name1, 
      dbo.mseg.bwart, dbo.mpmod.modnr, dbo.mseg.matnr, dbo.mpd1.crtxt, dbo.mpd1.rgb, 
      dbo.lpd1.sztxt, dbo.mseg.lgort, dbo.t001l.lgobe, dbo.mseg.shkzg, dbo.mseg.waers, 
      dbo.mseg.dmbtr, dbo.mseg.menge, dbo.mseg.meins, dbo.mseg.erfmg, 
      dbo.mseg.erfme, dbo.t006a.mseht, dbo.mseg.sgtxt, dbo.mseg.umwrk, 
      dbo.mseg.umlgo, dbo.mseg.charg, dbo.mseg.werks, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mara.crnmr, dbo.mara.size1
FROM dbo.mseg INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND 
      dbo.mara.mandt = dbo.lpd1.mandt INNER JOIN
      dbo.lfa1 ON dbo.mseg.mandt = dbo.lfa1.mandt AND dbo.mseg.lifnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_pltyp
AS
SELECT dbo.kna1.mandt, dbo.kna1.kunnr, dbo.kna1.ktokd, dbo.knvv.pltyp, 
      dbo.t077dmp.pltyp AS pltyp2, dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart
FROM dbo.kna1 INNER JOIN
      dbo.t077dmp ON dbo.kna1.mandt = dbo.t077dmp.mandt AND 
      dbo.kna1.ktokd = dbo.t077dmp.ktokd INNER JOIN
      dbo.knvv ON dbo.kna1.mandt = dbo.knvv.mandt AND dbo.kna1.kunnr = dbo.knvv.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_prcrd
AS
SELECT dbo.mpprcrd.mandt, dbo.mpprcrd.modnr, dbo.mpmod.stext, dbo.mpprcrd.erdat, 
      dbo.mpprcrd.erzet, dbo.mpprcrd.pricepoold, dbo.mpprcrd.pricesdold, 
      dbo.mpprcrd.priceponew, dbo.mpprcrd.pricesdnew, dbo.mpprcrd.ernam, 
      dbo.mpmod.barcode
FROM dbo.mpprcrd INNER JOIN
      dbo.mpmod ON dbo.mpprcrd.mandt = dbo.mpmod.mandt AND 
      dbo.mpprcrd.modnr = dbo.mpmod.modnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mp_rbkp
AS
SELECT dbo.rbkp.mandt, dbo.t003t.spras, dbo.rbkp.belnr, dbo.rbkp.gjahr, dbo.rbkp.blart, 
      dbo.rbkp.bldat, dbo.rbkp.budat, dbo.rbkp.usnam, dbo.rbkp.cpudt, dbo.rbkp.cputm, 
      dbo.rbkp.bukrs, dbo.rbkp.lifnr, dbo.rbkp.waers, dbo.rbkp.kursf, dbo.rbkp.rmwwr, 
      dbo.rbkp.bktxt, dbo.t003t.ltext, dbo.lfa1.name1
FROM dbo.rbkp INNER JOIN
      dbo.t003 ON dbo.rbkp.mandt = dbo.t003.mandt AND 
      dbo.rbkp.blart = dbo.t003.blart INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND 
      dbo.t003.blart = dbo.t003t.blart INNER JOIN
      dbo.lfa1 ON dbo.rbkp.mandt = dbo.lfa1.mandt AND dbo.rbkp.lifnr = dbo.lfa1.lifnr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mp_rseg
AS
SELECT dbo.rseg.mandt, dbo.t006a.spras, dbo.rseg.belnr, dbo.rseg.gjahr, dbo.rseg.buzei, 
      dbo.mpmod.stext, dbo.mpmod.modnr, dbo.lpd1.sznmr, dbo.lpd1.sztxt, 
      dbo.mpd1.crnmr, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.rseg.matnr, dbo.rseg.ebeln, 
      dbo.rseg.ebelp, dbo.rseg.bukrs, dbo.rseg.werks, dbo.rseg.shkzg, dbo.rseg.menge, 
      dbo.rseg.wrbtr, dbo.rseg.bstme, dbo.t006a.mseht
FROM dbo.rseg INNER JOIN
      dbo.mara ON dbo.rseg.mandt = dbo.mara.mandt AND 
      dbo.rseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND 
      dbo.mara.mandt = dbo.lpd1.mandt INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.t006 ON dbo.rseg.bstme = dbo.t006.msehi AND 
      dbo.rseg.mandt = dbo.t006.mandt INNER JOIN
      dbo.t006a ON dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t006.mandt = dbo.t006a.mandt


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mp_ska1
AS
SELECT dbo.ska1.mandt, dbo.skat.spras, dbo.ska1.ktopl, dbo.ska1.saknr, dbo.ska1.bilkt, 
      dbo.ska1.gvtyp, dbo.skat.txt50, dbo.ska1.xbilk
FROM dbo.ska1 INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.ktopl = dbo.skat.ktopl AND dbo.ska1.saknr = dbo.skat.saknr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mp_t030_bil
AS
SELECT dbo.t030.mandt, dbo.skat.spras, dbo.t030.ktopl, dbo.t030.ktosl, dbo.t030.bwmod, 
      dbo.t030.komok, dbo.t030.konts, dbo.skat.txt50
FROM dbo.t030 INNER JOIN
      dbo.ska1 ON dbo.t030.mandt = dbo.ska1.mandt AND 
      dbo.t030.ktopl = dbo.ska1.ktopl AND dbo.t030.konts = dbo.ska1.saknr INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.saknr = dbo.skat.saknr AND dbo.ska1.ktopl = dbo.skat.ktopl


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_t077d
AS
SELECT dbo.t077d.mandt, dbo.t077d.ktokd, dbo.t077x.spras, dbo.t077d.numkr, 
      dbo.t077x.txt30, dbo.t077dmp.type, dbo.t077dmp.pltyp, dbo.t077dmp.jfnmr
FROM dbo.t077d INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd INNER JOIN
      dbo.t077dmp ON dbo.t077d.mandt = dbo.t077dmp.mandt AND 
      dbo.t077d.ktokd = dbo.t077dmp.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_t077d
AS
SELECT dbo.t077d.mandt, dbo.t077d.ktokd, dbo.t077x.spras, dbo.t077x.txt30, 
      dbo.t077dmp.type, dbo.t077dmp.pltyp, dbo.t189.reflg, dbo.t189.replt, dbo.t189.ratio, 
      dbo.t189t.ptext, dbo.t077d.numkr
FROM dbo.t189 INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND 
      dbo.t189.pltyp = dbo.t189t.pltyp INNER JOIN
      dbo.t077d INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd INNER JOIN
      dbo.t077dmp ON dbo.t077d.mandt = dbo.t077dmp.mandt AND 
      dbo.t077d.ktokd = dbo.t077dmp.ktokd ON dbo.t189.mandt = dbo.t077dmp.mandt AND 
      dbo.t189.pltyp = dbo.t077dmp.pltyp AND dbo.t189t.spras = dbo.t077x.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_vbak
AS
SELECT dbo.vbak.mandt, dbo.tvkbt.spras, dbo.vbak.vbeln, dbo.vbak.kunnr, 
      dbo.kna1.name1, dbo.vbak.vkbur, dbo.vbak.vkgrp, dbo.vbak.netwr, dbo.vbak.waerk, 
      dbo.vbak.erdat, dbo.vbak.ernam, dbo.tvkbt.vkburx, dbo.vbak.auart, dbo.tvakt.auartx, 
      dbo.vbak.audat
FROM dbo.kna1 INNER JOIN
      dbo.vbak ON dbo.kna1.mandt = dbo.vbak.mandt AND 
      dbo.kna1.kunnr = dbo.vbak.kunnr INNER JOIN
      dbo.tvbur ON dbo.vbak.mandt = dbo.tvbur.mandt AND 
      dbo.vbak.vkbur = dbo.tvbur.vkbur INNER JOIN
      dbo.tvkbt ON dbo.tvbur.mandt = dbo.tvkbt.mandt AND 
      dbo.tvbur.vkbur = dbo.tvkbt.vkbur INNER JOIN
      dbo.tvak ON dbo.vbak.mandt = dbo.tvak.mandt AND 
      dbo.vbak.auart = dbo.tvak.auart INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart AND dbo.tvkbt.spras = dbo.tvakt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_vbap
AS
SELECT dbo.vbap.mandt, dbo.t006a.spras, dbo.vbap.vbeln, dbo.vbap.posnr, 
      dbo.mara.mdnmr, dbo.mpmod.stext, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sztxt, 
      dbo.t006a.mseht, dbo.kna1.name1, dbo.vbak.ernam, dbo.vbak.erdat, dbo.vbap.matnr, 
      dbo.vbap.kwmeng, dbo.vbap.netpr, dbo.vbap.netwr, dbo.vbap.waerk, dbo.t001l.lgobe, 
      dbo.vbap.werks, dbo.vbap.lgort, dbo.vbap.vrkme, dbo.mara.crnmr, dbo.mara.size1, 
      dbo.vbak.kunnr, dbo.mara.mtart, dbo.vbak.vkgrp, dbo.vbak.vkbur, dbo.tvap.shkzg, 
      dbo.vbap.ktgrm, dbo.tvkmt.vtext, dbo.vbap.kdmat
FROM dbo.vbap INNER JOIN
      dbo.mara INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr ON dbo.vbap.mandt = dbo.mara.mandt AND 
      dbo.vbap.matnr = dbo.mara.matnr INNER JOIN
      dbo.t006a INNER JOIN
      dbo.t006 ON dbo.t006a.mandt = dbo.t006.mandt AND 
      dbo.t006a.msehi = dbo.t006.msehi ON dbo.vbap.mandt = dbo.t006.mandt AND 
      dbo.vbap.vrkme = dbo.t006.msehi INNER JOIN
      dbo.vbak ON dbo.vbap.mandt = dbo.vbak.mandt AND 
      dbo.vbap.vbeln = dbo.vbak.vbeln INNER JOIN
      dbo.kna1 ON dbo.vbak.mandt = dbo.kna1.mandt AND 
      dbo.vbak.kunnr = dbo.kna1.kunnr INNER JOIN
      dbo.t001l ON dbo.vbap.mandt = dbo.t001l.mandt AND 
      dbo.vbap.werks = dbo.t001l.werks AND dbo.vbap.lgort = dbo.t001l.lgort INNER JOIN
      dbo.tvap ON dbo.vbap.mandt = dbo.tvap.mandt AND 
      dbo.vbap.pstyv = dbo.tvap.pstyv INNER JOIN
      dbo.tvkmt ON dbo.vbap.mandt = dbo.tvkmt.mandt AND 
      dbo.vbap.ktgrm = dbo.tvkmt.ktgrm AND dbo.t006a.spras = dbo.tvkmt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_vbrp
AS
SELECT dbo.vbrp.mandt, dbo.t006a.spras, dbo.vbrp.vbeln, dbo.vbrp.posnr, dbo.vbrp.vrkme, 
      dbo.vbrp.fkimg, dbo.vbrp.matnr, dbo.vbrp.aubel, dbo.vbrp.aupos, dbo.vbrp.netwr, 
      dbo.mpd1.crtxt, dbo.lpd1.sztxt, dbo.mpd1.rgb, dbo.mpmod.modnr, dbo.mpmod.stext, 
      dbo.t006a.mseht
FROM dbo.vbrp INNER JOIN
      dbo.mara ON dbo.vbrp.mandt = dbo.mara.mandt AND 
      dbo.vbrp.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr INNER JOIN
      dbo.t006 ON dbo.vbrp.mandt = dbo.t006.mandt AND 
      dbo.vbrp.vrkme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_vip
AS
SELECT dbo.kna1.mandt, dbo.t077x.spras, dbo.kna1.kunnr, dbo.knb1.bukrs, 
      dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart, dbo.t077x.txt30, dbo.kna1.ktokd, 
      dbo.kna1.name1, dbo.kna1.telf1, dbo.kna1.telf2, dbo.kna1.telfx, dbo.kna1.telbx, 
      dbo.kna1.land1, dbo.kna1.regio, dbo.kna1.cityc, dbo.t077dmp.type, 
      dbo.knc3mp.knjine, dbo.knc3mp.xfjine, dbo.knc3mp.xfjf, dbo.t189.reflg, dbo.t189.replt, 
      dbo.t189.ratio, dbo.t005u.blandx, dbo.t005h.citycx, dbo.t005t.landx, dbo.t189t.ptext, 
      dbo.knvv.vkbur, dbo.knvv.vkgrp, dbo.knvv.waers, dbo.knb1.akont, dbo.kna1.pstlz, 
      dbo.kna1.stras, dbo.kna1.anred, dbo.knvv.ktgrd, dbo.knvv.vwerk, dbo.kna1mp.sex, 
      dbo.kna1mp.birthday, dbo.kna1mp.bthday, dbo.kna1mp.vipcard, dbo.kna1mp.idnum, 
      dbo.kna1mp.validf, dbo.kna1mp.validt, dbo.kna1mp.pwd, dbo.kna1mp.vipcardp, 
      dbo.kna1mp.sinuc, dbo.kna1mp.qq, dbo.kna1mp.alww, dbo.kna1mp.ltext, 
      dbo.knvv.pltyp
FROM dbo.t189 INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND 
      dbo.t189.pltyp = dbo.t189t.pltyp INNER JOIN
      dbo.t005h INNER JOIN
      dbo.t005g ON dbo.t005h.mandt = dbo.t005g.mandt AND 
      dbo.t005h.land1 = dbo.t005g.land1 AND dbo.t005h.regio = dbo.t005g.regio AND 
      dbo.t005h.cityc = dbo.t005g.cityc INNER JOIN
      dbo.t077dmp INNER JOIN
      dbo.kna1 INNER JOIN
      dbo.knc3mp ON dbo.kna1.mandt = dbo.knc3mp.mandt AND 
      dbo.kna1.kunnr = dbo.knc3mp.kunnr INNER JOIN
      dbo.t077d INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd ON dbo.kna1.ktokd = dbo.t077d.ktokd AND 
      dbo.kna1.mandt = dbo.t077d.mandt ON dbo.t077dmp.mandt = dbo.t077d.mandt AND 
      dbo.t077dmp.ktokd = dbo.t077d.ktokd INNER JOIN
      dbo.t005t INNER JOIN
      dbo.t005 ON dbo.t005t.mandt = dbo.t005.mandt AND 
      dbo.t005t.land1 = dbo.t005.land1 ON dbo.kna1.mandt = dbo.t005.mandt AND 
      dbo.kna1.land1 = dbo.t005.land1 AND dbo.t077x.spras = dbo.t005t.spras INNER JOIN
      dbo.t005u ON dbo.t005t.spras = dbo.t005u.spras INNER JOIN
      dbo.t005s ON dbo.t005u.mandt = dbo.t005s.mandt AND 
      dbo.t005u.land1 = dbo.t005s.land1 AND dbo.t005u.bland = dbo.t005s.bland AND 
      dbo.kna1.mandt = dbo.t005s.mandt AND dbo.kna1.land1 = dbo.t005s.land1 AND 
      dbo.kna1.regio = dbo.t005s.bland ON dbo.t005h.spras = dbo.t005u.spras AND 
      dbo.t005g.mandt = dbo.kna1.mandt AND dbo.t005g.land1 = dbo.kna1.land1 AND 
      dbo.t005g.regio = dbo.kna1.regio AND dbo.t005g.cityc = dbo.kna1.cityc ON 
      dbo.t189t.spras = dbo.t005h.spras INNER JOIN
      dbo.knvv ON dbo.kna1.mandt = dbo.knvv.mandt AND 
      dbo.kna1.kunnr = dbo.knvv.kunnr AND dbo.t189.pltyp = dbo.knvv.pltyp AND 
      dbo.t189.mandt = dbo.knvv.mandt INNER JOIN
      dbo.knb1 ON dbo.kna1.mandt = dbo.knb1.mandt AND 
      dbo.kna1.kunnr = dbo.knb1.kunnr INNER JOIN
      dbo.kna1mp ON dbo.kna1.mandt = dbo.kna1mp.mandt AND 
      dbo.kna1.kunnr = dbo.kna1mp.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mp_vip_xf
AS
SELECT dbo.bseg.mandt, dbo.bseg.kunnr, dbo.kna1.name1, dbo.bseg.bukrs, 
      dbo.bseg.belnr, dbo.bseg.gjahr, dbo.bseg.buzei, dbo.bseg.buzid, dbo.bseg.bschl, 
      dbo.bseg.koart, dbo.bseg.umskz, dbo.bseg.shkzg, dbo.bseg.mwskz, dbo.bseg.dmbtr, 
      dbo.bseg.wrbtr, dbo.bseg.hwbas, dbo.bseg.fwbas, dbo.bseg.mwart, dbo.bseg.ktosl, 
      dbo.bseg.valut, dbo.bseg.zfbdt, dbo.bseg.sgtxt, dbo.bseg.kokrs, dbo.bseg.kostl, 
      dbo.bseg.aufnr, dbo.bseg.xauto, dbo.bseg.saknr
FROM dbo.bseg INNER JOIN
      dbo.kna1 ON dbo.bseg.mandt = dbo.kna1.mandt AND 
      dbo.bseg.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mpd31
AS
SELECT dbo.mpd31.mandt, dbo.mpd31.crgzp, dbo.mpd31.crnmr, dbo.mpd1.crtxt, 
      dbo.mpd3.crgrpx, dbo.mpd1.rgb
FROM dbo.mpd1 INNER JOIN
      dbo.mpd31 ON dbo.mpd1.mandt = dbo.mpd31.mandt AND 
      dbo.mpd1.crnmr = dbo.mpd31.crnmr INNER JOIN
      dbo.mpd3 ON dbo.mpd31.crgzp = dbo.mpd3.crgrp AND 
      dbo.mpd31.mandt = dbo.mpd3.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mpmod
AS
SELECT dbo.mpmod.mandt, dbo.t134t.spras, dbo.mpmod.modnr, dbo.mpmod.barcode, 
      dbo.mpmod.mtart, dbo.mpmod.stext, dbo.mpmod.ktgrm, dbo.mpmod.mtpos, 
      dbo.mpmod.vprsv, dbo.mpmod.bklas, dbo.mpmod.kkref, dbo.mpmod.meins, 
      dbo.t006a.mseht, dbo.mpmod.spart, dbo.mpmod.xmcng, dbo.mpmod.xchpf, 
      dbo.mpmod.pricpo, dbo.mpmod.pricsd, dbo.mpmod.matkl, dbo.mpmod.eisbe, 
      dbo.mpmod.mwskz, dbo.mpmod.lifnr, dbo.mpmod.ernam, dbo.mpmod.erdat, 
      dbo.tspat.spartx, dbo.t023t.wgbez, dbo.t134t.mtbez, dbo.mpmod.waers, 
      dbo.mpmod.bdnmr, dbo.mpbrd.bdtxt, dbo.mpmod.xfjf, dbo.mpmod.rpsy, 
      dbo.mpmod.eisae, dbo.mpmod.alwjf, dbo.mpmod.alwzk, dbo.mpmod.alwgj, 
      dbo.mpmod.xsbdj, dbo.mpmod.tjflg, dbo.mpmod.tjpt, dbo.mpmod.tjhy, dbo.mpmod.tjjf, 
      dbo.mpmod.tjdtf, dbo.mpmod.tjdtt, dbo.mpmod.cmpnd, dbo.t134.szgrp, 
      dbo.t134.crgrp
FROM dbo.mpmod INNER JOIN
      dbo.t006 ON dbo.mpmod.mandt = dbo.t006.mandt AND 
      dbo.mpmod.meins = dbo.t006.msehi INNER JOIN
      dbo.t134 ON dbo.mpmod.mandt = dbo.t134.mandt AND 
      dbo.mpmod.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t134t.spras = dbo.t006a.spras INNER JOIN
      dbo.tspa ON dbo.mpmod.mandt = dbo.tspa.mandt AND 
      dbo.mpmod.spart = dbo.tspa.spart INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND 
      dbo.tspa.spart = dbo.tspat.spart AND dbo.t006a.spras = dbo.tspat.spras INNER JOIN
      dbo.t023 ON dbo.mpmod.mandt = dbo.t023.mandt AND 
      dbo.mpmod.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl AND dbo.t006a.spras = dbo.t023t.spras INNER JOIN
      dbo.mpbrd ON dbo.mpmod.mandt = dbo.mpbrd.mandt AND 
      dbo.mpmod.bdnmr = dbo.mpbrd.bdnmr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_mpska1
AS
SELECT dbo.mpska1.mandt, dbo.skat.spras, dbo.mpska1.saknr, dbo.skat.txt20, 
      dbo.skat.txt50, dbo.mpska1.ktopl
FROM dbo.mpska1 INNER JOIN
      dbo.skat ON dbo.mpska1.mandt = dbo.skat.mandt AND 
      dbo.mpska1.ktopl = dbo.skat.ktopl AND dbo.mpska1.saknr = dbo.skat.saknr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_mseg
AS
SELECT dbo.mseg.mandt, dbo.makt.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bldat, dbo.mseg.budat, dbo.mseg.matnr, dbo.mseg.bwart, 
      dbo.mseg.xauto, dbo.mseg.werks, dbo.mseg.lgort, dbo.mseg.charg, dbo.mseg.insmk, 
      dbo.mseg.sobkz, dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, 
      dbo.mseg.menge, dbo.mseg.erfmg, dbo.mseg.kzbew, dbo.mseg.erfme, 
      dbo.makt.maktx, dbo.t134t.mtbez, dbo.lpd4.clstxt, dbo.lpd1.sztxt, dbo.t023t.wgbez, 
      dbo.mara.mtart, dbo.lpd4.clsnr, dbo.mara.matkl, dbo.lpd1.sznmr, dbo.mara.mdnmr, 
      dbo.mara.meins, dbo.lp23.rgb, dbo.lpd5.icon
FROM dbo.t023 INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND 
      dbo.mara.mtart = dbo.lpd5.mtart INNER JOIN
      dbo.t134t ON dbo.lpd5.mandt = dbo.t134t.mandt AND 
      dbo.makt.spras = dbo.t134t.spras AND dbo.lpd5.mtart = dbo.t134t.mtart INNER JOIN
      dbo.lpd4 ON dbo.lpd5.mandt = dbo.lpd4.mandt AND 
      dbo.lpd5.clsnr = dbo.lpd4.clsnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr ON dbo.t023.mandt = dbo.mara.mandt AND 
      dbo.t023.matkl = dbo.mara.matkl AND dbo.t023t.spras = dbo.makt.spras INNER JOIN
      dbo.lp23 ON dbo.t023.mandt = dbo.lp23.mandt AND 
      dbo.t023.matkl = dbo.lp23.matkl INNER JOIN
      dbo.mseg ON dbo.mara.mandt = dbo.mseg.mandt AND 
      dbo.mara.matnr = dbo.mseg.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_mara
AS
SELECT dbo.mara.mandt, dbo.t006a.spras, dbo.mara.matnr, dbo.lpd1.sztxt, dbo.mara.mtart, 
      dbo.mara.matkl, dbo.lpd1.sznmr, dbo.mpd1.crnmr, dbo.mpd1.crtxt, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mpmod.meins, dbo.mpmod.pricpo, 
      dbo.mpmod.waers, dbo.mpmod.eisbe, dbo.t006a.mseht, dbo.mpmod.bdnmr, 
      dbo.mpbrd.bdtxt, dbo.mpmod.bklas, dbo.mpmod.mtpos, dbo.mpmod.ktgrm, 
      dbo.mpmod.vprsv, dbo.mpmod.kkref, dbo.mpmod.spart, dbo.mpmod.xmcng, 
      dbo.mpmod.xchpf, dbo.mpmod.pricsd, dbo.mpmod.mwskz, dbo.mpmod.lifnr, 
      dbo.mpmod.ltext, dbo.mpmod.ernam, dbo.mpmod.erdat, dbo.mpmod.alwjf, 
      dbo.mpmod.eisae, dbo.mpmod.alwzk, dbo.mpmod.alwgj, dbo.mpmod.xsbdj, 
      dbo.mpmod.tjflg, dbo.mpmod.tjpt, dbo.mpmod.tjhy, dbo.mpmod.tjjf, dbo.mpmod.tjdtf, 
      dbo.mpmod.tjdtt, dbo.mpmod.xfjf, dbo.mpmod.rpsy, dbo.mpmod.verpr, 
      dbo.mpmod.cmpnd, dbo.mpmod.modnr
FROM dbo.mara INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr INNER JOIN
      dbo.t006 ON dbo.mara.mandt = dbo.t006.mandt AND 
      dbo.mara.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.mpbrd ON dbo.mpmod.mandt = dbo.mpbrd.mandt AND 
      dbo.mpmod.bdnmr = dbo.mpbrd.bdnmr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_mard
AS
SELECT dbo.mard.mandt, dbo.t006a.spras, dbo.mard.matnr, dbo.mard.werks, 
      dbo.mard.lgort, dbo.mpmod.modnr, dbo.mpmod.stext, dbo.mpmod.eisbe, 
      dbo.mard.labst, dbo.mard.umlme, dbo.t001l.lgobe, dbo.mpmod.barcode, 
      dbo.lpd1.sztxt, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sznmr, dbo.mpd1.crnmr, 
      dbo.t006a.mseht, dbo.mpmod.cmpnd
FROM dbo.mara INNER JOIN
      dbo.mard ON dbo.mara.mandt = dbo.mard.mandt AND 
      dbo.mara.matnr = dbo.mard.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.t006 ON dbo.mpmod.mandt = dbo.t006.mandt AND 
      dbo.mpmod.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_mbew
AS
SELECT dbo.mbew.mandt, dbo.t006a.spras, dbo.mpmod.modnr, dbo.mpmod.stext, 
      dbo.lpd1.sznmr, dbo.lpd1.sztxt, dbo.t006a.mseht, dbo.mbew.matnr, dbo.mbew.lbkum, 
      dbo.mbew.salk3, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.mara.mtart, dbo.t134t.mtbez, 
      dbo.t001.waers
FROM dbo.t006 INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.mbew INNER JOIN
      dbo.mara ON dbo.mbew.mandt = dbo.mara.mandt AND 
      dbo.mbew.matnr = dbo.mara.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpd1 ON dbo.mara.crnmr = dbo.mpd1.crnmr AND 
      dbo.mara.mandt = dbo.mpd1.mandt INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr ON dbo.t006.mandt = dbo.mara.mandt AND 
      dbo.t006.msehi = dbo.mara.meins INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.t006a.spras = dbo.t134t.spras INNER JOIN
      dbo.t001k ON dbo.mbew.mandt = dbo.t001k.mandt AND 
      dbo.mbew.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_mseg
AS
SELECT dbo.mseg.mandt, dbo.t156t.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bwart, dbo.t156t.btext, dbo.mpmod.modnr, 
      dbo.mseg.matnr, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sztxt, dbo.mseg.lgort, 
      dbo.t001l.lgobe, dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, 
      dbo.mseg.menge, dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, 
      dbo.t006a.mseht, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.sgtxt, dbo.mseg.bukrs, 
      dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.umwrk, dbo.mseg.umlgo, 
      dbo.mseg.charg, dbo.mseg.werks, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mara.crnmr, dbo.mara.size1, 
      dbo.mara.mtart
FROM dbo.mseg INNER JOIN
      dbo.t156t ON dbo.mseg.mandt = dbo.t156t.mandt AND 
      dbo.mseg.bwart = dbo.t156t.bwart AND dbo.mseg.sobkz = dbo.t156t.sobkz AND 
      dbo.mseg.kzbew = dbo.t156t.kzbew AND dbo.mseg.kzvbr = dbo.t156t.kzvbr AND 
      dbo.mseg.kzzug = dbo.t156t.kzzug INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t156t.spras = dbo.t006a.spras INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND dbo.mara.mandt = dbo.lpd1.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_mseg_d
AS
SELECT dbo.mseg.mandt, dbo.t156t.spras, dbo.mseg.mblnr, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.bwart, dbo.t156t.btext, dbo.mpmod.modnr, 
      dbo.mseg.matnr, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sztxt, dbo.mseg.lgort, 
      dbo.t001l.lgobe, dbo.mseg.shkzg, dbo.mseg.waers, dbo.mseg.dmbtr, 
      dbo.mseg.menge, dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, 
      dbo.t006a.mseht, dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.sgtxt, dbo.mseg.bukrs, 
      dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.umwrk, dbo.mseg.umlgo, 
      dbo.mseg.charg, dbo.mseg.werks, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mpmod.barcode, dbo.mpmod.stext, dbo.mara.crnmr, dbo.mara.size1, 
      dbo.mara.mtart, dbo.mseg.kunnr, dbo.kna1.name1
FROM dbo.mseg INNER JOIN
      dbo.t156t ON dbo.mseg.mandt = dbo.t156t.mandt AND 
      dbo.mseg.bwart = dbo.t156t.bwart AND dbo.mseg.sobkz = dbo.t156t.sobkz AND 
      dbo.mseg.kzbew = dbo.t156t.kzbew AND dbo.mseg.kzvbr = dbo.t156t.kzvbr AND 
      dbo.mseg.kzzug = dbo.t156t.kzzug INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND 
      dbo.t156t.spras = dbo.t006a.spras INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.matnr = dbo.mpmod.modnr INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.size1 = dbo.lpd1.sznmr AND 
      dbo.mara.mandt = dbo.lpd1.mandt INNER JOIN
      dbo.kna1 ON dbo.mseg.mandt = dbo.kna1.mandt AND 
      dbo.mseg.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_np_vbap_d
AS
SELECT dbo.vbap.mandt, dbo.t077x.spras, dbo.vbap.vbeln, dbo.vbap.posnr, 
      dbo.tvap.shkzg, dbo.vbap.kwmeng, dbo.vbap.netpr, dbo.vbap.netwr, dbo.vbap.waerk, 
      dbo.vbak.kunnr, dbo.kna1.ktokd, dbo.t077x.txt30, dbo.vbak.audat, dbo.vbak.erdat, 
      dbo.vbak.erzet, dbo.vbak.aedat
FROM dbo.tvap INNER JOIN
      dbo.vbap ON dbo.tvap.mandt = dbo.vbap.mandt AND 
      dbo.tvap.pstyv = dbo.vbap.pstyv INNER JOIN
      dbo.vbak ON dbo.vbap.mandt = dbo.vbak.mandt AND 
      dbo.vbap.vbeln = dbo.vbak.vbeln INNER JOIN
      dbo.kna1 ON dbo.vbak.mandt = dbo.kna1.mandt AND 
      dbo.vbak.kunnr = dbo.kna1.kunnr INNER JOIN
      dbo.t077d ON dbo.kna1.mandt = dbo.t077d.mandt AND 
      dbo.kna1.ktokd = dbo.t077d.ktokd INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_npmarp
AS
SELECT dbo.npmarp.mandt, dbo.npmarp.mptnr, dbo.npmark.peiph, dbo.npmark.meiph, 
      dbo.npmark.ernam, dbo.npmark.erdat, dbo.npmarp.matnr, dbo.mpmod.stext, 
      dbo.npmarp.peint, dbo.npmarp.meint, dbo.mpmod.meins
FROM dbo.npmark INNER JOIN
      dbo.npmarp ON dbo.npmark.mandt = dbo.npmarp.mandt AND 
      dbo.npmark.mptnr = dbo.npmarp.mptnr INNER JOIN
      dbo.mpmod ON dbo.npmarp.mandt = dbo.mpmod.mandt AND 
      dbo.npmarp.matnr = dbo.mpmod.modnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_npmarp_189i
AS
SELECT dbo.npmarp.mandt, dbo.npmarp.mptnr, dbo.npmarp.matnr, dbo.lp189i.pltyp, 
      dbo.npmarp.peint, dbo.npmarp.meint, dbo.lp189i.meinh, dbo.lp189i.peinh, 
      dbo.lp189i.price, dbo.lp189i.waers, dbo.mpmod.stext, dbo.mpmod.xfjf, 
      dbo.mpmod.rpsy, dbo.mpmod.pricpo
FROM dbo.lp189i INNER JOIN
      dbo.npmarp ON dbo.lp189i.mandt = dbo.npmarp.mandt AND 
      dbo.lp189i.matnr = dbo.npmarp.matnr INNER JOIN
      dbo.mpmod ON dbo.npmarp.mandt = dbo.mpmod.mandt AND 
      dbo.npmarp.matnr = dbo.mpmod.modnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_npmarp_po
AS
SELECT dbo.npmarp.mandt, dbo.t006a.spras, dbo.npmarp.mptnr, dbo.npmarp.matnr, 
      dbo.npmarp.peint, dbo.npmarp.meint, dbo.mpmod.stext, dbo.mpmod.xfjf, 
      dbo.mpmod.rpsy, dbo.mpmod.pricpo, dbo.mpmod.meins, dbo.mpmod.waers, 
      dbo.t006a.mseht
FROM dbo.npmarp INNER JOIN
      dbo.mpmod ON dbo.npmarp.mandt = dbo.mpmod.mandt AND 
      dbo.npmarp.matnr = dbo.mpmod.modnr INNER JOIN
      dbo.t006 ON dbo.mpmod.mandt = dbo.t006.mandt AND 
      dbo.mpmod.meins = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_s_ekpo
AS
SELECT dbo.ekpo.mandt, dbo.t006a.spras, dbo.ekpo.ebeln, dbo.ekpo.ebelp, dbo.ekko.lifnr, 
      dbo.lfa1.name1, dbo.ekpo.txz01, dbo.ekpo.matnr, dbo.mpmod.modnr, 
      dbo.mpmod.stext, dbo.ekpo.charg, dbo.ekpo.menge, dbo.ekpo.meins, 
      dbo.ekpo.bprme, dbo.ekpo.bpumz, dbo.ekpo.bpumn, dbo.ekpo.umrez, 
      dbo.ekpo.umren, dbo.ekpo.netpr, dbo.ekpo.peinh, dbo.ekpo.netwr, dbo.mpmod.mtart, 
      dbo.ekpo.bukrs, dbo.ekpo.werks, dbo.ekpo.lgort, dbo.ekko.ernam, dbo.ekko.aedat, 
      dbo.t006a.mseht, dbo.ekko.ekgrp, dbo.ekko.ekorg, dbo.ekko.waers, dbo.t001l.lgobe, 
      dbo.t024.eknam, dbo.mara.crnmr, dbo.mpd1.crtxt, dbo.mpd1.rgb, dbo.lpd1.sznmr, 
      dbo.lpd1.sztxt, dbo.mpmod.bdnmr, dbo.mpbrd.bdtxt, dbo.mpmod.ltext
FROM dbo.t006 INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi INNER JOIN
      dbo.ekko INNER JOIN
      dbo.ekpo ON dbo.ekko.mandt = dbo.ekpo.mandt AND 
      dbo.ekko.ebeln = dbo.ekpo.ebeln INNER JOIN
      dbo.lfa1 ON dbo.ekko.mandt = dbo.lfa1.mandt AND 
      dbo.ekko.lifnr = dbo.lfa1.lifnr INNER JOIN
      dbo.mara ON dbo.ekpo.mandt = dbo.mara.mandt AND 
      dbo.ekpo.matnr = dbo.mara.matnr INNER JOIN
      dbo.mpmod ON dbo.mara.mandt = dbo.mpmod.mandt AND 
      dbo.mara.mdnmr = dbo.mpmod.modnr ON dbo.t006.mandt = dbo.ekpo.mandt AND 
      dbo.t006.msehi = dbo.ekpo.meins INNER JOIN
      dbo.t001l ON dbo.ekpo.mandt = dbo.t001l.mandt AND 
      dbo.ekpo.werks = dbo.t001l.werks AND dbo.ekpo.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t024 ON dbo.ekko.mandt = dbo.t024.mandt AND 
      dbo.ekko.ekgrp = dbo.t024.ekgrp INNER JOIN
      dbo.mpd1 ON dbo.mara.mandt = dbo.mpd1.mandt AND 
      dbo.mara.crnmr = dbo.mpd1.crnmr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.mpbrd ON dbo.mpmod.mandt = dbo.mpbrd.mandt AND 
      dbo.mpmod.bdnmr = dbo.mpbrd.bdnmr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_ska1
AS
SELECT dbo.ska1.mandt, dbo.ska1.ktopl, dbo.ska1.saknr, dbo.skat.spras, dbo.skat.txt50, 
      dbo.ska1.hzflg, dbo.ska1.parnr, dbo.ska1.cleve, dbo.ska1.ktoks, dbo.t077z.txt30, 
      dbo.t077s.sclass, dbo.ska1.yedir, dbo.ska1.wlhs, dbo.ska1.slhs, dbo.ska1.xjkm, 
      dbo.ska1.yhkm, dbo.ska1.rjzkm, dbo.ska1.xjdjw, dbo.ska1.mcod1
FROM dbo.ska1 INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.ktopl = dbo.skat.ktopl AND dbo.ska1.saknr = dbo.skat.saknr INNER JOIN
      dbo.t077s ON dbo.ska1.mandt = dbo.t077s.mandt AND 
      dbo.ska1.ktopl = dbo.t077s.ktopl AND dbo.ska1.ktoks = dbo.t077s.ktoks INNER JOIN
      dbo.t077z ON dbo.t077s.mandt = dbo.t077z.mandt AND 
      dbo.t077s.ktopl = dbo.t077z.ktopl AND dbo.t077s.ktoks = dbo.t077z.ktoks AND 
      dbo.skat.spras = dbo.t077z.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001c
AS
SELECT mandt, bukrs, butxt, chkperiod, ort01
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_t077d
AS
SELECT dbo.t077d.mandt, dbo.t077x.spras, dbo.t077d.ktokd, dbo.t077x.txt30, 
      dbo.t077dmp.type
FROM dbo.t077d INNER JOIN
      dbo.t077dmp ON dbo.t077d.mandt = dbo.t077dmp.mandt AND 
      dbo.t077d.ktokd = dbo.t077dmp.ktokd INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_test1
AS
SELECT mandt, aufnr, posnr, psobs, qunum, qupos, projn, plnum, strmp, etrmp, kdauf, 
      kdpos, uebtk, untto, insmk, wepos
FROM dbo.afpo
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_tvbur
AS
SELECT dbo.tvbur.mandt, dbo.tvkbt.spras, dbo.tvbur.vkbur, dbo.tvkbt.vkburx, 
      dbo.tvbur.adrnr, dbo.tvbur.ernam, dbo.tvbur.txnam_adr, dbo.tvbur.txnam_kop, 
      dbo.tvbur.txnam_fus, dbo.tvbur.txnam_gru, dbo.tvbur.txnam_sdb, dbo.tvburdef.saknr, 
      dbo.tvburdef.vkorg, dbo.tvburdef.vtweg, dbo.tvburdef.spart, dbo.tvburdef.werks, 
      dbo.tvburdef.lgort, dbo.tvburdef.kunnr, dbo.tvburdef.kunst, dbo.tvburdef.werst, 
      dbo.tvburdef.lgost, dbo.tvburdef.sakst, dbo.tvburdef.auart, dbo.tvburdef.auast, 
      dbo.tvburp.land1, dbo.tvburp.regio, dbo.tvburp.cityc, dbo.tvburp.pstlz, 
      dbo.tvburp.admin, dbo.tvburp.address, dbo.tvburp.tel1, dbo.tvburp.tel2, 
      dbo.tvburp.title, dbo.tvburp.footer1, dbo.tvburp.footer2, dbo.tvburp.footer3, 
      dbo.tvburp.copys, dbo.tvburp.width, dbo.tvburp.ttfnt, dbo.tvburp.ctfnt, dbo.tvburp.hdfnt, 
      dbo.tvburp.ttsize, dbo.tvburp.hdsize, dbo.tvburp.ctsize, dbo.tvburp.leftm, 
      dbo.tvburp.rightm, dbo.tvburp.topm, dbo.tvburp.bottom, dbo.kna1.name1, 
      dbo.knvv.waers, dbo.tvburp.pnttyp, dbo.tvburp.pntdrv
FROM dbo.tvbur INNER JOIN
      dbo.tvburdef ON dbo.tvbur.mandt = dbo.tvburdef.mandt AND 
      dbo.tvbur.vkbur = dbo.tvburdef.vkbur INNER JOIN
      dbo.tvkbt ON dbo.tvbur.mandt = dbo.tvkbt.mandt AND 
      dbo.tvbur.vkbur = dbo.tvkbt.vkbur INNER JOIN
      dbo.tvburp ON dbo.tvbur.mandt = dbo.tvburp.mandt AND 
      dbo.tvbur.vkbur = dbo.tvburp.vkbur INNER JOIN
      dbo.kna1 ON dbo.tvbur.mandt = dbo.kna1.mandt AND 
      dbo.tvburdef.kunnr = dbo.kna1.kunnr INNER JOIN
      dbo.knvv ON dbo.tvburdef.mandt = dbo.knvv.mandt AND 
      dbo.tvburdef.kunnr = dbo.knvv.kunnr AND dbo.tvburdef.vkorg = dbo.knvv.vkorg AND 
      dbo.tvburdef.vtweg = dbo.knvv.vtweg AND dbo.tvburdef.spart = dbo.knvv.spart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_tvbur_prc
AS
SELECT dbo.tvburdef.mandt, dbo.tvkbt.spras, dbo.tvburdef.vkorg, dbo.tvburdef.vtweg, 
      dbo.tvburdef.spart, dbo.tvburdef.vkbur, dbo.tvburdef.saknr, dbo.tvburdef.kunnr, 
      dbo.knvv.pltyp, dbo.tvkbt.vkburx, dbo.t189.reflg, dbo.t189.replt, dbo.t189.ratio, 
      dbo.t189t.ptext, dbo.kna1.name1
FROM dbo.tvburdef INNER JOIN
      dbo.knvv ON dbo.tvburdef.mandt = dbo.knvv.mandt AND 
      dbo.tvburdef.vkorg = dbo.knvv.vkorg AND dbo.tvburdef.vtweg = dbo.knvv.vtweg AND 
      dbo.tvburdef.spart = dbo.knvv.spart AND 
      dbo.tvburdef.kunnr = dbo.knvv.kunnr INNER JOIN
      dbo.tvkbt ON dbo.tvburdef.mandt = dbo.tvkbt.mandt AND 
      dbo.tvburdef.vkbur = dbo.tvkbt.vkbur INNER JOIN
      dbo.t189 ON dbo.knvv.mandt = dbo.t189.mandt AND 
      dbo.knvv.pltyp = dbo.t189.pltyp INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND 
      dbo.t189.pltyp = dbo.t189t.pltyp AND dbo.tvkbt.spras = dbo.t189t.spras INNER JOIN
      dbo.kna1 ON dbo.knvv.mandt = dbo.kna1.mandt AND dbo.knvv.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_tvburacc
AS
SELECT dbo.tvburacc.mandt, dbo.skat.spras, dbo.tvburacc.vkbur, dbo.tvburacc.ktopl, 
      dbo.tvburacc.saknr, dbo.skat.txt50
FROM dbo.tvburacc INNER JOIN
      dbo.skat ON dbo.tvburacc.mandt = dbo.skat.mandt AND 
      dbo.tvburacc.ktopl = dbo.skat.ktopl AND dbo.tvburacc.saknr = dbo.skat.saknr


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_tvburdef
AS
SELECT dbo.tvburdef.mandt, dbo.tvburdef.vkbur, dbo.tvburdef.vkorg, dbo.tvburdef.vtweg, 
      dbo.tvburdef.spart, dbo.tvburdef.werks, dbo.tvburdef.lgort, dbo.tvburdef.kunnr, 
      dbo.tvburdef.kunst, dbo.t001l.lgobe, dbo.tvburdef.werst, dbo.tvburdef.lgost, 
      dbo.tvburdef.sakst, dbo.tvburdef.saknryhk, dbo.tvburdef.saknr
FROM dbo.tvburdef INNER JOIN
      dbo.t001l ON dbo.tvburdef.mandt = dbo.t001l.mandt AND 
      dbo.tvburdef.lgort = dbo.t001l.lgort AND 
      dbo.tvburdef.werks = dbo.t001l.werks INNER JOIN
      dbo.t001w ON dbo.tvburdef.mandt = dbo.t001w.mandt AND 
      dbo.tvburdef.werks = dbo.t001w.werks INNER JOIN
      dbo.t001k ON dbo.t001w.mandt = dbo.t001k.mandt AND 
      dbo.t001w.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_tvburp
AS
SELECT dbo.tvburp.mandt, dbo.tvkbt.spras, dbo.tvburp.vkbur, dbo.tvkbt.vkburx, 
      dbo.tvburp.land1, dbo.tvburp.regio, dbo.tvburp.cityc, dbo.tvburp.pstlz, 
      dbo.tvburp.admin, dbo.tvburp.title, dbo.tvburp.address, dbo.tvburp.tel1, 
      dbo.tvburp.tel2, dbo.t005t.landx, dbo.t005u.blandx, dbo.t005h.citycx, 
      dbo.tvburp.footer1, dbo.tvburp.footer2, dbo.tvburp.footer3, dbo.tvburp.width, 
      dbo.tvburp.copys, dbo.tvburp.ttfnt, dbo.tvburp.hdfnt, dbo.tvburp.ctfnt, 
      dbo.tvburp.ttsize, dbo.tvburp.hdsize, dbo.tvburp.ctsize, dbo.tvburp.leftm, 
      dbo.tvburp.rightm, dbo.tvburp.topm, dbo.tvburp.bottom, dbo.tvburp.pnttyp, 
      dbo.tvburp.pntdrv
FROM dbo.t005t INNER JOIN
      dbo.t005 ON dbo.t005t.mandt = dbo.t005.mandt AND 
      dbo.t005t.land1 = dbo.t005.land1 INNER JOIN
      dbo.t005s INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND 
      dbo.t005s.bland = dbo.t005u.bland INNER JOIN
      dbo.t005h INNER JOIN
      dbo.t005g ON dbo.t005h.mandt = dbo.t005g.mandt AND 
      dbo.t005h.land1 = dbo.t005g.land1 AND dbo.t005h.regio = dbo.t005g.regio AND 
      dbo.t005h.cityc = dbo.t005g.cityc ON dbo.t005u.spras = dbo.t005h.spras ON 
      dbo.t005t.spras = dbo.t005u.spras INNER JOIN
      dbo.tvbur INNER JOIN
      dbo.tvburp ON dbo.tvbur.mandt = dbo.tvburp.mandt AND 
      dbo.tvbur.vkbur = dbo.tvburp.vkbur INNER JOIN
      dbo.tvkbt ON dbo.tvbur.mandt = dbo.tvkbt.mandt AND 
      dbo.tvbur.vkbur = dbo.tvkbt.vkbur ON dbo.t005t.spras = dbo.tvkbt.spras AND 
      dbo.t005s.mandt = dbo.tvburp.mandt AND dbo.t005.mandt = dbo.tvburp.mandt AND 
      dbo.t005g.mandt = dbo.tvburp.mandt AND dbo.t005.land1 = dbo.tvburp.land1 AND 
      dbo.t005s.land1 = dbo.tvburp.land1 AND dbo.t005s.bland = dbo.tvburp.regio AND 
      dbo.t005g.land1 = dbo.tvburp.land1 AND dbo.t005g.regio = dbo.tvburp.regio AND 
      dbo.t005g.cityc = dbo.tvburp.cityc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.l_usr01
AS
SELECT dbo.usr01.mandt, dbo.usr01.bname, dbo.usr01.stcod, dbo.usr01.spld, 
      dbo.usrlp.gpnmr, dbo.usrlp.vkbur, dbo.usr01.splg, dbo.usr01.spdb, dbo.usr01.spda, 
      dbo.usr01.datfm, dbo.usr01.dcpfm, dbo.usr01.hdest, dbo.usr01.hmand, 
      dbo.usr01.hname, dbo.usr01.menon, dbo.usr01.menuver, dbo.usr01.menue, 
      dbo.usr01.strtt, dbo.usr01.langu, dbo.usr01.cattkennz, dbo.usr01.cfg1, dbo.usr01.cfg2, 
      dbo.usr01.cfg3, dbo.usr01.cfg4, dbo.usr01.cfg5, dbo.usr01.locked, dbo.usr02.bcode, 
      dbo.usr02.gltgv, dbo.usr02.gltgb, dbo.usr02.ustyp, dbo.usr02.class, dbo.usr02.locnt, 
      dbo.usr02.uflag, dbo.usr02.accnt, dbo.usr02.aname, dbo.usr02.erdat, dbo.usr02.trdat, 
      dbo.usr02.ltime, dbo.usr02.ocod1, dbo.usr02.bcda1, dbo.usr02.codv1, 
      dbo.usr02.ocod2, dbo.usr02.bcda2, dbo.usr02.codv2, dbo.usr02.ocod3, 
      dbo.usr02.bcda3, dbo.usr02.codv3, dbo.usr02.ocod4, dbo.usr02.bcda4, 
      dbo.usr02.codv4, dbo.usr02.ocod5, dbo.usr02.bcda5, dbo.usr02.codv5, 
      dbo.usr02.versn, dbo.usr02.codvn, dbo.usr02.tzone, dbo.usr02.zbvmaster, 
      dbo.usr03.name1, dbo.usr03.name2, dbo.usr03.name3, dbo.usr03.name4, 
      dbo.usr03.salut, dbo.usr03.abtlg, dbo.usr03.kostl, dbo.usr03.buinr, dbo.usr03.roonr, 
      dbo.usr03.stras, dbo.usr03.pfach, dbo.usr03.pstlz, dbo.usr03.ort01, dbo.usr03.regio, 
      dbo.usr03.land1, dbo.usr03.spras, dbo.usr03.telpr, dbo.usr03.telnr, dbo.usr03.tel01, 
      dbo.usr03.tel02, dbo.usr03.telx1, dbo.usr03.telfx, dbo.usr03.teltx, dbo.usr03.ort02, 
      dbo.usr03.pstl2
FROM dbo.usrlp INNER JOIN
      dbo.usr01 ON dbo.usrlp.mandt = dbo.usr01.mandt AND 
      dbo.usrlp.bname = dbo.usr01.bname INNER JOIN
      dbo.usr02 ON dbo.usr01.mandt = dbo.usr02.mandt AND 
      dbo.usr01.bname = dbo.usr02.bname INNER JOIN
      dbo.usr03 ON dbo.usr01.mandt = dbo.usr03.mandt AND 
      dbo.usr01.bname = dbo.usr03.bname

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_vbak
AS
SELECT dbo.vbak.mandt, dbo.tvakt.spras, dbo.vbak.bstdk, dbo.tvkbt.vkburx, 
      dbo.tvakt.auartx, dbo.vbak.kunnr, dbo.vbak.ernam, dbo.vbak.auart, dbo.vbak.vbeln, 
      dbo.vbak.erdat, dbo.vbak.bstnk, dbo.vbak.audat, dbo.vbak.vdatu, dbo.vbak.vkorg, 
      dbo.vbak.vtweg, dbo.vbak.spart, dbo.vbak.vkbur, dbo.vbak.vkgrp, dbo.vbak.augru, 
      dbo.vbak.erzet
FROM dbo.vbak INNER JOIN
      dbo.tvak ON dbo.vbak.mandt = dbo.tvak.mandt AND 
      dbo.vbak.auart = dbo.tvak.auart INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvbur ON dbo.vbak.mandt = dbo.tvbur.mandt AND 
      dbo.vbak.vkbur = dbo.tvbur.vkbur INNER JOIN
      dbo.tvkbt ON dbo.tvbur.mandt = dbo.tvkbt.mandt AND 
      dbo.tvbur.vkbur = dbo.tvkbt.vkbur AND dbo.tvakt.spras = dbo.tvkbt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.l_vbfa_vbap
AS
SELECT dbo.vbfa.mandt, dbo.vbfa.vbelv, dbo.vbfa.posnv, dbo.vbfa.vbeln, dbo.vbfa.posnn, 
      dbo.vbfa.vbtyp_n, dbo.vbfa.rfmng, dbo.vbfa.meins, dbo.vbfa.rfwrt, dbo.vbfa.waers, 
      dbo.vbfa.vbtyp_v, dbo.vbap.matnr, dbo.vbap.vrkme, dbo.vbap.netwr, 
      dbo.vbap.kwmeng, dbo.tvap.shkzg, dbo.vbfa.bwart, dbo.vbfa.erdat, dbo.vbfa.erzet, 
      dbo.vbap.werks, dbo.vbap.lgort
FROM dbo.vbap INNER JOIN
      dbo.vbfa ON dbo.vbap.mandt = dbo.vbfa.mandt AND 
      dbo.vbap.vbeln = dbo.vbfa.vbelv AND dbo.vbap.posnr = dbo.vbfa.posnv INNER JOIN
      dbo.tvap ON dbo.vbap.mandt = dbo.tvap.mandt AND dbo.vbap.pstyv = dbo.tvap.pstyv

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_debid
AS
SELECT dbo.kna1.mandt, dbo.kna1.kunnr, dbo.kna1.sortl, dbo.kna1.pstlz, dbo.kna1.name1, 
      dbo.kna1.name2, dbo.knb1.bukrs
FROM dbo.kna1 INNER JOIN
      dbo.knb1 ON dbo.kna1.mandt = dbo.knb1.mandt AND dbo.kna1.kunnr = dbo.knb1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_debis
AS
SELECT dbo.knvv.mandt, dbo.knvv.kunnr, dbo.knvv.vkorg, dbo.knvv.vtweg, dbo.knvv.spart, 
      dbo.knvv.vkbur, dbo.knvv.vkgrp, dbo.kna1.pstlz, dbo.kna1.sortl, dbo.kna1.name1, 
      dbo.kna1.name2
FROM dbo.knvv INNER JOIN
      dbo.kna1 ON dbo.knvv.mandt = dbo.kna1.mandt AND dbo.knvv.kunnr = dbo.kna1.kunnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_krede
AS
SELECT dbo.lfa1.mandt, dbo.lfa1.lifnr, dbo.lfa1.sortl, dbo.lfa1.pstlz, dbo.lfa1.name1, 
      dbo.lfa1.name2, dbo.lfm1.ekorg, dbo.lfm1.bolre
FROM dbo.lfa1 INNER JOIN
      dbo.lfm1 ON dbo.lfa1.mandt = dbo.lfm1.mandt AND dbo.lfa1.lifnr = dbo.lfm1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_kredk
AS
SELECT dbo.lfa1.mandt, dbo.lfa1.sortl, dbo.lfa1.pstlz, dbo.lfa1.name1, dbo.lfa1.name2, 
      dbo.lfa1.lifnr, dbo.lfb1.bukrs
FROM dbo.lfa1 INNER JOIN
      dbo.lfb1 ON dbo.lfa1.mandt = dbo.lfb1.mandt AND dbo.lfa1.lifnr = dbo.lfb1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_likp
AS
SELECT mandt, vbeln, vstel, kodat, tddat, lddat, wadat, lfdat, lifsk, lstel, kunnr, anzpk, 
      ernam, erdat
FROM dbo.likp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mard_l
AS
SELECT dbo.mard.mandt, dbo.mard.matnr, dbo.mard.werks, dbo.mard.lgort, 
      dbo.t001l.lgobe, dbo.t001l.spart, dbo.t001l.xlong, dbo.t001l.xbufx, dbo.t001l.diskz, 
      dbo.t001l.xblgo, dbo.t001l.xress
FROM dbo.mard INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1b
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.mast.werks, dbo.mast.stlan, 
      dbo.makt.matnr, dbo.mast.stlal, dbo.makt.spras
FROM dbo.mast INNER JOIN
      dbo.makt ON dbo.mast.mandt = dbo.makt.mandt AND 
      dbo.mast.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1i
AS
SELECT dbo.eina.mandt, dbo.eina.idnlf, dbo.eina.lifnr, dbo.makt.matnr, dbo.makt.maktg, 
      dbo.makt.spras
FROM dbo.makt INNER JOIN
      dbo.eina ON dbo.makt.mandt = dbo.eina.mandt AND dbo.makt.matnr = dbo.eina.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1n
AS
SELECT dbo.mean.mandt, dbo.mean.ean11, dbo.mean.matnr, dbo.makt.maktg, 
      dbo.makt.spras, dbo.mean.meinh, dbo.mean.hpean
FROM dbo.mean INNER JOIN
      dbo.makt ON dbo.mean.mandt = dbo.makt.mandt AND 
      dbo.mean.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1r
AS
SELECT dbo.makt.mandt, dbo.makt.maktg, dbo.makt.spras, dbo.makt.matnr, 
      dbo.mapl.werks, dbo.mapl.plnty, dbo.mapl.plnnr, dbo.mapl.plnal, 
      dbo.mapl.datuv
FROM dbo.makt INNER JOIN
      dbo.mapl ON dbo.makt.mandt = dbo.mapl.mandt AND 
      dbo.makt.matnr = dbo.mapl.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_mat1v
AS
SELECT dbo.pkhd.mandt, dbo.pkhd.prvbe, dbo.mara.matnr, dbo.marc.werks, 
      dbo.makt.maktg, dbo.makt.spras
FROM dbo.pkhd INNER JOIN
      dbo.makt INNER JOIN
      dbo.mara ON dbo.makt.mandt = dbo.mara.mandt AND 
      dbo.makt.matnr = dbo.mara.matnr INNER JOIN
      dbo.marc ON dbo.mara.mandt = dbo.marc.mandt AND 
      dbo.mara.matnr = dbo.marc.matnr ON dbo.pkhd.mandt = dbo.mara.mandt AND 
      dbo.pkhd.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_usr03
AS
SELECT mandt, bname, name1, abtlg, buinr, kostl, roonr, stras, pfach, pstlz, ort01, regio, 
      land1, spras, telpr, telnr, tel01, telx1, tel02, telfx, teltx, ort02, pstl2, tzone, salut
FROM dbo.usr03

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_vmcfa
AS
SELECT mandt, vkorg, kunrg, fktyp, fkart, ernam, erdat, kunag, vbeln, rfbsk, fkdat, 
      vbtyp
FROM dbo.vbrk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.m_vmvaa
AS
SELECT mandt, bstdk, vkorg, kunnr, vtweg, spart, vkbur, vkgrp, ernam, auart, vbeln, erdat, 
      bstnk, audat, vdatu
FROM dbo.vbak

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.r_vbap_a
AS
SELECT dbo.vbap.mandt, dbo.vbap.vbeln, dbo.vbap.posnr, dbo.vbap.matnr, 
      dbo.makt.maktg, dbo.vbap.netpr, dbo.vbap.waerk, dbo.vbap.smeng, dbo.t006a.mseht, 
      dbo.vbap.matkl, dbo.vbap.spart, dbo.vbap.vkgrp, dbo.vbap.vkgrp2, dbo.vbap.werks, 
      dbo.vbap.lgort, dbo.vbap.erdat, dbo.vbap.ernam, dbo.makt.spras, 
      dbo.tvgrt.vkgrpx
FROM dbo.vbap INNER JOIN
      dbo.makt ON dbo.vbap.mandt = dbo.makt.mandt AND 
      dbo.vbap.matnr = dbo.makt.matnr INNER JOIN
      dbo.t006a ON dbo.vbap.mandt = dbo.t006a.mandt AND 
      dbo.vbap.vrkme = dbo.t006a.msehi AND 
      dbo.makt.spras = dbo.t006a.spras INNER JOIN
      dbo.tvgrt ON dbo.vbap.mandt = dbo.tvgrt.mandt AND 
      dbo.makt.spras = dbo.tvgrt.spras AND dbo.vbap.vkgrp = dbo.tvgrt.vkgrp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.r_vbap_b
AS
SELECT dbo.vbap.mandt, dbo.vbap.vbeln, dbo.vbap.posnr, dbo.vbap.matnr, 
      dbo.makt.maktg, dbo.vbap.netpr, dbo.vbap.waerk, dbo.vbap.smeng, dbo.t006a.mseht, 
      dbo.vbap.matkl, dbo.vbap.spart, dbo.vbap.vkgrp, dbo.vbap.vkgrp2, dbo.vbap.werks, 
      dbo.vbap.lgort, dbo.vbap.erdat, dbo.vbap.ernam, dbo.makt.spras, 
      dbo.tvgrt.vkgrpx
FROM dbo.vbap INNER JOIN
      dbo.makt ON dbo.vbap.mandt = dbo.makt.mandt AND 
      dbo.vbap.matnr = dbo.makt.matnr INNER JOIN
      dbo.t006a ON dbo.vbap.mandt = dbo.t006a.mandt AND 
      dbo.vbap.vrkme = dbo.t006a.msehi AND 
      dbo.makt.spras = dbo.t006a.spras INNER JOIN
      dbo.tvgrt ON dbo.vbap.mandt = dbo.tvgrt.mandt AND 
      dbo.makt.spras = dbo.tvgrt.spras AND dbo.vbap.vkgrp2 = dbo.tvgrt.vkgrp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v025
AS
SELECT dbo.t025.mandt, dbo.t025.bklas, dbo.t025t.spras, dbo.t025t.bkbez, 
      dbo.t025.kkref
FROM dbo.t025 INNER JOIN
      dbo.t025t ON dbo.t025.mandt = dbo.t025t.mandt AND dbo.t025.bklas = dbo.t025t.bklas

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_marv
AS
SELECT dbo.t001.mandt, dbo.t001.bukrs, dbo.t001.butxt, dbo.marv.lfgja, dbo.marv.lfmon, 
      dbo.marv.vmgja, dbo.marv.vmmon, dbo.marv.vjgja, dbo.marv.vjmon, 
      dbo.marv.xruem, dbo.marv.xruev, dbo.marv.vvmgj, dbo.marv.vvmmo, 
      dbo.marv.vvjgj, dbo.marv.vvjmo, dbo.marv.gja_40c, dbo.marv.mon_40c
FROM dbo.t001 INNER JOIN
      dbo.marv ON dbo.t001.bukrs = dbo.marv.bukrs AND dbo.t001.mandt = dbo.marv.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001_np
AS
SELECT mandt, bukrs, butxt, ort01, xnegp
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_001k_k
AS
SELECT dbo.t001k.mandt, dbo.t001k.bwkey, dbo.t001k.bwmod, dbo.t001k.bukrs, 
      dbo.t001.butxt, dbo.t001.ktopl
FROM dbo.t001k INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v024
AS
SELECT mandt, ekgrp, eknam, ektel, ldest, telfx, tel_number, tel_extens, smtm_addr
FROM dbo.t024

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_025k
AS
SELECT dbo.t025k.mandt, dbo.t025k.kkref, dbo.t025l.spras, dbo.t025l.krftx
FROM dbo.t025k INNER JOIN
      dbo.t025l ON dbo.t025k.mandt = dbo.t025l.mandt AND dbo.t025k.kkref = dbo.t025l.kkref

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_030a
AS
SELECT dbo.t030a.ktosl, dbo.t030w.spras, dbo.t030w.ltext, dbo.t030w.ktext, 
      dbo.t030w.ktxt2, dbo.t030w.ktxt3, dbo.t030a.grupp, dbo.t030a.tcode, dbo.t030a.f1kom, 
      dbo.t030a.f1bwm, dbo.t030a.f1bkl, dbo.t030a.f4kom, dbo.t030a.f4bwm, 
      dbo.t030a.f4bkl
FROM dbo.t030a INNER JOIN
      dbo.t030w ON dbo.t030a.ktosl = dbo.t030w.ktosl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_134_k
AS
SELECT dbo.t134.mandt, dbo.t134.mtart, dbo.t134t.spras, dbo.t134t.mtbez, 
      dbo.t134.kkref
FROM dbo.t134 INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND dbo.t134.mtart = dbo.t134t.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_134_p
AS
SELECT dbo.t134.mandt, dbo.t134.mtart, dbo.t134t.spras, dbo.t134t.mtbez, dbo.t134.vprsv, 
      dbo.t134.kzvpr
FROM dbo.t134 INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND dbo.t134.mtart = dbo.t134t.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156_vc
AS
SELECT mandt, bwart, shkzg, kzwes, kzvbu, kzdru, kzkon, rstyp, selpa, xlaut, kzstr, kzgru, 
      xinvb, kzbwa, xstbw, xpbed, xwsbr, kzmhd, kzcla, xkoko, xkcfc, chneu, rules, xoarc, 
      xnebe, bustr, kzdir
FROM dbo.t156

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156b
AS
SELECT dbo.t156b.mandt, dbo.t148t.spras, dbo.t156b.bwart, dbo.t156b.sobkz, 
      dbo.t148t.sotxt, dbo.t156b.umsok, dbo.t156b.fausw, dbo.t156b.xlist, 
      dbo.t156b.chsmm, dbo.t156b.umchs, dbo.t156b.kzchp, dbo.t156b.umchp, 
      dbo.t156b.migo_exist
FROM dbo.t148 INNER JOIN
      dbo.t148t ON dbo.t148.mandt = dbo.t148t.mandt AND 
      dbo.t148.sobkz = dbo.t148t.sobkz INNER JOIN
      dbo.t156b ON dbo.t148.mandt = dbo.t156b.mandt AND 
      dbo.t148.sobkz = dbo.t156b.sobkz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156n_vc
AS
SELECT mandt, fcode, bwart, bwart_next, xstor
FROM dbo.t156n

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156sc_vc
AS
SELECT mandt, bwart, wertu, mengu, sobkz, kzbew, kzzug, kzvbr, xverf, kzvbp, prreg, xlifo, 
      rblvs, umrbl, umrbu, anzre
FROM dbo.t156sc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156t_vc
AS
SELECT mandt, spras, bwart, sobkz, kzbew, kzzug, kzvbr, btext
FROM dbo.t156t

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156w_30
AS
SELECT dbo.t156w.bustw, dbo.t156w.cnt02, dbo.t156w.vorsl, dbo.t030w.spras, 
      dbo.t030w.ltext, dbo.t030w.ktext
FROM dbo.t030w INNER JOIN
      dbo.t156w ON dbo.t030w.ktosl = dbo.t156w.vorsl INNER JOIN
      dbo.t030a ON dbo.t030w.ktosl = dbo.t030a.ktosl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_156x_vc
AS
SELECT dbo.t156x.mandt, dbo.t156x.bwart, dbo.t156x.wertu, dbo.t156x.mengu, 
      dbo.t156x.sobkz, dbo.t156x.kzbew, dbo.t156x.kzvbr, dbo.t156x.bustw, 
      dbo.t156x.cnt02, dbo.t156x.komok, dbo.t156x.xpkon, dbo.t156w.vorsl
FROM dbo.t156x INNER JOIN
      dbo.t156w ON dbo.t156x.bustw = dbo.t156w.bustw AND 
      dbo.t156x.cnt02 = dbo.t156w.cnt02

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_157d
AS
SELECT dbo.t157d.mandt, dbo.t157e.spras, dbo.t157d.bwart, dbo.t157d.grund, 
      dbo.t157e.grtxt
FROM dbo.t157d INNER JOIN
      dbo.t157e ON dbo.t157d.mandt = dbo.t157e.mandt AND 
      dbo.t157d.bwart = dbo.t157e.bwart AND dbo.t157d.grund = dbo.t157e.grund

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_157h_8b
AS
SELECT dbo.t157h.mandt, dbo.t157h.spras, dbo.t157h.tcode, dbo.t157h.bwart, 
      dbo.t157h.sobkz, dbo.t157h.htext
FROM dbo.t157h INNER JOIN
      dbo.t158b ON dbo.t157h.mandt = dbo.t158b.mandt AND 
      dbo.t157h.tcode = dbo.t158b.tcode AND dbo.t157h.bwart = dbo.t158b.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_158
AS
SELECT dbo.t158.mandt, dbo.t158.tcode, dbo.tstct.spras, dbo.tstct.ttext, dbo.t158.blart, 
      dbo.t158.trtyp, dbo.t158.blaum, dbo.t158.vgart, dbo.t158.kzbew, dbo.t158.fcode, 
      dbo.t158.kzvar, dbo.t158.kztit, dbo.t158.xsele, dbo.t158.wever, dbo.t158.prreg, 
      dbo.t158.bla2d, dbo.t158.xarch
FROM dbo.tstc INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode INNER JOIN
      dbo.t158 ON dbo.tstc.tcode = dbo.t158.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_158b_vc
AS
SELECT dbo.t158b.mandt, dbo.tstct.spras, dbo.t158b.bwart, dbo.t158b.tcode, 
      dbo.t158b.xkzbew, dbo.tstct.ttext
FROM dbo.t158b INNER JOIN
      dbo.tstc ON dbo.t158b.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_159l_x
AS
SELECT dbo.t159l.mandt, dbo.t159l.werks, dbo.t001w.name1, dbo.t159l.xlaut
FROM dbo.t001w INNER JOIN
      dbo.t159l ON dbo.t001w.mandt = dbo.t159l.mandt AND 
      dbo.t001w.werks = dbo.t159l.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_161e
AS
SELECT mandt, frgab, frgtx
FROM dbo.t161e

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_169f
AS
SELECT dbo.t169f.mandt, dbo.tstct.spras, dbo.t169f.tcode, dbo.tstct.ttext, dbo.t169f.blart, 
      dbo.t169f.blaum, dbo.t169f.bla2d, dbo.t169f.blrek, dbo.t169f.blhsc
FROM dbo.t169f INNER JOIN
      dbo.t169 ON dbo.t169f.tcode = dbo.t169.tcode INNER JOIN
      dbo.tstc ON dbo.t169.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_169v_st
AS
SELECT dbo.t169v.mandt, dbo.t169v.bukrs, dbo.t001.butxt, dbo.t169v.vstki, 
      dbo.t169v.vstka, dbo.t169v.vstk2, dbo.t169v.vstk3, dbo.t169v.vstk4, 
      dbo.t169v.mwskz_bnk, dbo.t169v.txjcd_bnk
FROM dbo.t001 INNER JOIN
      dbo.t169v ON dbo.t001.mandt = dbo.t169v.mandt AND 
      dbo.t001.bukrs = dbo.t169v.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_bseg
AS
SELECT dbo.bseg.mandt, dbo.skat.spras, dbo.bseg.bukrs, dbo.bseg.belnr, dbo.bseg.gjahr, 
      dbo.bseg.buzei, dbo.bseg.bschl, dbo.bseg.koart, dbo.bseg.shkzg, dbo.bseg.wrbtr, 
      dbo.bseg.sgtxt, dbo.bseg.hkont, dbo.bseg.dmbtr, dbo.skat.txt20, dbo.bseg.mwskz, 
      dbo.bseg.mwart, dbo.bseg.lifnr, dbo.bseg.kunnr, dbo.bseg.ktosl, dbo.bseg.umskz, 
      dbo.bseg.kostl, dbo.bseg.kokrs, dbo.bseg.aufnr, dbo.bseg.saknr, dbo.bseg.vbeln, 
      dbo.bseg.bewar
FROM dbo.bseg INNER JOIN
      dbo.t001 ON dbo.bseg.mandt = dbo.t001.mandt AND 
      dbo.bseg.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.ska1 ON dbo.bseg.mandt = dbo.ska1.mandt AND 
      dbo.t001.ktopl = dbo.ska1.ktopl AND dbo.bseg.hkont = dbo.ska1.saknr INNER JOIN
      dbo.skat ON dbo.ska1.ktopl = dbo.skat.ktopl AND 
      dbo.ska1.saknr = dbo.skat.saknr AND dbo.ska1.mandt = dbo.skat.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c001l
AS
SELECT mandt, werks, rpanr, rpatx, xasin, lvorm, rpang
FROM dbo.c001l

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c001lm
AS
SELECT dbo.c001l.mandt, dbo.c001l.werks, dbo.c001l.rpang, dbo.c001m.rpgtx, 
      dbo.c001l.rpanr, dbo.c001l.rpatx, dbo.c001m.rpgrm, dbo.c001l.lvorm
FROM dbo.c001l INNER JOIN
      dbo.c001m ON dbo.c001l.mandt = dbo.c001m.mandt AND 
      dbo.c001l.werks = dbo.c001m.werks AND dbo.c001l.rpang = dbo.c001m.rpang

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c001m
AS
SELECT mandt, werks, rpang, rpgtx, rpgrm
FROM dbo.c001m

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c002
AS
SELECT dbo.c002l.mandt, dbo.c002l.werks, dbo.c002l.rpanr, dbo.c002l.tplnr, 
      dbo.c001l.rpatx, dbo.c001l.lvorm
FROM dbo.c001l INNER JOIN
      dbo.c002l ON dbo.c001l.mandt = dbo.c002l.mandt AND 
      dbo.c001l.werks = dbo.c002l.werks AND dbo.c001l.rpanr = dbo.c002l.rpanr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c002l_a
AS
SELECT dbo.c002l.mandt, dbo.c002l.werks, dbo.c002l.rpanr, dbo.c002l.tplnr, 
      dbo.wiflot.swerk, dbo.wiflot.lvorm
FROM dbo.c002l INNER JOIN
      dbo.wiflot ON dbo.c002l.mandt = dbo.wiflot.mandt AND 
      dbo.c002l.tplnr = dbo.wiflot.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003
AS
SELECT dbo.c003l.mandt, dbo.c003l.tplnr, dbo.c003l.qmart, dbo.tq80_t.spras, 
      dbo.tq80_t.qmartx
FROM dbo.tq80_t INNER JOIN
      dbo.c003l ON dbo.tq80_t.mandt = dbo.c003l.mandt AND 
      dbo.tq80_t.qmart = dbo.c003l.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003m_a
AS
SELECT dbo.c003m.mandt, dbo.c003m.tplnr, dbo.c003m.tplnrd, dbo.iflotx.spras, 
      dbo.iflotx.pltxt
FROM dbo.c003m INNER JOIN
      dbo.iflotx ON dbo.c003m.mandt = dbo.iflotx.mandt AND 
      dbo.c003m.tplnrd = dbo.iflotx.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003m_b
AS
SELECT dbo.c003m.mandt, dbo.c003m.tplnr, dbo.c003m.tplnrd, dbo.wiflot.swerk, 
      dbo.c002l.rpanr
FROM dbo.c003m INNER JOIN
      dbo.wiflot ON dbo.c003m.mandt = dbo.wiflot.mandt AND 
      dbo.c003m.tplnr = dbo.wiflot.tplnr INNER JOIN
      dbo.c002l ON dbo.c003m.mandt = dbo.c002l.mandt AND 
      dbo.wiflot.swerk = dbo.c002l.werks AND dbo.c003m.tplnrd = dbo.c002l.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003m_e
AS
SELECT dbo.c003m.mandt, dbo.iflotx.spras, dbo.wiflot.swerk, dbo.c003m.tplnrd, 
      dbo.iflotx.pltxt
FROM dbo.c003m INNER JOIN
      dbo.wiflot ON dbo.c003m.mandt = dbo.wiflot.mandt AND 
      dbo.c003m.tplnr = dbo.wiflot.tplnr INNER JOIN
      dbo.iflotx ON dbo.c003m.mandt = dbo.iflotx.mandt AND 
      dbo.c003m.tplnrd = dbo.iflotx.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003n
AS
SELECT dbo.c003n.mandt, dbo.c003n.tplnr, dbo.c003n.veran, dbo.tc24.ktext
FROM dbo.c003n INNER JOIN
      dbo.tc24 ON dbo.c003n.mandt = dbo.tc24.mandt AND 
      dbo.c003n.werks = dbo.tc24.werks AND dbo.c003n.veran = dbo.tc24.veran

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_c003n_a
AS
SELECT dbo.c003n.mandt, dbo.c003n.tplnr, dbo.c003n.veran, dbo.tc24.ktext, 
      dbo.tc24.isasn
FROM dbo.tc24 INNER JOIN
      dbo.c003n ON dbo.tc24.mandt = dbo.c003n.mandt AND 
      dbo.tc24.veran = dbo.c003n.veran INNER JOIN
      dbo.wiflot ON dbo.c003n.mandt = dbo.wiflot.mandt AND 
      dbo.c003n.tplnr = dbo.wiflot.tplnr AND dbo.c003n.werks = dbo.wiflot.swerk AND 
      dbo.tc24.werks = dbo.wiflot.swerk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_cn01
AS
SELECT dbo.cn01.mandt, dbo.cn01.type, dbo.cn01.versi, dbo.cn01.treid, dbo.cn01t.spras, 
      dbo.cn01.parid, dbo.cn01.flag1, dbo.cn01.fokey1, dbo.cn01.fokey2, 
      dbo.cn01t.tretxt
FROM dbo.cn01 INNER JOIN
      dbo.cn01t ON dbo.cn01.mandt = dbo.cn01t.mandt AND 
      dbo.cn01.type = dbo.cn01t.type AND dbo.cn01.versi = dbo.cn01t.versi AND 
      dbo.cn01.treid = dbo.cn01t.treid

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_cn01_ska1
AS
SELECT dbo.cn01.mandt, dbo.cn01.type, dbo.cn01.versi, dbo.cn01.treid, dbo.cn01t.spras, 
      dbo.cn01.parid, dbo.cn01.flag1, dbo.cn01t.tretxt, dbo.skat.txt20, dbo.skat.txt50, 
      dbo.ska1.ktopl, dbo.ska1.saknr, dbo.ska1.ktoks, dbo.t077s.sclass
FROM dbo.cn01 INNER JOIN
      dbo.cn01t ON dbo.cn01.mandt = dbo.cn01t.mandt AND 
      dbo.cn01.type = dbo.cn01t.type AND dbo.cn01.versi = dbo.cn01t.versi AND 
      dbo.cn01.treid = dbo.cn01t.treid INNER JOIN
      dbo.ska1 ON dbo.cn01.mandt = dbo.ska1.mandt AND 
      dbo.cn01.fokey1 = dbo.ska1.ktopl AND dbo.cn01.fokey2 = dbo.ska1.saknr INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.ktopl = dbo.skat.ktopl AND dbo.ska1.saknr = dbo.skat.saknr AND 
      dbo.cn01t.spras = dbo.skat.spras INNER JOIN
      dbo.t077s ON dbo.ska1.mandt = dbo.t077s.mandt AND 
      dbo.ska1.ktopl = dbo.t077s.ktopl AND dbo.ska1.ktoks = dbo.t077s.ktoks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_cq15
AS
SELECT dbo.cq15.mandt, dbo.cq15.katat, dbo.cq15t.spras, dbo.cq15t.katatx, 
      dbo.cq15.kacls, dbo.cq15.kalvm, dbo.cq15.ernam, dbo.cq15.erdat, dbo.cq15.aenam, 
      dbo.cq15.aedat
FROM dbo.cq15 INNER JOIN
      dbo.cq15t ON dbo.cq15.mandt = dbo.cq15t.mandt AND 
      dbo.cq15.katat = dbo.cq15t.katat

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_cq16
AS
SELECT dbo.cq16.mandt, dbo.cq16.katat, dbo.cq16.kalaf, dbo.cq16t.spras, dbo.cq16t.kalafx, 
      dbo.cq16.kflvm, dbo.cq16.qmsta
FROM dbo.cq16 INNER JOIN
      dbo.cq16t ON dbo.cq16.mandt = dbo.cq16t.mandt AND 
      dbo.cq16.katat = dbo.cq16t.katat AND dbo.cq16.kalaf = dbo.cq16t.kalaf

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_df41s
AS
SELECT dbo.df41s.node_id, dbo.df41s.node_typ, dbo.df41s.version, dbo.df41t.spras, 
      dbo.df41t.tname, dbo.df41s.parent_id, dbo.df41s.cflag, dbo.df41s.trancode, 
      dbo.df41s.tvalue
FROM dbo.df41s INNER JOIN
      dbo.df41t ON dbo.df41s.node_id = dbo.df41t.node_id AND 
      dbo.df41s.node_typ = dbo.df41t.node_typ AND dbo.df41s.version = dbo.df41t.version

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_df41sw
AS
SELECT dbo.df41sw.node_id, dbo.df41tw.spras, dbo.df41tw.tname, dbo.df41sw.node_typ, 
      dbo.df41sw.parent_id, dbo.df41sw.cflag, dbo.df41sw.tvalue, 
      dbo.df41sw.menu_id
FROM dbo.df41sw INNER JOIN
      dbo.df41tw ON dbo.df41sw.node_id = dbo.df41tw.node_id AND 
      dbo.df41sw.menu_id = dbo.df41tw.menu_id

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ekko
AS
SELECT dbo.ekko.mandt, dbo.ekko.bsart, dbo.ekko.ebeln, dbo.ekko.bukrs, dbo.ekko.lifnr, 
      dbo.lfa1.name1, dbo.ekko.bstyp, dbo.ekko.aedat, dbo.ekko.ernam, dbo.ekko.ekorg, 
      dbo.ekko.ekgrp, dbo.ekko.waers, dbo.ekko.bedat
FROM dbo.ekko INNER JOIN
      dbo.lfa1 ON dbo.ekko.mandt = dbo.lfa1.mandt AND dbo.ekko.lifnr = dbo.lfa1.lifnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_fm01_a
AS
SELECT dbo.fm01.mandt, dbo.fm01.fikrs, dbo.fm01t.spras, dbo.fm01t.fitxt, dbo.fm01.objnr, 
      dbo.fm01.waers
FROM dbo.fm01 INNER JOIN
      dbo.fm01t ON dbo.fm01.mandt = dbo.fm01t.mandt AND dbo.fm01.fikrs = dbo.fm01t.fikrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.v_glt0
AS
SELECT dbo.glt0.mandt, dbo.glt0.bukrs, dbo.t001.butxt, dbo.glt0.rldnr, dbo.glt0.rrcty, 
      dbo.glt0.rvers, dbo.glt0.ryear, dbo.glt0.racct, dbo.skat.spras, dbo.skat.txt20, 
      dbo.glt0.rbusa, dbo.glt0.rtcur, dbo.glt0.drcrk, dbo.glt0.rpmax, dbo.glt0.tslvt, 
      dbo.glt0.tsl01, dbo.glt0.tsl02, dbo.glt0.tsl04, dbo.glt0.tsl03, dbo.glt0.tsl05, 
      dbo.glt0.tsl06, dbo.glt0.tsl07, dbo.glt0.tsl08, dbo.glt0.tsl09, dbo.glt0.tsl10, 
      dbo.glt0.tsl11, dbo.glt0.tsl12, dbo.glt0.tsl13, dbo.glt0.tsl15, dbo.glt0.tsl14, 
      dbo.glt0.tsl16, dbo.glt0.hslvt, dbo.glt0.hsl01, dbo.glt0.hsl02, dbo.glt0.hsl03, 
      dbo.glt0.hsl04, dbo.glt0.hsl05, dbo.glt0.hsl06, dbo.glt0.hsl07, dbo.glt0.hsl08, 
      dbo.glt0.hsl09, dbo.glt0.hsl10, dbo.glt0.hsl11, dbo.glt0.hsl12, dbo.glt0.hsl13, 
      dbo.glt0.hsl14, dbo.glt0.hsl15, dbo.glt0.hsl16, dbo.glt0.cspred, dbo.glt0.kslvt, 
      dbo.glt0.ksl01, dbo.glt0.ksl02, dbo.glt0.ksl03, dbo.glt0.ksl04, dbo.glt0.ksl05, 
      dbo.glt0.ksl06, dbo.glt0.ksl08, dbo.glt0.ksl07, dbo.glt0.ksl09, dbo.glt0.ksl10, 
      dbo.glt0.ksl11, dbo.glt0.ksl12, dbo.glt0.ksl13, dbo.glt0.ksl14, dbo.glt0.ksl15, 
      dbo.glt0.ksl16
FROM dbo.glt0 INNER JOIN
      dbo.t001 ON dbo.glt0.mandt = dbo.t001.mandt AND 
      dbo.glt0.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.skat ON dbo.glt0.mandt = dbo.skat.mandt AND 
      dbo.glt0.racct = dbo.skat.saknr AND dbo.t001.ktopl = dbo.skat.ktopl


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_gsb_a
AS
SELECT dbo.tgsb.mandt, dbo.tgsbt.spras, dbo.tgsb.gsber, dbo.tgsbt.gtext, 
      dbo.tgsb.gsber_kons, dbo.tgsbl.txt
FROM dbo.tgsb INNER JOIN
      dbo.tgsbt ON dbo.tgsb.mandt = dbo.tgsbt.mandt AND 
      dbo.tgsb.gsber = dbo.tgsbt.gsber INNER JOIN
      dbo.tgsbk ON dbo.tgsb.mandt = dbo.tgsbk.mandt AND 
      dbo.tgsb.gsber_kons = dbo.tgsbk.gsber_kons INNER JOIN
      dbo.tgsbl ON dbo.tgsbk.mandt = dbo.tgsbl.mandt AND 
      dbo.tgsbk.gsber_kons = dbo.tgsbl.gsber_kons AND dbo.tgsbt.spras = dbo.tgsbl.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ikpf
AS
SELECT mandt, iblnr, gjahr, vgart, werks, lgort, sobkz, bldat, gidat, zldat, budat, monat, 
      usnam, sperr, zstat, dstat, xblni, lstat, xbufi, keord, ordng, invnu
FROM dbo.ikpf

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_iseg
AS
SELECT dbo.iseg.mandt, dbo.makt.spras, dbo.iseg.iblnr, dbo.iseg.gjahr, dbo.iseg.zeili, 
      dbo.iseg.matnr, dbo.makt.maktx, dbo.iseg.werks, dbo.iseg.lgort, dbo.iseg.charg, 
      dbo.iseg.sobkz, dbo.iseg.bstar, dbo.iseg.kdauf, dbo.iseg.kdpos, dbo.iseg.kdein, 
      dbo.iseg.lifnr, dbo.iseg.kunnr, dbo.iseg.plpla, dbo.iseg.usnam, dbo.iseg.aedat, 
      dbo.iseg.usnaz, dbo.iseg.zldat, dbo.iseg.usnad, dbo.iseg.budat, dbo.iseg.xblni, 
      dbo.iseg.xzael, dbo.iseg.xdiff, dbo.iseg.xnzae, dbo.iseg.xloek, dbo.iseg.xamei, 
      dbo.iseg.buchm, dbo.iseg.xnull, dbo.iseg.menge, dbo.iseg.meins, dbo.iseg.erfmg, 
      dbo.iseg.erfme, dbo.iseg.mblnr, dbo.iseg.mjahr, dbo.iseg.zeile, dbo.iseg.nblnr, 
      dbo.iseg.dmbtr, dbo.iseg.waers, dbo.iseg.abcin, dbo.iseg.ps_psp_pnr, dbo.iseg.vkwrt, 
      dbo.iseg.exvkw, dbo.iseg.buchw, dbo.iseg.kwart, dbo.iseg.vkwra, dbo.iseg.vkmzl, 
      dbo.iseg.vknzl, dbo.iseg.wrtzl, dbo.iseg.wrtbm, dbo.iseg.diwzl, dbo.iseg.attyp, 
      dbo.iseg.grund, dbo.iseg.samat
FROM dbo.iseg INNER JOIN
      dbo.makt ON dbo.iseg.mandt = dbo.makt.mandt AND dbo.iseg.matnr = dbo.makt.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_kna1_t077d
AS
SELECT dbo.kna1.mandt, dbo.kna1.kunnr, dbo.kna1.ktokd, dbo.kna1.name1, 
      dbo.kna1.name2, dbo.t077x.spras, dbo.t077x.txt30, dbo.t077d.zhekou, 
      dbo.kna1.vkgrps
FROM dbo.kna1 INNER JOIN
      dbo.t077d INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd ON dbo.kna1.mandt = dbo.t077d.mandt AND 
      dbo.kna1.ktokd = dbo.t077d.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_knb1
AS
SELECT mandt, kunnr, bukrs, erdat, ernam, sperr, loevm, zuawa, busab, akont, begru, 
      knrze, knrzb, zamim, zamiv, zamir, zamib, zamio, zwels, xverr, zahls, zterm, wakon, 
      vzskz, zindt, zinrt, eikto, zsabe, kverm, fdgrv, vrbkz, vlibb, vrszl, vrspr, vrsnr, verdt, 
      perkz, xdezv, xausz, webtr, remit, datlz, xzver, togru, kultg, hbkid, xpore, blnkz, altkn, 
      zgrup, urlid, mgrup, lockb, uzawe, ekvbd, sregl, xedip, frgrp, vrsdg, pernr, tlfxs, intad, 
      xknzb, guzte, gricd, gridt, wbrsl, confs, updat, uptim, nodel
FROM dbo.knb1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_knc3
AS
SELECT dbo.knc3.mandt, dbo.t074t.spras, dbo.knc3.bukrs, dbo.knc3.kunnr, dbo.knc3.gjahr, 
      dbo.t074u.koart, dbo.knc3.shbkz, dbo.t074t.ktext, dbo.t074t.ltext, dbo.knc3.saldv, 
      dbo.knc3.solll, dbo.knc3.habnl, dbo.t074u.umsks, dbo.t074u.merkp, dbo.t074u.zumkz, 
      dbo.t074u.klimp, dbo.t074u.diams
FROM dbo.t074t INNER JOIN
      dbo.t074u ON dbo.t074t.mandt = dbo.t074u.mandt AND 
      dbo.t074t.koart = dbo.t074u.koart AND 
      dbo.t074t.shbkz = dbo.t074u.umskz INNER JOIN
      dbo.knc3 ON dbo.t074u.mandt = dbo.knc3.mandt AND 
      dbo.t074u.umskz = dbo.knc3.shbkz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_knww
AS
SELECT dbo.knww.mandt, dbo.knww.kunnr, dbo.knww.ktoww, dbo.knww.ktovv, 
      dbo.t077v.ktovvx, dbo.knww.vvtxt, dbo.knww.vvcnt, dbo.knww.vmseh
FROM dbo.knww INNER JOIN
      dbo.t077v ON dbo.knww.mandt = dbo.t077v.mandt AND 
      dbo.knww.ktovv = dbo.t077v.ktovv AND dbo.knww.ktoww = dbo.t077v.ktoww

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lfa1
AS
SELECT dbo.lfa1.mandt, dbo.lfa1.lifnr, dbo.lfa1.ktokk, dbo.lfa1.anred, dbo.lfa1.name1, 
      dbo.lfa1.name2, dbo.lfa1.land1, dbo.lfa1.regio, dbo.lfa1.ort01, dbo.lfa1.pstlz, 
      dbo.lfa1.pfach, dbo.lfa1.pstl2, dbo.lfa1.stras, dbo.lfa1.telf1, dbo.lfa1.telbx, 
      dbo.lfa1.telf2, dbo.lfa1.telfx, dbo.lfa1.teltx, dbo.lfa1.telx1, dbo.lfa1.loevm, 
      dbo.lfa1.erdat, dbo.lfa1.ernam, dbo.t077y.txt30, dbo.t077y.spras, dbo.lfa1.sprasl, 
      dbo.lfa1.sortl, dbo.lfa1.nodel, dbo.lfa1.sperr, dbo.lfa1.sperm, dbo.lfa1.sperq, 
      dbo.lfa1.kunnr, dbo.lfa1.sperz
FROM dbo.lfa1 INNER JOIN
      dbo.t077k ON dbo.lfa1.mandt = dbo.t077k.mandt AND 
      dbo.lfa1.ktokk = dbo.t077k.ktokk INNER JOIN
      dbo.t077y ON dbo.t077k.mandt = dbo.t077y.mandt AND 
      dbo.t077k.ktokk = dbo.t077y.ktokk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lfc3
AS
SELECT dbo.lfc3.mandt, dbo.t074t.spras, dbo.t074t.koart, dbo.lfc3.lifnr, dbo.lfc3.bukrs, 
      dbo.lfc3.gjahr, dbo.lfc3.shbkz, dbo.lfc3.saldv, dbo.lfc3.solll, dbo.lfc3.habnl, 
      dbo.t074t.ktext, dbo.t074t.ltext, dbo.t074u.umsks, dbo.t074u.merkp, dbo.t074u.zumkz, 
      dbo.t074u.klimp, dbo.t074u.diams
FROM dbo.t074u INNER JOIN
      dbo.t074t ON dbo.t074u.mandt = dbo.t074t.mandt AND 
      dbo.t074u.umskz = dbo.t074t.shbkz AND 
      dbo.t074u.koart = dbo.t074t.koart INNER JOIN
      dbo.lfc3 ON dbo.t074u.umskz = dbo.lfc3.shbkz AND dbo.t074u.mandt = dbo.lfc3.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lips_ap
AS
SELECT mandt, vbeln, posnr, pstyv, fkrel
FROM dbo.lips

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpa1
AS
SELECT dbo.lpa1.mandt, dbo.lpa1.ktopl, dbo.lpa1.saknr, dbo.skat.spras, dbo.skat.txt20, 
      dbo.skat.txt50
FROM dbo.lpa1 INNER JOIN
      dbo.skat ON dbo.lpa1.mandt = dbo.skat.mandt AND 
      dbo.lpa1.ktopl = dbo.skat.ktopl AND dbo.lpa1.saknr = dbo.skat.saknr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpd5
AS
SELECT dbo.lpd5.mandt, dbo.t134t.spras, dbo.lpd5.mtart, dbo.t134t.mtbez, dbo.lpd5.clsnr, 
      dbo.lpd5.ktgrm, dbo.t134.kkref, dbo.t134.vprsv, dbo.t134.kzvpr, dbo.t134.vmtpo, 
      dbo.t134.kzgrp, dbo.t134.kzkfg, dbo.lpd5.nrlen, dbo.lpd5.nrchk, dbo.lpd5.nrfrm, 
      dbo.lpd5.nrto, dbo.lpd5.spart, dbo.lpd5.icon, dbo.t134.numki, dbo.t134.numke, 
      dbo.t134.envop
FROM dbo.t134 INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart INNER JOIN
      dbo.lpd5 ON dbo.t134.mandt = dbo.lpd5.mandt AND dbo.t134.mtart = dbo.lpd5.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpkna1
AS
SELECT dbo.kna1.mandt, dbo.t005t.spras, dbo.kna1.kunnr, dbo.kna1.ktokd, dbo.kna1.anred, 
      dbo.kna1.name1, dbo.kna1.name2, dbo.kna1.land1, dbo.kna1.regio, dbo.t005t.landx, 
      dbo.t005u.blandx, dbo.kna1.cityc, dbo.kna1.pstlz, dbo.kna1.stras, dbo.kna1.bbsnr, 
      dbo.kna1.telf1, dbo.kna1.telf2, dbo.kna1.telfx, dbo.kna1.telbx, 
      dbo.kna1.spras AS sprask, dbo.kna1.erdat, dbo.kna1.ernam, dbo.kna1.sex, 
      dbo.kna1.birthday, dbo.kna1.rcvsms, dbo.kna1.rcvemail, dbo.kna1.vipcard, 
      dbo.kna1.idnum, dbo.kna1.intro, dbo.kna1.parant, dbo.kna1.ort01, dbo.kna1.sortl, 
      dbo.kna1.vkgrps
FROM dbo.kna1 INNER JOIN
      dbo.t005 ON dbo.kna1.mandt = dbo.t005.mandt AND 
      dbo.kna1.land1 = dbo.t005.land1 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND 
      dbo.t005.land1 = dbo.t005t.land1 INNER JOIN
      dbo.t005s ON dbo.kna1.mandt = dbo.t005s.mandt AND 
      dbo.kna1.land1 = dbo.t005s.land1 AND dbo.kna1.regio = dbo.t005s.bland INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND dbo.t005s.bland = dbo.t005u.bland AND 
      dbo.t005t.spras = dbo.t005u.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lplfa1
AS
SELECT dbo.lfa1.mandt, dbo.t005t.spras, dbo.lfa1.lifnr, dbo.lfa1.ktokk, dbo.lfa1.anred, 
      dbo.lfa1.name1, dbo.lfa1.name2, dbo.lfa1.land1, dbo.lfa1.regio, dbo.t005t.landx, 
      dbo.t005u.blandx, dbo.lfa1.sortl, dbo.lfa1.ort01, dbo.lfa1.pstlz, dbo.lfa1.pfach, 
      dbo.lfa1.pstl2, dbo.lfa1.stras, dbo.lfa1.sprasl, dbo.lfa1.telbx, dbo.lfa1.telf1, 
      dbo.lfa1.telf2, dbo.lfa1.telfx, dbo.lfa1.teltx, dbo.lfa1.telx1, dbo.lfa1.loevm, 
      dbo.lfa1.nodel, dbo.lfa1.erdat, dbo.lfa1.ernam, dbo.lfa1.cityc
FROM dbo.t005 INNER JOIN
      dbo.lfa1 ON dbo.t005.mandt = dbo.lfa1.mandt AND 
      dbo.t005.land1 = dbo.lfa1.land1 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND 
      dbo.t005.land1 = dbo.t005t.land1 INNER JOIN
      dbo.t005s ON dbo.lfa1.mandt = dbo.t005s.mandt AND 
      dbo.lfa1.land1 = dbo.t005s.land1 AND dbo.lfa1.regio = dbo.t005s.bland INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND dbo.t005s.bland = dbo.t005u.bland AND 
      dbo.t005t.spras = dbo.t005u.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpmara
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.spras, dbo.makt.maktx, 
      dbo.t134t.mtbez, dbo.t023t.wgbez, dbo.lpd1.sztxt, dbo.mara.mtart, dbo.mara.matkl, 
      dbo.mara.meins, dbo.mara.size1, dbo.lpd5.icon, dbo.lp23.rgb
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.t023 ON dbo.mara.mandt = dbo.t023.mandt AND 
      dbo.mara.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.makt.spras = dbo.t023t.spras AND 
      dbo.t023.mandt = dbo.t023t.mandt AND dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.t134t INNER JOIN
      dbo.t134 ON dbo.t134t.mandt = dbo.t134.mandt AND 
      dbo.t134t.mtart = dbo.t134.mtart ON dbo.makt.spras = dbo.t134t.spras AND 
      dbo.mara.mandt = dbo.t134.mandt AND dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND 
      dbo.mara.mtart = dbo.lpd5.mtart INNER JOIN
      dbo.lp23 ON dbo.mara.mandt = dbo.lp23.mandt AND dbo.mara.matkl = dbo.lp23.matkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_lpmm01
AS
SELECT dbo.mard.mandt, dbo.makt.spras, dbo.mard.werks, dbo.mard.lgort, dbo.t001l.lgobe, 
      dbo.mara.mdnmr, dbo.mard.matnr, dbo.makt.maktx, dbo.mara.matkl, dbo.t023t.wgbez, 
      dbo.mara.size1, dbo.lpd1.sztxt, dbo.mara.meins, dbo.mard.labst, dbo.t134t.mtbez, 
      dbo.mara.mtart, dbo.lp23.rgb, dbo.lpd5.icon, dbo.mard.umlme, dbo.mard.insme, 
      dbo.mard.einme, dbo.mard.speme, dbo.mard.retme, dbo.mard.vmlab, 
      dbo.mard.vmuml
FROM dbo.t134t INNER JOIN
      dbo.t134 ON dbo.t134t.mandt = dbo.t134.mandt AND 
      dbo.t134t.mtart = dbo.t134.mtart INNER JOIN
      dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.lpd1 ON dbo.mara.mandt = dbo.lpd1.mandt AND 
      dbo.mara.size1 = dbo.lpd1.sznmr INNER JOIN
      dbo.t023 ON dbo.mara.mandt = dbo.t023.mandt AND 
      dbo.mara.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.makt.spras = dbo.t023t.spras AND 
      dbo.t023.mandt = dbo.t023t.mandt AND dbo.t023.matkl = dbo.t023t.matkl INNER JOIN
      dbo.mard ON dbo.mara.matnr = dbo.mard.matnr AND 
      dbo.mara.mandt = dbo.mard.mandt INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort ON 
      dbo.t134t.spras = dbo.makt.spras AND dbo.t134.mandt = dbo.mara.mandt AND 
      dbo.t134.mtart = dbo.mara.mtart INNER JOIN
      dbo.lp23 ON dbo.t023.mandt = dbo.lp23.mandt AND 
      dbo.t023.matkl = dbo.lp23.matkl INNER JOIN
      dbo.lpd5 ON dbo.mara.mandt = dbo.lpd5.mandt AND dbo.mara.mtart = dbo.lpd5.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mara
AS
SELECT dbo.mara.mandt, dbo.mara.matnr, dbo.makt.spras, dbo.makt.maktx, 
      dbo.makt.maktg, dbo.mara.mtart, dbo.mara.matkl, dbo.mara.meins, dbo.mara.mfrnr, 
      dbo.mara.bismt, dbo.mara.spart, dbo.mara.ersda, dbo.mara.ernam, dbo.mara.laeda, 
      dbo.mara.aenam, dbo.mara.lvorm, dbo.mara.mfrpn, dbo.mara.liqdt, dbo.mara.ihivi, 
      dbo.mara.iloos, dbo.mara.kzgvh, dbo.mara.brgew, dbo.mara.ntgew, dbo.mara.gewei, 
      dbo.mara.volum, dbo.mara.voleh, dbo.mara.laeng, dbo.mara.breit, dbo.mara.hoehe, 
      dbo.mara.meabm, dbo.mara.price, dbo.mara.pwaers, dbo.t023t.wgbez, 
      dbo.t134t.mtbez, dbo.mara.xchpf
FROM dbo.mara INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t023 ON dbo.mara.mandt = dbo.t023.mandt AND 
      dbo.mara.matkl = dbo.t023.matkl INNER JOIN
      dbo.t023t ON dbo.t023.mandt = dbo.t023t.mandt AND 
      dbo.t023.matkl = dbo.t023t.matkl AND dbo.makt.spras = dbo.t023t.spras INNER JOIN
      dbo.t134 ON dbo.mara.mandt = dbo.t134.mandt AND 
      dbo.mara.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND 
      dbo.t134.mtart = dbo.t134t.mtart AND dbo.makt.spras = dbo.t134t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_marc
AS
SELECT dbo.marc.mandt, dbo.marc.matnr, dbo.marc.werks, dbo.marc.maabc, 
      dbo.marc.ausme, dbo.marc.minbe, dbo.marc.xmcng, dbo.marc.eislo, dbo.marc.eisbe, 
      dbo.marc.lvorm, dbo.t001w.name1, dbo.t001w.name2, dbo.marc.konts, 
      dbo.marc.konth, dbo.marc.xchpf, dbo.marc.umlmc
FROM dbo.marc INNER JOIN
      dbo.t001w ON dbo.marc.mandt = dbo.t001w.mandt AND 
      dbo.marc.werks = dbo.t001w.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_marm006
AS
SELECT dbo.marm.mandt, dbo.marm.matnr, dbo.t006a.spras, dbo.marm.meinh, 
      dbo.t006a.mseht, dbo.t006a.msehl, dbo.marm.umrez, dbo.marm.umren
FROM dbo.marm INNER JOIN
      dbo.t006 ON dbo.marm.mandt = dbo.t006.mandt AND 
      dbo.marm.meinh = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mb53a
AS
SELECT dbo.mard.mandt, dbo.mard.matnr, dbo.mard.werks, dbo.mard.lgort, 
      dbo.t001w.name1, dbo.t001l.lgobe, dbo.mard.labst, dbo.mard.umlme, 
      dbo.mard.insme, dbo.mard.einme, dbo.mard.speme, dbo.mard.retme
FROM dbo.mard INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.lgort = dbo.t001l.lgort AND dbo.mard.werks = dbo.t001l.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mb53b
AS
SELECT dbo.mchb.mandt, dbo.mchb.matnr, dbo.mchb.werks, dbo.mchb.lgort, 
      dbo.t001w.name1, dbo.t001l.lgobe, dbo.mchb.charg, dbo.mchb.clabs, 
      dbo.mchb.cumlm, dbo.mchb.cinsm, dbo.mchb.cspem, dbo.mchb.ceinm, 
      dbo.mchb.cretm, dbo.mchb.cvmla, dbo.mchb.cvmum, dbo.mchb.cvmin, 
      dbo.mchb.cvmei, dbo.mchb.cvmsp, dbo.mchb.cvmre
FROM dbo.mchb INNER JOIN
      dbo.t001w ON dbo.mchb.mandt = dbo.t001w.mandt AND 
      dbo.mchb.werks = dbo.t001w.werks INNER JOIN
      dbo.t001l ON dbo.mchb.mandt = dbo.t001l.mandt AND 
      dbo.mchb.werks = dbo.t001l.werks AND dbo.mchb.lgort = dbo.t001l.lgort

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mcd9
AS
SELECT dbo.mbew.mandt, dbo.mbew.matnr, dbo.mbew.bwkey, dbo.t001k.bukrs, 
      dbo.makt.spras, dbo.mara.mtart, dbo.mara.matkl, dbo.mbew.bklas, dbo.mbew.lbkum, 
      dbo.mbew.salk3, dbo.mara.meins, dbo.t001.waers, dbo.t001.butxt, dbo.makt.maktx, 
      dbo.mbew.vprsv, dbo.mbew.verpr, dbo.mbew.stprs, dbo.mbew.peinh, 
      dbo.mbew.salkv
FROM dbo.mbew INNER JOIN
      dbo.mara ON dbo.mbew.mandt = dbo.mara.mandt AND 
      dbo.mbew.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t001k ON dbo.mbew.mandt = dbo.t001k.mandt AND 
      dbo.mbew.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.t001 ON dbo.t001k.bukrs = dbo.t001.bukrs AND dbo.t001k.mandt = dbo.t001.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mmbe_a
AS
SELECT dbo.mard.mandt, dbo.mard.matnr, dbo.mard.werks, dbo.mard.lgort, dbo.t001.bukrs, 
      dbo.mard.labst, dbo.mard.umlme, dbo.mard.insme, dbo.mard.einme, 
      dbo.mard.speme, dbo.mard.retme, dbo.mard.vmlab, dbo.mard.vmuml, dbo.t001.butxt, 
      dbo.t001w.name1, dbo.t001l.lgobe, dbo.marc.umlmc
FROM dbo.mard INNER JOIN
      dbo.t001w ON dbo.mard.mandt = dbo.t001w.mandt AND 
      dbo.mard.werks = dbo.t001w.werks INNER JOIN
      dbo.t001k ON dbo.t001w.bwkey = dbo.t001k.bwkey AND 
      dbo.t001w.mandt = dbo.t001k.mandt INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND 
      dbo.t001k.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.t001l ON dbo.mard.mandt = dbo.t001l.mandt AND 
      dbo.mard.werks = dbo.t001l.werks AND dbo.mard.lgort = dbo.t001l.lgort INNER JOIN
      dbo.marc ON dbo.mard.mandt = dbo.marc.mandt AND 
      dbo.mard.matnr = dbo.marc.matnr AND dbo.mard.werks = dbo.marc.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mmbe_b
AS
SELECT dbo.mchb.mandt, dbo.mchb.matnr, dbo.t001.bukrs, dbo.mchb.werks, 
      dbo.mchb.lgort, dbo.mchb.charg, dbo.mchb.clabs, dbo.mchb.cumlm, dbo.mchb.cinsm, 
      dbo.mchb.ceinm, dbo.mchb.cspem, dbo.mchb.cretm, dbo.mchb.cvmla, 
      dbo.mchb.cvmum, dbo.mchb.cvmin, dbo.mchb.cvmei, dbo.mchb.cvmsp, 
      dbo.mchb.cvmre, dbo.t001.butxt, dbo.t001l.lgobe, dbo.t001w.name1, dbo.mch1.ltext, 
      dbo.marc.umlmc
FROM dbo.t001w INNER JOIN
      dbo.t001k ON dbo.t001w.bwkey = dbo.t001k.bwkey AND 
      dbo.t001w.mandt = dbo.t001k.mandt INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND 
      dbo.t001k.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.mchb ON dbo.t001w.mandt = dbo.mchb.mandt AND 
      dbo.t001w.werks = dbo.mchb.werks INNER JOIN
      dbo.t001l ON dbo.mchb.mandt = dbo.t001l.mandt AND 
      dbo.mchb.werks = dbo.t001l.werks AND dbo.mchb.lgort = dbo.t001l.lgort INNER JOIN
      dbo.mch1 ON dbo.mchb.mandt = dbo.mch1.mandt AND 
      dbo.mchb.matnr = dbo.mch1.matnr AND 
      dbo.mchb.charg = dbo.mch1.charg INNER JOIN
      dbo.marc ON dbo.mchb.mandt = dbo.marc.mandt AND 
      dbo.mchb.matnr = dbo.marc.matnr AND dbo.mchb.werks = dbo.marc.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mmbe_bukrs
AS
SELECT dbo.marc.mandt, dbo.marc.matnr, dbo.marc.werks, dbo.t001.bukrs, 
      dbo.t001.butxt
FROM dbo.t001w INNER JOIN
      dbo.t001k ON dbo.t001w.bwkey = dbo.t001k.bwkey AND 
      dbo.t001w.mandt = dbo.t001k.mandt INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND 
      dbo.t001k.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.marc ON dbo.t001w.werks = dbo.marc.werks AND 
      dbo.t001w.mandt = dbo.marc.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mmbe_c
AS
SELECT dbo.ekbe.mandt, dbo.ekbe.matnr, dbo.ekbe.ebeln, dbo.ekbe.ebelp, dbo.ekbe.bwart, 
      dbo.t001k.bukrs, dbo.ekbe.werks, dbo.t001.butxt, dbo.ekbe.wesbs, dbo.ekbe.bpwes, 
      dbo.ekbe.elikz, dbo.ekbe.menge, dbo.ekbe.bpmng, dbo.ekbe.shkzg
FROM dbo.ekbe INNER JOIN
      dbo.t001w ON dbo.ekbe.mandt = dbo.t001w.mandt AND 
      dbo.ekbe.werks = dbo.t001w.werks INNER JOIN
      dbo.t001k ON dbo.t001w.bwkey = dbo.t001k.bwkey AND 
      dbo.t001w.mandt = dbo.t001k.mandt INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND dbo.t001k.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mmbe_werks
AS
SELECT dbo.marc.mandt, dbo.marc.matnr, dbo.marc.werks, dbo.t001w.name1, 
      dbo.t001.bukrs
FROM dbo.t001w INNER JOIN
      dbo.t001k ON dbo.t001w.bwkey = dbo.t001k.bwkey AND 
      dbo.t001w.mandt = dbo.t001k.mandt INNER JOIN
      dbo.t001 ON dbo.t001k.mandt = dbo.t001.mandt AND 
      dbo.t001k.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.marc ON dbo.t001w.mandt = dbo.marc.mandt AND 
      dbo.t001w.werks = dbo.marc.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ms189
AS
SELECT dbo.ms189.mandt, dbo.ms189.pltyp, dbo.ms189.matnr, dbo.mara.mtart, 
      dbo.ms189.prmng, dbo.ms189.price, dbo.ms189.waers, dbo.ms189.erdat, 
      dbo.ms189.ernam, dbo.ms189.meinsa
FROM dbo.ms189 INNER JOIN
      dbo.mara ON dbo.ms189.mandt = dbo.mara.mandt AND 
      dbo.ms189.matnr = dbo.mara.matnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ms189a
AS
SELECT dbo.ms189.mandt, dbo.makt.spras, dbo.ms189.pltyp, dbo.ms189.matnr, 
      dbo.ms189.prmng, dbo.ms189.price, dbo.ms189.waers, dbo.ms189.meinsa, 
      dbo.ms189.erdat, dbo.ms189.ernam, dbo.mara.mtart, dbo.mara.matkl, 
      dbo.makt.maktx, dbo.makt.maktg, dbo.t189t.ptext
FROM dbo.ms189 INNER JOIN
      dbo.mara ON dbo.ms189.mandt = dbo.mara.mandt AND 
      dbo.ms189.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t189 ON dbo.ms189.mandt = dbo.t189.mandt AND 
      dbo.ms189.pltyp = dbo.t189.pltyp INNER JOIN
      dbo.t189t ON dbo.ms189.mandt = dbo.t189t.mandt AND 
      dbo.t189.pltyp = dbo.t189t.pltyp AND dbo.makt.spras = dbo.t189t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mseg
AS
SELECT dbo.mseg.mandt, dbo.mseg.mblnr, dbo.makt.spras, dbo.mseg.mjahr, 
      dbo.mseg.zeile, dbo.mseg.matnr, dbo.makt.maktx, dbo.t001l.lgobe, dbo.mseg.meins, 
      dbo.mseg.bukrs, dbo.mseg.werks, dbo.mseg.waers, dbo.mseg.lgort, dbo.mseg.charg, 
      dbo.mseg.lfbnr, dbo.mseg.lfpos, dbo.mseg.lifnr, dbo.mseg.kunnr, dbo.mseg.kdauf, 
      dbo.mseg.kdpos, dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.sakto, 
      dbo.mseg.belum, dbo.mseg.buzum, dbo.mseg.shkzg, dbo.mseg.bwart, 
      dbo.mseg.insmk, dbo.mseg.sobkz, dbo.mseg.bwtar, dbo.mseg.menge, 
      dbo.mseg.kzbew, dbo.mseg.kzvbr, dbo.mseg.kzzug, dbo.mseg.grund, 
      dbo.mseg.erfme, dbo.mseg.erfmg, dbo.mseg.ebeln, dbo.mseg.ebelp, 
      dbo.mseg.bpmng, dbo.mseg.bprme, dbo.mseg.dmbtr, dbo.mseg.shkum, 
      dbo.mseg.dmbum, dbo.mseg.bualt, dbo.mseg.bnbtr, dbo.mseg.xauto, dbo.mseg.sgtxt, 
      dbo.mseg.gjahr, dbo.mseg.umwrk, dbo.mseg.umlgo, dbo.t006a.mseht, 
      dbo.mseg.lfbja, dbo.mseg.kdein, dbo.mseg.plpla, dbo.mseg.zusch, dbo.mseg.zustd, 
      dbo.mseg.line_id, dbo.mseg.parent_id, dbo.mseg.line_depth, dbo.mseg.sjahr, 
      dbo.mseg.smbln, dbo.mseg.smblp, dbo.mseg.elikz, dbo.mseg.equnr, 
      dbo.mseg.wempf, dbo.mseg.gsber, dbo.mseg.ablad, dbo.mseg.kokrs, 
      dbo.mseg.pargb, dbo.mseg.kostl, dbo.mseg.parbu, dbo.mseg.projn, dbo.mseg.aufnr, 
      dbo.mseg.anln1, dbo.mseg.anln2, dbo.mseg.xskst, dbo.mseg.xspro, dbo.mseg.xsauf, 
      dbo.mseg.xserg, dbo.mseg.xruem, dbo.mseg.xruej, dbo.mseg.rsnum, 
      dbo.mseg.rspos, dbo.mseg.kzear, dbo.mseg.pbamg, dbo.mseg.kzstr, 
      dbo.mseg.ummat, dbo.mseg.umcha, dbo.mseg.umzst, dbo.mseg.umzus, 
      dbo.mseg.umbar, dbo.mseg.umsok, dbo.mseg.weunb, dbo.mseg.palan, 
      dbo.mseg.lgnum, dbo.mseg.lgtyp, dbo.mseg.lgpla, dbo.mseg.bestq, dbo.mseg.bwlvs, 
      dbo.mseg.tbnum, dbo.mseg.tbpos, dbo.mseg.vschn, dbo.mseg.xblvs, 
      dbo.mseg.nschn, dbo.mseg.dypla, dbo.mseg.ubnum, dbo.mseg.tbpri, 
      dbo.mseg.tanum, dbo.mseg.weanz, dbo.mseg.evers, dbo.mseg.evere, 
      dbo.mseg.imkey, dbo.mseg.kstrg, dbo.mseg.paobjnr, dbo.mseg.prctr, 
      dbo.mseg.ps_psp_pnr, dbo.mseg.nplnr, dbo.mseg.aufpl, dbo.mseg.aplzl, 
      dbo.mseg.aufps, dbo.mseg.vptnr, dbo.mseg.fipos, dbo.mseg.bstmg, dbo.mseg.bstme, 
      dbo.mseg.vfdat, dbo.mseg.salk3, dbo.mseg.lbkum, dbo.mseg.wertu, 
      dbo.mseg.mengu, dbo.mseg.bustw, dbo.mseg.bustm, dbo.mseg.ummab, 
      dbo.mseg.matbf, dbo.mseg.fistl, dbo.mseg.geber, dbo.mseg.rsart, dbo.mseg.pprctr, 
      dbo.mseg.vprsv, dbo.mseg.fkber, dbo.mseg.dabrbz, dbo.mseg.vkwra, 
      dbo.mseg.dabrz, dbo.mseg.xbeau, dbo.mseg.lsmng, dbo.mseg.lsmeh, 
      dbo.mseg.kzbws, dbo.mseg.qinspst, dbo.mseg.urzei, dbo.mseg.j_1bexbase, 
      dbo.mseg.mwskz, dbo.mseg.txjcd, dbo.mseg.ematn, dbo.mseg.j_1agirupd, 
      dbo.mseg.vkmws, dbo.mseg.hsdat, dbo.mseg.berkz, dbo.mseg.mat_kdauf, 
      dbo.mseg.mat_kdpos, dbo.mseg.mat_pspnr, dbo.mseg.xwoff, dbo.mseg.bemot, 
      dbo.mseg.prznr, dbo.mseg.llief, dbo.mseg.lstar, dbo.mseg.xobew, 
      dbo.mseg.grant_nbr, dbo.mseg.zustd_t156m, dbo.mseg.oinavnw, 
      dbo.mseg.oicondcod, dbo.mseg.condi, dbo.mseg.bldat, dbo.mseg.budat, 
      dbo.mseg.cuobj_ch, dbo.mseg.exvkw, dbo.mseg.zekkn, dbo.mseg.aktnr, 
      dbo.mseg.vkwrt, dbo.mseg.exbwr, dbo.mseg.emlif, dbo.mseg.xwsbr
FROM dbo.mseg INNER JOIN
      dbo.mara ON dbo.mseg.mandt = dbo.mara.mandt AND 
      dbo.mseg.matnr = dbo.mara.matnr INNER JOIN
      dbo.makt ON dbo.mara.mandt = dbo.makt.mandt AND 
      dbo.mara.matnr = dbo.makt.matnr INNER JOIN
      dbo.t001l ON dbo.mseg.mandt = dbo.t001l.mandt AND 
      dbo.mseg.werks = dbo.t001l.werks AND dbo.mseg.lgort = dbo.t001l.lgort INNER JOIN
      dbo.t006 ON dbo.mseg.mandt = dbo.t006.mandt AND 
      dbo.mseg.erfme = dbo.t006.msehi INNER JOIN
      dbo.t006a ON dbo.t006.mandt = dbo.t006a.mandt AND 
      dbo.t006.msehi = dbo.t006a.msehi AND dbo.makt.spras = dbo.t006a.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mseg_mb01
AS
SELECT dbo.mseg.mandt, dbo.mseg.mblnr, dbo.mseg.mjahr, dbo.mseg.zeile, 
      dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.bwart, dbo.t156.kzwes, dbo.mseg.shkzg, 
      dbo.mseg.matnr, dbo.mseg.werks, dbo.mseg.lgort, dbo.mseg.menge, 
      dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, dbo.mseg.bpmng, 
      dbo.mseg.bprme, dbo.mseg.dmbtr, dbo.mseg.lfbja, dbo.mseg.lfbnr, dbo.mseg.lfpos, 
      dbo.mseg.lifnr, dbo.mseg.charg, dbo.mseg.insmk, dbo.mseg.sobkz, dbo.mseg.bwtar, 
      dbo.mseg.waers, dbo.mseg.elikz, dbo.mseg.sgtxt, dbo.mseg.kostl, dbo.mseg.gjahr, 
      dbo.mseg.bukrs, dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.belum, 
      dbo.mseg.buzum, dbo.mseg.kzbew, dbo.mseg.kzvbr, dbo.mseg.kzzug, 
      dbo.mseg.umsok, dbo.t156.xstbw
FROM dbo.mseg INNER JOIN
      dbo.t156 ON dbo.mseg.mandt = dbo.t156.mandt AND dbo.mseg.bwart = dbo.t156.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mseg_miro
AS
SELECT dbo.mseg.mandt, dbo.mseg.mblnr, dbo.mseg.mjahr, dbo.mseg.zeile, 
      dbo.mseg.ebeln, dbo.mseg.ebelp, dbo.mseg.bwart, dbo.t156.kzwes, dbo.mseg.shkzg, 
      dbo.mseg.matnr, dbo.mseg.werks, dbo.mseg.lgort, dbo.mseg.menge, 
      dbo.mseg.meins, dbo.mseg.erfmg, dbo.mseg.erfme, dbo.mseg.bpmng, 
      dbo.mseg.bprme, dbo.mseg.dmbtr, dbo.mseg.lfbja, dbo.mseg.lfbnr, dbo.mseg.lfpos, 
      dbo.mseg.lifnr, dbo.mseg.charg, dbo.mseg.insmk, dbo.mseg.sobkz, dbo.mseg.bwtar, 
      dbo.mseg.waers, dbo.mseg.elikz, dbo.mseg.kzbew, dbo.mseg.kzvbr, 
      dbo.mseg.kzzug, dbo.mseg.umsok, dbo.mseg.bukrs, dbo.mseg.gjahr, 
      dbo.mseg.belnr, dbo.mseg.buzei, dbo.mseg.belum, dbo.mseg.buzum, 
      dbo.mseg.sgtxt, dbo.mseg.kostl, dbo.t156.xstbw, dbo.mseg.mwskz
FROM dbo.mseg INNER JOIN
      dbo.t156 ON dbo.mseg.mandt = dbo.t156.mandt AND dbo.mseg.bwart = dbo.t156.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_mvke_a
AS
SELECT dbo.mvke.mandt, dbo.mvke.matnr, dbo.mvke.vkorg, dbo.mvke.vtweg, 
      dbo.makt.spras, dbo.makt.maktg, dbo.tvkot.vkorgx, dbo.tvtwt.vtwegx
FROM dbo.makt INNER JOIN
      dbo.mvke ON dbo.makt.mandt = dbo.mvke.mandt AND 
      dbo.makt.matnr = dbo.mvke.matnr INNER JOIN
      dbo.tvkot ON dbo.mvke.mandt = dbo.tvkot.mandt AND 
      dbo.mvke.vkorg = dbo.tvkot.vkorg AND dbo.makt.spras = dbo.tvkot.spras INNER JOIN
      dbo.tvtwt ON dbo.makt.spras = dbo.tvtwt.spras AND 
      dbo.mvke.mandt = dbo.tvtwt.mandt AND dbo.mvke.vtweg = dbo.tvtwt.vtweg

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ob53
AS
SELECT dbo.t030.mandt, dbo.skat.spras, dbo.t030.ktopl, dbo.t030.ktosl, dbo.t030.bwmod, 
      dbo.t030.komok, dbo.t030.bklas, dbo.t030.konts, dbo.skat.txt20, dbo.skat.txt50
FROM dbo.t030 INNER JOIN
      dbo.ska1 ON dbo.t030.mandt = dbo.ska1.mandt AND 
      dbo.t030.ktopl = dbo.ska1.ktopl AND dbo.t030.konts = dbo.ska1.saknr INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.ktopl = dbo.skat.ktopl AND dbo.ska1.saknr = dbo.skat.saknr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_qpcd_kat
AS
SELECT dbo.qpcd.mandt, dbo.qpcd.katalogart, dbo.qpcd.codegruppe, dbo.qpcd.code, 
      dbo.qpcd.version, dbo.qpcd.gueltigab, dbo.qpct.kurztextc, dbo.qpcd.fehlklasse, 
      dbo.qpcd.folgeakti, dbo.qpct.gueltigabt, dbo.qpct.ltextv, dbo.qpcd.verwendung, 
      dbo.qpcd.inaktiv, dbo.qpcd.geloescht, dbo.qpcd.ersteller, dbo.qpcd.e_datum, 
      dbo.qpcd.aenderer, dbo.qpcd.a_datum, dbo.qpct.inaktivt, dbo.qpct.geloeschtt, 
      dbo.tq15t.katalogtxt, dbo.qpgt.kurztext, dbo.qpct.spras
FROM dbo.qpcd INNER JOIN
      dbo.qpct ON dbo.qpcd.mandt = dbo.qpct.mandt AND 
      dbo.qpcd.katalogart = dbo.qpct.katalogart AND 
      dbo.qpcd.codegruppe = dbo.qpct.codegruppe AND dbo.qpcd.code = dbo.qpct.code AND 
      dbo.qpcd.version = dbo.qpct.version INNER JOIN
      dbo.qpgr ON dbo.qpcd.mandt = dbo.qpgr.mandt AND 
      dbo.qpcd.katalogart = dbo.qpgr.katalogart AND 
      dbo.qpcd.codegruppe = dbo.qpgr.codegruppe INNER JOIN
      dbo.qpgt ON dbo.qpgr.mandt = dbo.qpgt.mandt AND 
      dbo.qpgr.katalogart = dbo.qpgt.katalogart AND 
      dbo.qpgr.codegruppe = dbo.qpgt.codegruppe AND 
      dbo.qpct.spras = dbo.qpgt.spras INNER JOIN
      dbo.tq15 ON dbo.qpcd.mandt = dbo.tq15.mandt AND 
      dbo.qpcd.katalogart = dbo.tq15.katalogart INNER JOIN
      dbo.tq15t ON dbo.tq15.mandt = dbo.tq15t.mandt AND 
      dbo.tq15.katalogart = dbo.tq15t.katalogart AND dbo.qpgt.spras = dbo.tq15t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_qpgr_kat
AS
SELECT dbo.qpgr.mandt, dbo.qpgr.katalogart, dbo.qpgr.codegruppe, dbo.qpgt.kurztext, 
      dbo.qpgr.status, dbo.qpgt.ltextv, dbo.qpgr.verwendung, dbo.qpgr.inaktiv, 
      dbo.qpgr.ersteller, dbo.qpgr.e_datum, dbo.qpgr.aenderer, dbo.qpgr.a_datum, 
      dbo.tq15t.katalogtxt, dbo.qpgt.inaktivt, dbo.qpgt.spras
FROM dbo.qpgr INNER JOIN
      dbo.qpgt ON dbo.qpgr.mandt = dbo.qpgt.mandt AND 
      dbo.qpgr.katalogart = dbo.qpgt.katalogart AND 
      dbo.qpgr.codegruppe = dbo.qpgt.codegruppe INNER JOIN
      dbo.tq15 ON dbo.qpgr.mandt = dbo.tq15.mandt AND 
      dbo.qpgr.katalogart = dbo.tq15.katalogart INNER JOIN
      dbo.tq15t ON dbo.tq15.mandt = dbo.tq15t.mandt AND 
      dbo.tq15.katalogart = dbo.tq15t.katalogart AND dbo.qpgt.spras = dbo.tq15t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_qpgrc
AS
SELECT dbo.qpgr.mandt, dbo.qpgr.katalogart, dbo.qpgr.codegruppe, dbo.qpgt.spras, 
      dbo.qpgt.kurztext, dbo.qpct.code, dbo.qpct.kurztextc, dbo.qpcd.version
FROM dbo.qpct INNER JOIN
      dbo.qpcd ON dbo.qpct.mandt = dbo.qpcd.mandt AND 
      dbo.qpct.katalogart = dbo.qpcd.katalogart AND 
      dbo.qpct.codegruppe = dbo.qpcd.codegruppe AND dbo.qpct.code = dbo.qpcd.code AND 
      dbo.qpct.version = dbo.qpcd.version INNER JOIN
      dbo.qpgr INNER JOIN
      dbo.qpgt ON dbo.qpgr.mandt = dbo.qpgt.mandt AND 
      dbo.qpgr.katalogart = dbo.qpgt.katalogart AND 
      dbo.qpgr.codegruppe = dbo.qpgt.codegruppe ON 
      dbo.qpcd.mandt = dbo.qpgr.mandt AND dbo.qpcd.katalogart = dbo.qpgr.katalogart AND 
      dbo.qpcd.codegruppe = dbo.qpgr.codegruppe AND dbo.qpct.spras = dbo.qpgt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_rlaut
AS
SELECT dbo.rlaut.mandt, dbo.tobct.langu, dbo.rlaut.rolnr, dbo.rlaut.objct, dbo.rlaut.auth, 
      dbo.rlaut.aktps, dbo.tobj.oclss, dbo.rl01.roltx, dbo.tobjt.ttext, dbo.tobct.ctext, 
      dbo.tobj.fiel1, dbo.tobj.fiel2, dbo.tobj.fiel3, dbo.tobj.fiel4, dbo.tobj.fiel5, dbo.tobj.fiel6, 
      dbo.tobj.fiel7, dbo.tobj.fiel8, dbo.tobj.fiel9, dbo.tobj.fiel0
FROM dbo.rlaut INNER JOIN
      dbo.rl01 ON dbo.rlaut.mandt = dbo.rl01.mandt AND 
      dbo.rlaut.rolnr = dbo.rl01.rolnr INNER JOIN
      dbo.usr12 ON dbo.rlaut.mandt = dbo.usr12.mandt AND 
      dbo.rlaut.objct = dbo.usr12.objct AND dbo.rlaut.auth = dbo.usr12.auth AND 
      dbo.rlaut.aktps = dbo.usr12.aktps INNER JOIN
      dbo.tobj ON dbo.rlaut.objct = dbo.tobj.objct INNER JOIN
      dbo.tobjt ON dbo.tobj.objct = dbo.tobjt.object INNER JOIN
      dbo.tobc ON dbo.tobj.oclss = dbo.tobc.oclss INNER JOIN
      dbo.tobct ON dbo.tobc.oclss = dbo.tobct.oclss AND dbo.tobjt.langu = dbo.tobct.langu

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_rlaut_txt
AS
SELECT dbo.rlaut.mandt, dbo.usr12t.spras, dbo.rlaut.rolnr, dbo.rlaut.objct, dbo.rlaut.auth, 
      dbo.rlaut.aktps, dbo.rl01.roltx, dbo.usr12t.ltext
FROM dbo.rlaut INNER JOIN
      dbo.rl01 ON dbo.rlaut.mandt = dbo.rl01.mandt AND 
      dbo.rlaut.rolnr = dbo.rl01.rolnr INNER JOIN
      dbo.usr12 ON dbo.rlaut.mandt = dbo.usr12.mandt AND 
      dbo.rlaut.objct = dbo.usr12.objct AND dbo.rlaut.auth = dbo.usr12.auth AND 
      dbo.rlaut.aktps = dbo.usr12.aktps INNER JOIN
      dbo.usr12t ON dbo.usr12.mandt = dbo.usr12t.mandt AND 
      dbo.usr12.objct = dbo.usr12t.objct AND dbo.usr12.auth = dbo.usr12t.auth AND 
      dbo.usr12.aktps = dbo.usr12t.aktps

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_rltc
AS
SELECT dbo.rltc.mandt, dbo.rltc.rolnr, dbo.rltc.tcode, dbo.tstct.spras, dbo.tstct.ttext
FROM dbo.rltc INNER JOIN
      dbo.tstc ON dbo.rltc.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ska1_core
AS
SELECT dbo.ska1.mandt, dbo.ska1.ktopl, dbo.ska1.saknr, dbo.skat.spras, dbo.ska1.xbilk, 
      dbo.ska1.sakan, dbo.skat.txt20, dbo.skat.txt50, dbo.ska1.bilkt, dbo.ska1.erdat, 
      dbo.ska1.ernam, dbo.ska1.ktoks, dbo.ska1.vbund, dbo.ska1.xloev, dbo.ska1.xspea, 
      dbo.ska1.xspeb, dbo.ska1.xspep, dbo.t077s.sclass, dbo.t077s.bschlh, 
      dbo.t077s.bschls, dbo.t077z.txt30, dbo.skb1.bukrs, dbo.skb1.waers, dbo.skb1.mitkz, 
      dbo.ska1.cleve, dbo.ska1.parnr, dbo.ska1.hzflg, dbo.ska1.yedir, dbo.ska1.wlhs, 
      dbo.ska1.slhs, dbo.ska1.xjkm, dbo.ska1.yhkm, dbo.ska1.rjzkm, dbo.ska1.xjdjw, 
      dbo.ska1.gvtyp, dbo.ska1.mustr, dbo.ska1.mcod1
FROM dbo.ska1 INNER JOIN
      dbo.skat ON dbo.ska1.mandt = dbo.skat.mandt AND 
      dbo.ska1.ktopl = dbo.skat.ktopl AND dbo.ska1.saknr = dbo.skat.saknr INNER JOIN
      dbo.t077s ON dbo.ska1.mandt = dbo.t077s.mandt AND 
      dbo.ska1.ktopl = dbo.t077s.ktopl AND dbo.ska1.ktoks = dbo.t077s.ktoks INNER JOIN
      dbo.t077z ON dbo.t077s.mandt = dbo.t077z.mandt AND 
      dbo.skat.spras = dbo.t077z.spras AND dbo.t077s.ktopl = dbo.t077z.ktopl AND 
      dbo.t077s.ktoks = dbo.t077z.ktoks INNER JOIN
      dbo.skb1 ON dbo.ska1.mandt = dbo.skb1.mandt AND dbo.ska1.saknr = dbo.skb1.saknr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_skb1
AS
SELECT mandt, bukrs, saknr, begru, busab, datlz, erdat, ernam, fdgrv, fdlev, fipls, fstag, 
      hbkid, hktid, kdfsl, mitkz, mwskz, stext, vzskz, waers, wmeth, xgkon, xintb, xkres, 
      xloeb, xnkon, xopvw, xspeb, zindt, zinrt, zuawa, altkt, xmitk, recid, fipos, xmwno, 
      xsalh, bewgp, infky, togru
FROM dbo.skb1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001
AS
SELECT mandt, bukrs, butxt, ort01, land1, waers, spras, adrnr, dlflg, chkperiod
FROM dbo.t001

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001b
AS
SELECT mandt, rrcty, bukrs, mkoar, bkont, vkont, frye1, frpe1, toye1, tope1, frye2, frpe2, 
      toye2, brgru, tope2
FROM dbo.t001b

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001p
AS
SELECT dbo.t001p.mandt, dbo.t001p.werks, dbo.t001p.btrtl, dbo.t001p.molga AS modif, 
      dbo.t500p.name1, dbo.t001p.btext, dbo.t500p.molga AS molgat500p
FROM dbo.t001p INNER JOIN
      dbo.t500p ON dbo.t001p.mandt = dbo.t500p.mandt AND 
      dbo.t001p.werks = dbo.t500p.persa

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001w
AS
SELECT dbo.t001w.mandt, dbo.t001w.werks, dbo.t001w.name1, dbo.t001w.name2, 
      dbo.t001w.stras, dbo.t001w.pfach, dbo.t001w.pstlz, dbo.t001w.ort01, dbo.t001w.land1, 
      dbo.t005t.landx, dbo.t001w.regio, dbo.t005u.blandx, dbo.t001w.counc, 
      dbo.t005f.councx, dbo.t001w.cityc, dbo.t005h.citycx, dbo.t001w.adrnr, 
      dbo.t001w.spras, dbo.t002t.sptxt, dbo.t001w.fabkl, dbo.tfact.ltext, dbo.t001w.txjcd, 
      dbo.t001w.dlflg
FROM dbo.tfacd INNER JOIN
      dbo.tfact ON dbo.tfacd.ident = dbo.tfact.ident INNER JOIN
      dbo.t001w INNER JOIN
      dbo.t005 ON dbo.t001w.mandt = dbo.t005.mandt AND 
      dbo.t001w.land1 = dbo.t005.land1 INNER JOIN
      dbo.t002 ON dbo.t001w.spras = dbo.t002.spras INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND 
      dbo.t005.land1 = dbo.t005t.land1 INNER JOIN
      dbo.t002t ON dbo.t002.spras = dbo.t002t.sprsl INNER JOIN
      dbo.t005s ON dbo.t001w.mandt = dbo.t005s.mandt AND 
      dbo.t001w.land1 = dbo.t005s.land1 AND 
      dbo.t001w.regio = dbo.t005s.bland INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND 
      dbo.t005s.bland = dbo.t005u.bland INNER JOIN
      dbo.t005g ON dbo.t001w.mandt = dbo.t005g.mandt AND 
      dbo.t001w.land1 = dbo.t005g.land1 AND dbo.t001w.regio = dbo.t005g.regio AND 
      dbo.t001w.cityc = dbo.t005g.cityc INNER JOIN
      dbo.t005e ON dbo.t001w.mandt = dbo.t005e.mandt AND 
      dbo.t001w.land1 = dbo.t005e.land1 AND dbo.t001w.regio = dbo.t005e.regio AND 
      dbo.t001w.counc = dbo.t005e.counc INNER JOIN
      dbo.t005h ON dbo.t005g.mandt = dbo.t005h.mandt AND 
      dbo.t005g.land1 = dbo.t005h.land1 AND dbo.t005g.regio = dbo.t005h.regio AND 
      dbo.t005g.cityc = dbo.t005h.cityc INNER JOIN
      dbo.t005f ON dbo.t005e.mandt = dbo.t005f.mandt AND 
      dbo.t005e.land1 = dbo.t005f.land1 AND dbo.t005e.regio = dbo.t005f.regio AND 
      dbo.t005e.counc = dbo.t005f.counc ON dbo.tfacd.ident = dbo.t001w.fabkl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t001w_a
AS
SELECT dbo.t001w.mandt, dbo.t001w.werks, dbo.t001k.bwkey, dbo.t001k.bukrs, 
      dbo.t001.waers
FROM dbo.t001w INNER JOIN
      dbo.t001k ON dbo.t001w.mandt = dbo.t001k.mandt AND 
      dbo.t001w.bwkey = dbo.t001k.bwkey INNER JOIN
      dbo.t001 ON dbo.t001k.bukrs = dbo.t001.bukrs AND dbo.t001k.mandt = dbo.t001.mandt

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t003
AS
SELECT dbo.t003.mandt, dbo.t003t.spras, dbo.t003.blart, dbo.t003t.ltext, dbo.t003.numkr, 
      dbo.t003.koars, dbo.t003.stbla, dbo.t003.xnetb, dbo.t003.xrvup, dbo.t003.xsybl, 
      dbo.t003.xvork, dbo.t003.xkkpr, dbo.t003.xgsub, dbo.t003.xmges, dbo.t003.brgru, 
      dbo.t003.recid, dbo.t003.recic, dbo.t003.xmtxt, dbo.t003.xmref, dbo.t003.xngbk, 
      dbo.t003.kurst, dbo.t003.xnegp, dbo.t003.xkoaa, dbo.t003.xkoad, dbo.t003.xkoak, 
      dbo.t003.xkoam, dbo.t003.xkoas, dbo.t003.xnmrl, dbo.t003.xausg, dbo.t003.xdtch, 
      dbo.t003.blkls, dbo.t003.xrollup, dbo.t003.xplan, dbo.t003.xallocact, 
      dbo.t003.xallocplan, dbo.t003.x_pp_process, dbo.t003.xmref2
FROM dbo.t003 INNER JOIN
      dbo.t003t ON dbo.t003.mandt = dbo.t003t.mandt AND dbo.t003.blart = dbo.t003t.blart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t003o_a
AS
SELECT dbo.t003o.mandt, dbo.t003o.auart, dbo.t003p.spras, dbo.t003p.txt, 
      dbo.t003o.numkr, dbo.t003o.autyp, dbo.t003o.erloese, dbo.t003o.stsma, 
      dbo.t003o.aprof, dbo.t003o.pprof, dbo.t003o.copar, dbo.t003o.resz1, dbo.t003o.resz2, 
      dbo.t003o.aufkl, dbo.t003o.relkz, dbo.t003o.chgkz, dbo.t003o.bprof, dbo.t003o.plint, 
      dbo.t003o.nabpf, dbo.t003o.vorpl, dbo.t003o.layout, dbo.t003o.tdform, 
      dbo.t003o.scope, dbo.t003o.colordproc, dbo.t003o.vrg_stsma
FROM dbo.t003o INNER JOIN
      dbo.t003p ON dbo.t003o.mandt = dbo.t003p.mandt AND 
      dbo.t003o.auart = dbo.t003p.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t004
AS
SELECT dbo.t004.mandt, dbo.t004.ktopl, dbo.t004t.spras, dbo.t004t.ktplt, dbo.t004.sakln, 
      dbo.t004.dspra, dbo.t004.aspra, dbo.t004.kktpl, dbo.t004.xsper, 
      dbo.t004.integ_co
FROM dbo.t004 INNER JOIN
      dbo.t004t ON dbo.t004.mandt = dbo.t004t.mandt AND dbo.t004.ktopl = dbo.t004t.ktopl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t005g
AS
SELECT dbo.t005g.mandt, dbo.t005h.spras, dbo.t005g.land1, dbo.t005g.regio, 
      dbo.t005g.cityc, dbo.t005h.citycx
FROM dbo.t005g INNER JOIN
      dbo.t005h ON dbo.t005g.mandt = dbo.t005h.mandt AND 
      dbo.t005g.land1 = dbo.t005h.land1 AND dbo.t005g.regio = dbo.t005h.regio AND 
      dbo.t005g.cityc = dbo.t005h.cityc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t005s
AS
SELECT dbo.t005s.mandt, dbo.t005s.land1, dbo.t005s.bland, dbo.t005u.spras, 
      dbo.t005u.blandx
FROM dbo.t005s INNER JOIN
      dbo.t005u ON dbo.t005s.mandt = dbo.t005u.mandt AND 
      dbo.t005s.land1 = dbo.t005u.land1 AND dbo.t005s.bland = dbo.t005u.bland

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t006d
AS
SELECT dbo.t006d.mandt, dbo.t006d.dimid, dbo.t006t.spras, dbo.t006t.txdim, 
      dbo.t006d.leng, dbo.t006d.mass, dbo.t006d.timex, dbo.t006d.ecurr, dbo.t006d.[temp], 
      dbo.t006d.molqu, dbo.t006d.light, dbo.t006d.mssie, dbo.t006d.temp_dep
FROM dbo.t006d INNER JOIN
      dbo.t006t ON dbo.t006d.mandt = dbo.t006t.mandt AND 
      dbo.t006d.dimid = dbo.t006t.dimid

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t008
AS
SELECT dbo.t008.mandt, dbo.t008t.spras, dbo.t008.zahls, dbo.t008t.textl, dbo.t008.char1, 
      dbo.t008.xozsp, dbo.t008.xnchg
FROM dbo.t008 INNER JOIN
      dbo.t008t ON dbo.t008.mandt = dbo.t008t.mandt AND dbo.t008.zahls = dbo.t008t.zahls

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t010o
AS
SELECT dbo.t010o.mandt, dbo.t010o.opvar, dbo.t010p.spras, dbo.t010p.opvtx
FROM dbo.t010o INNER JOIN
      dbo.t010p ON dbo.t010o.mandt = dbo.t010p.mandt AND 
      dbo.t010o.opvar = dbo.t010p.opvar

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t011
AS
SELECT dbo.t011.mandt, dbo.t011t.spras, dbo.t011.versn, dbo.t011t.vstxt, dbo.t011.dspra, 
      dbo.t011.aspra, dbo.t011.xergs, dbo.t011.ktopl, dbo.t011.aktva, dbo.t011.pssva, 
      dbo.t011.ergak, dbo.t011.ergpa, dbo.t011.erggv, dbo.t011.zuord, dbo.t011.xauto, 
      dbo.t011.xfber, dbo.t011.anhng
FROM dbo.t011 INNER JOIN
      dbo.t011t ON dbo.t011.mandt = dbo.t011t.mandt AND dbo.t011.versn = dbo.t011t.versn

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t012
AS
SELECT dbo.t012.mandt, dbo.t012.bukrs, dbo.t012.hbkid, dbo.t012.banks, dbo.t012.bankl, 
      dbo.t012.telf1, dbo.t012.stcd1, dbo.t012.name1, dbo.t012.spras, dbo.t012.bupla, 
      dbo.t001.butxt, dbo.t012d.xbrie, dbo.t012d.dtxbb, dbo.t012d.dtaws, dbo.t012d.dtvta, 
      dbo.t012d.dtlbe, dbo.t012d.dtxms, dbo.t012d.dtgis, dbo.t012d.dtgbk, dbo.t012d.dtglz, 
      dbo.t012d.dtelz, dbo.t012d.dtfin, dbo.t012d.edipn, dbo.t012d.edisn, dbo.t012d.dtbid, 
      dbo.t012d.dtkid, dbo.t012d.xdrah, dbo.t012d.xkoba, dbo.t012d.xbabe, 
      dbo.t012d.xbegu, dbo.t012d.dtblz, dbo.t012d.dtlfi, dbo.t012d.dtlbr, dbo.t012d.dtgbl, 
      dbo.t012d.dtgkt
FROM dbo.t001 INNER JOIN
      dbo.t012 ON dbo.t001.mandt = dbo.t012.mandt AND 
      dbo.t001.bukrs = dbo.t012.bukrs INNER JOIN
      dbo.t012d ON dbo.t012.mandt = dbo.t012d.mandt AND 
      dbo.t012.bukrs = dbo.t012d.bukrs AND dbo.t012.hbkid = dbo.t012d.hbkid

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t012bn
AS
SELECT dbo.t012.mandt, dbo.t012.bukrs, dbo.t012.hbkid, dbo.t012.banks, dbo.t012.bankl, 
      dbo.bnka.banka, dbo.bnka.erdat, dbo.bnka.ernam, dbo.bnka.provz, dbo.bnka.stras, 
      dbo.bnka.ort01, dbo.bnka.brnch, dbo.t012.telf1, dbo.t012.stcd1, dbo.t012.name1
FROM dbo.t012 INNER JOIN
      dbo.t001 ON dbo.t012.mandt = dbo.t001.mandt AND 
      dbo.t012.bukrs = dbo.t001.bukrs INNER JOIN
      dbo.bnka ON dbo.t012.mandt = dbo.bnka.mandt AND 
      dbo.t012.bankl = dbo.bnka.bankl AND dbo.t012.banks = dbo.bnka.banks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t014
AS
SELECT dbo.t014.mandt, dbo.t014.kkber, dbo.t014t.spras, dbo.t014t.kkbtx, dbo.t014.waers, 
      dbo.t014.stafo, dbo.t014.periv, dbo.t014.ctlpc, dbo.t014.klimk, dbo.t014.sbgrp, 
      dbo.t014.allcc
FROM dbo.t014 INNER JOIN
      dbo.t014t ON dbo.t014.mandt = dbo.t014t.mandt AND dbo.t014.kkber = dbo.t014t.kkber

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t024w
AS
SELECT dbo.t024w.mandt, dbo.t024w.ekorg, dbo.t024w.werks, dbo.t001w.name1, 
      dbo.t001w.name2
FROM dbo.t024w INNER JOIN
      dbo.t001w ON dbo.t024w.mandt = dbo.t001w.mandt AND 
      dbo.t024w.werks = dbo.t001w.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t030a
AS
SELECT dbo.t030a.ktosl, dbo.t030w.spras, dbo.t030w.ltext, dbo.t030w.ktext, 
      dbo.t030w.ktxt2, dbo.t030w.ktxt3, dbo.t030a.grupp, dbo.t030a.tcode, dbo.t030a.f1kom, 
      dbo.t030a.f1bkl, dbo.t030a.f1bwm, dbo.t030a.f4kom, dbo.t030a.f4bwm, 
      dbo.t030a.f4bkl
FROM dbo.t030a INNER JOIN
      dbo.t030w ON dbo.t030a.ktosl = dbo.t030w.ktosl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t030x
AS
SELECT dbo.t030x.grupp, dbo.t030y.spras, dbo.t030y.ltext
FROM dbo.t030x INNER JOIN
      dbo.t030y ON dbo.t030x.grupp = dbo.t030y.grupp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t041c
AS
SELECT dbo.t041c.mandt, dbo.t041ct.spras, dbo.t041c.stgrd, dbo.t041ct.txt40, 
      dbo.t041c.xnegp, dbo.t041c.xabwd
FROM dbo.t041c INNER JOIN
      dbo.t041ct ON dbo.t041c.mandt = dbo.t041ct.mandt AND 
      dbo.t041c.stgrd = dbo.t041ct.stgrd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t052
AS
SELECT dbo.t052.mandt, dbo.t052u.spras, dbo.t052.zterm, dbo.t052.ztagg, dbo.tvzbt.vtext, 
      dbo.t052u.text1, dbo.t052.zdart, dbo.t052.zfael, dbo.t052.zmona, dbo.t052.ztag1, 
      dbo.t052.zprz1, dbo.t052.ztag2, dbo.t052.zprz2, dbo.t052.ztag3, dbo.t052.zstg1, 
      dbo.t052.zsmn1, dbo.t052.zstg2, dbo.t052.zsmn2, dbo.t052.zstg3, dbo.t052.zsmn3, 
      dbo.t052.xzbrv, dbo.t052.zschf, dbo.t052.txn08, dbo.t052.zlsch, dbo.t052.koart, 
      dbo.t052.xsplt, dbo.t052.xchpb, dbo.t052.xchpm, dbo.t052.xscrc
FROM dbo.t052 INNER JOIN
      dbo.t052u ON dbo.t052.mandt = dbo.t052u.mandt AND 
      dbo.t052.zterm = dbo.t052u.zterm AND dbo.t052.ztagg = dbo.t052u.ztagg INNER JOIN
      dbo.tvzbt ON dbo.t052.mandt = dbo.tvzbt.mandt AND 
      dbo.t052u.spras = dbo.tvzbt.spras AND dbo.t052.zterm = dbo.tvzbt.zterm

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t059q
AS
SELECT dbo.t059q.mandt, dbo.t005t.spras, dbo.t059q.land1, dbo.t059q.qsskz, 
      dbo.t005t.landx, dbo.t059q.qscod, dbo.t059q.qsbez, dbo.t059q.qproz, 
      dbo.t059q.qsatz, dbo.t059q.xntto, dbo.t059q.xqsbz, dbo.t059q.qsatr, dbo.t059q.xqfor, 
      dbo.t059q.qmind, dbo.t059q.qmiwa, dbo.t059q.bland, dbo.t059q.fprcd, 
      dbo.t059q.qekar, dbo.t059q.qsats
FROM dbo.t005 INNER JOIN
      dbo.t059q ON dbo.t005.mandt = dbo.t059q.mandt AND 
      dbo.t005.land1 = dbo.t059q.land1 INNER JOIN
      dbo.t005t ON dbo.t005.mandt = dbo.t005t.mandt AND dbo.t005.land1 = dbo.t005t.land1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t064a
AS
SELECT dbo.t064a.bstar, dbo.t064b.spras, dbo.t064b.btext
FROM dbo.t064a INNER JOIN
      dbo.t064b ON dbo.t064a.bstar = dbo.t064b.bstar

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t074
AS
SELECT dbo.t074.mandt, dbo.t001.bukrs, dbo.t074.ktopl, dbo.t074.koart, dbo.t074.umskz, 
      dbo.t074.hkont, dbo.t074.skont, dbo.t074.kon30, dbo.t074.ebene
FROM dbo.t001 INNER JOIN
      dbo.t074 ON dbo.t001.mandt = dbo.t074.mandt AND dbo.t001.ktopl = dbo.t074.ktopl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t074u
AS
SELECT dbo.t074u.mandt, dbo.t074t.spras, dbo.t074u.koart, dbo.t074u.umskz, 
      dbo.t074u.umsks, dbo.t074u.merkp, dbo.t074u.zumkz, dbo.t074u.klimp, 
      dbo.t074u.diams, dbo.t074t.ktext, dbo.t074t.ltext, dbo.t074u.xanet
FROM dbo.t074t INNER JOIN
      dbo.t074u ON dbo.t074t.mandt = dbo.t074u.mandt AND 
      dbo.t074t.koart = dbo.t074u.koart AND dbo.t074t.shbkz = dbo.t074u.umskz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077d
AS
SELECT dbo.t077d.mandt, dbo.t077d.ktokd, dbo.t077x.spras, dbo.t077x.txt30, 
      dbo.t077d.fausa, dbo.t077d.fausf, dbo.t077d.fausv, dbo.t077d.numkr, 
      dbo.t077d.xcpds, dbo.t077d.kalsm, dbo.t077d.dear1, dbo.t077d.dear2, 
      dbo.t077d.dear3, dbo.t077d.dear4, dbo.t077d.dear5, dbo.t077d.faus1, 
      dbo.t077d.fausw, dbo.t077d.fausg, dbo.t077d.dear6, dbo.t077d.faus2, 
      dbo.t077d.fausu, dbo.tkupa.pargr, dbo.tkupa.kalks, dbo.tkupa.txtgz, dbo.tkupa.txtgf, 
      dbo.tkupa.txtgv, dbo.t077d.zhekou, dbo.t077d.ktoww, dbo.t077d.deflg
FROM dbo.t077d INNER JOIN
      dbo.tkupa ON dbo.t077d.mandt = dbo.tkupa.mandt AND 
      dbo.t077d.ktokd = dbo.tkupa.ktokd INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077d_v
AS
SELECT dbo.t077d.mandt, dbo.t077d.ktokd, dbo.t077v.ktovv, dbo.t077v.ktovvx, 
      dbo.t077w.ktoww
FROM dbo.t077d INNER JOIN
      dbo.t077w ON dbo.t077d.mandt = dbo.t077w.mandt AND 
      dbo.t077d.ktoww = dbo.t077w.ktoww INNER JOIN
      dbo.t077v ON dbo.t077w.mandt = dbo.t077v.mandt AND 
      dbo.t077w.ktoww = dbo.t077v.ktoww

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077d_w
AS
SELECT dbo.t077d.mandt, dbo.t077d.ktokd, dbo.t077x.spras, dbo.t077x.txt30, 
      dbo.t077d.ktoww, dbo.t077d.deflg, dbo.t077d.fausu, dbo.t077d.faus2, dbo.t077d.dear6, 
      dbo.t077d.fausg, dbo.t077d.fausw, dbo.t077d.faus1, dbo.t077d.dear5, 
      dbo.t077d.dear4, dbo.t077d.dear3, dbo.t077d.dear2, dbo.t077d.dear1, 
      dbo.t077d.kalsm, dbo.t077d.xcpds, dbo.t077d.numkr, dbo.t077d.fausv, 
      dbo.t077d.fausf, dbo.t077d.fausa, dbo.t077d.zhekou
FROM dbo.t077d INNER JOIN
      dbo.t077x ON dbo.t077d.mandt = dbo.t077x.mandt AND 
      dbo.t077d.ktokd = dbo.t077x.ktokd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077k
AS
SELECT dbo.t077k.mandt, dbo.t077k.ktokk, dbo.t077y.spras, dbo.t077y.txt30, 
      dbo.t077k.fausa, dbo.t077k.fausf, dbo.t077k.fausm, dbo.t077k.numkr, dbo.t077k.xcpds, 
      dbo.t077k.faus1, dbo.t077k.fausw, dbo.t077k.faust, dbo.t077k.ltsna, dbo.t077k.werkr, 
      dbo.t077k.parge, dbo.t077k.pargt, dbo.t077k.pargw, dbo.t077k.duras, dbo.t077k.ktokd, 
      dbo.t077k.fausg, dbo.t077k.fausn, dbo.t077k.fausx, dbo.t077k.fausu
FROM dbo.t077k INNER JOIN
      dbo.t077y ON dbo.t077k.mandt = dbo.t077y.mandt AND 
      dbo.t077k.ktokk = dbo.t077y.ktokk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077s
AS
SELECT dbo.t077s.mandt, dbo.t077s.ktopl, dbo.t077s.ktoks, dbo.t077z.spras, 
      dbo.t077z.txt30, dbo.t077s.vonnr, dbo.t077s.bisnr, dbo.t077s.fauss, 
      dbo.t077s.layout_o, dbo.t077s.layout_p, dbo.t077s.layout_s, dbo.t077s.bschls, 
      dbo.t077s.bschlh, dbo.t077s.sclass
FROM dbo.t077s INNER JOIN
      dbo.t077z ON dbo.t077s.mandt = dbo.t077z.mandt AND 
      dbo.t077s.ktopl = dbo.t077z.ktopl AND dbo.t077s.ktoks = dbo.t077z.ktoks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077v
AS
SELECT mandt, ktoww, ktovv, ktovvx
FROM dbo.t077v

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t077w
AS
SELECT mandt, ktoww, ktowwx
FROM dbo.t077w

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t134
AS
SELECT dbo.t134.mandt, dbo.t134.mtart, dbo.t134t.spras, dbo.t134t.mtbez, dbo.t134.numki, 
      dbo.t134.numke, dbo.t134.envop, dbo.t134.mtref, dbo.t134.mbref, dbo.t134.flref, 
      dbo.t134.bsext, dbo.t134.bsint, dbo.t134.pstat, dbo.t134.kkref, dbo.t134.vprsv, 
      dbo.t134.kzvpr, dbo.t134.vmtpo, dbo.t134.ekalr, dbo.t134.kzgrp, dbo.t134.kzkfg, 
      dbo.t134.kzprc, dbo.t134.begru, dbo.t134.kzpip, dbo.t134.prdru, dbo.t134.aranz, 
      dbo.t134.wmakg, dbo.t134.izust, dbo.t134.ardel, dbo.t134.kzmpn, dbo.t134.mstae, 
      dbo.t134.szgrp, dbo.t134.crgrp, dbo.t134.bctyp, dbo.t134.sepor, dbo.t134.numkb, 
      dbo.t134.icon, dbo.t134.ktgrm, dbo.t134.numfg
FROM dbo.t134 INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND dbo.t134.mtart = dbo.t134t.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t134_v
AS
SELECT dbo.t134.mandt, dbo.t134.mtart, dbo.t134t.spras, dbo.t134t.mtbez, 
      dbo.t134.vmtpo
FROM dbo.t134 INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND dbo.t134.mtart = dbo.t134t.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t134g_ws
AS
SELECT dbo.t134g.mandt, dbo.t134g.werks, dbo.t134g.spart, dbo.tspat.spartx, 
      dbo.t134g.gsber, dbo.tgsbt.gtext
FROM dbo.t134g INNER JOIN
      dbo.tspa ON dbo.t134g.mandt = dbo.tspa.mandt AND 
      dbo.t134g.spart = dbo.tspa.spart INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND 
      dbo.tspa.spart = dbo.tspat.spart INNER JOIN
      dbo.tgsb ON dbo.t134g.mandt = dbo.tgsb.mandt AND 
      dbo.t134g.gsber = dbo.tgsb.gsber INNER JOIN
      dbo.tgsbt ON dbo.tgsb.mandt = dbo.tgsbt.mandt AND 
      dbo.tgsb.gsber = dbo.tgsbt.gsber AND dbo.tspat.spras = dbo.tgsbt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t148
AS
SELECT dbo.t148.mandt, dbo.t148.sobkz, dbo.t148t.spras, dbo.t148t.sotxt, dbo.t148.sobfi, 
      dbo.t148.soblo, dbo.t148.sobvo
FROM dbo.t148 INNER JOIN
      dbo.t148t ON dbo.t148.mandt = dbo.t148t.mandt AND 
      dbo.t148.sobkz = dbo.t148t.sobkz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t151
AS
SELECT dbo.t151.mandt, dbo.t151.kdgrp, dbo.t151t.spras, dbo.t151t.ktext, 
      dbo.t151.bezbg
FROM dbo.t151 INNER JOIN
      dbo.t151t ON dbo.t151.mandt = dbo.t151t.mandt AND dbo.t151.kdgrp = dbo.t151t.kdgrp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t156b
AS
SELECT dbo.t156b.mandt, dbo.t156b.bwart, dbo.t156b.sobkz, dbo.t156b.umsok, 
      dbo.t156b.xlist, dbo.t156b.fausw, dbo.t156b.chsmm, dbo.t156b.umchs, 
      dbo.t156b.kzchp, dbo.t156b.umchp, dbo.t156b.migo_exist, dbo.t156.shkzg
FROM dbo.t156 INNER JOIN
      dbo.t156b ON dbo.t156.mandt = dbo.t156b.mandt AND 
      dbo.t156.bwart = dbo.t156b.bwart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t158v
AS
SELECT dbo.t158v.vgart, dbo.t158w.spras, dbo.t158w.ltext, dbo.t158v.xvorg, 
      dbo.t158v.j_1bnfrel, dbo.t158v.xmkpf, dbo.t158v.xikpf, dbo.t158v.xrkpf
FROM dbo.t158v INNER JOIN
      dbo.t158w ON dbo.t158v.vgart = dbo.t158w.vgart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t161
AS
SELECT dbo.t161.mandt, dbo.t161.bstyp, dbo.t161.bsart, dbo.t161t.spras, dbo.t161t.batxt, 
      dbo.t161.bsakz, dbo.t161.pincr, dbo.t161.numki, dbo.t161.numke, dbo.t161.brefn, 
      dbo.t161.refba, dbo.t161.abvor, dbo.t161.stafo, dbo.t161.upinc, dbo.t161.stako, 
      dbo.t161.pargr, dbo.t161.numka, dbo.t161.hityp, dbo.t161.lphis, dbo.t161.gsfrg, 
      dbo.t161.variante, dbo.t161.shenq, dbo.t161.kzale, dbo.t161.abgebot, dbo.t161.kornr, 
      dbo.t161.umlif, dbo.t161.koett, dbo.t161.ar_object, dbo.t161.koako
FROM dbo.t161 INNER JOIN
      dbo.t161t ON dbo.t161.mandt = dbo.t161t.mandt AND 
      dbo.t161.bsart = dbo.t161t.bsart AND dbo.t161.bstyp = dbo.t161t.bstyp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.v_t161p
AS
SELECT dbo.t161p.mandt, dbo.t163y.spras, dbo.t161p.bsart, dbo.t161p.bstyp, 
      dbo.t161p.pstyp, dbo.t161t.batxt, dbo.t163y.epstp, dbo.t163y.ptext
FROM dbo.t161p INNER JOIN
      dbo.t163 ON dbo.t161p.mandt = dbo.t163.mandt AND 
      dbo.t161p.pstyp = dbo.t163.pstyp INNER JOIN
      dbo.t163y ON dbo.t163.mandt = dbo.t163y.mandt AND 
      dbo.t163.pstyp = dbo.t163y.pstyp INNER JOIN
      dbo.t161 ON dbo.t161p.mandt = dbo.t161.mandt AND 
      dbo.t161p.bstyp = dbo.t161.bstyp AND dbo.t161p.bsart = dbo.t161.bsart INNER JOIN
      dbo.t161t ON dbo.t161.mandt = dbo.t161t.mandt AND 
      dbo.t161.bstyp = dbo.t161t.bstyp AND dbo.t161.bsart = dbo.t161t.bsart AND 
      dbo.t163y.spras = dbo.t161t.spras


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.v_t163
AS
SELECT dbo.t163.mandt, dbo.t163y.spras, dbo.t163.pstyp, dbo.t163y.ptext, 
      dbo.t163y.epstp, dbo.t163.matno, dbo.t163.kntzu, dbo.t163.bfknz, dbo.t163.wepos, 
      dbo.t163.wepov, dbo.t163.weunb, dbo.t163.weunv, dbo.t163.repos, dbo.t163.repov, 
      dbo.t163.stafo, dbo.t163.j_1bitmtyp
FROM dbo.t163 INNER JOIN
      dbo.t163y ON dbo.t163.mandt = dbo.t163y.mandt AND 
      dbo.t163.pstyp = dbo.t163y.pstyp


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t169
AS
SELECT dbo.tstct.spras, dbo.t169.tcode, dbo.tstct.ttext, dbo.t169.trtyp, dbo.t169.vgart, 
      dbo.t169.acti1, dbo.t169.funcl, dbo.t169.trart
FROM dbo.t169 INNER JOIN
      dbo.tstc ON dbo.t169.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t171
AS
SELECT dbo.t171.mandt, dbo.t171.bzirk, dbo.t171t.spras, dbo.t171t.bztxt
FROM dbo.t171 INNER JOIN
      dbo.t171t ON dbo.t171.mandt = dbo.t171t.mandt AND dbo.t171.bzirk = dbo.t171t.bzirk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t189
AS
SELECT dbo.t189.mandt, dbo.t189.pltyp, dbo.t189t.spras, dbo.t189t.ptext, dbo.t189.reflg, 
      dbo.t189.replt, dbo.t189.ratio, dbo.t189.ernam, dbo.t189.erdat
FROM dbo.t189 INNER JOIN
      dbo.t189t ON dbo.t189.mandt = dbo.t189t.mandt AND dbo.t189.pltyp = dbo.t189t.pltyp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t350w
AS
SELECT dbo.t350w.mandt, dbo.t350w.iwerk, dbo.t350w.auart, dbo.t001w.name1, 
      dbo.t003p.txt
FROM dbo.t003o INNER JOIN
      dbo.t350w ON dbo.t003o.mandt = dbo.t350w.mandt AND 
      dbo.t003o.auart = dbo.t350w.auart INNER JOIN
      dbo.t003p ON dbo.t003o.mandt = dbo.t003p.mandt AND 
      dbo.t003o.auart = dbo.t003p.auart INNER JOIN
      dbo.t399i ON dbo.t350w.mandt = dbo.t399i.mandt AND 
      dbo.t350w.iwerk = dbo.t399i.iwerk INNER JOIN
      dbo.t001w ON dbo.t399i.mandt = dbo.t001w.mandt AND 
      dbo.t399i.iwerk = dbo.t001w.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t350w_a
AS
SELECT dbo.t350w.mandt, dbo.t350w.iwerk, dbo.t350w.auart, dbo.t003p.spras, 
      dbo.t003p.txt
FROM dbo.t350w INNER JOIN
      dbo.t003p ON dbo.t350w.mandt = dbo.t003p.mandt AND 
      dbo.t350w.auart = dbo.t003p.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t356
AS
SELECT dbo.t356.mandt, dbo.t356.artpr, dbo.t356.priok, dbo.t356.tagbn, dbo.t356.tagen, 
      dbo.t356_t.priokx, dbo.t356.ehtbg, dbo.t356.ehten, dbo.t356_t.spras, 
      dbo.t356a_t.artprx
FROM dbo.t356 INNER JOIN
      dbo.t356_t ON dbo.t356.mandt = dbo.t356_t.mandt AND 
      dbo.t356.artpr = dbo.t356_t.artpr AND dbo.t356.priok = dbo.t356_t.priok INNER JOIN
      dbo.t356a ON dbo.t356.mandt = dbo.t356a.mandt AND 
      dbo.t356.artpr = dbo.t356a.artpr INNER JOIN
      dbo.t356a_t ON dbo.t356a.mandt = dbo.t356a_t.mandt AND 
      dbo.t356a.artpr = dbo.t356a_t.artpr AND dbo.t356_t.spras = dbo.t356a_t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t356a
AS
SELECT dbo.t356a.mandt, dbo.t356a.artpr, dbo.t356a_t.spras, dbo.t356a_t.artprx
FROM dbo.t356a INNER JOIN
      dbo.t356a_t ON dbo.t356a.mandt = dbo.t356a_t.mandt AND 
      dbo.t356a.artpr = dbo.t356a_t.artpr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t370f
AS
SELECT dbo.t370f.mandt, dbo.t370f.fltyp, dbo.t370f_t.spras, dbo.t370f_t.typtx, 
      dbo.t370f.sales
FROM dbo.t370f INNER JOIN
      dbo.t370f_t ON dbo.t370f.mandt = dbo.t370f_t.mandt AND 
      dbo.t370f.fltyp = dbo.t370f_t.fltyp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t370s
AS
SELECT dbo.t370s.mandt, dbo.t370s.tplkz, dbo.t370s_t.spras, dbo.t370s_t.tplxt, 
      dbo.t370s.editm, dbo.t370s.stufm
FROM dbo.t370s INNER JOIN
      dbo.t370s_t ON dbo.t370s.mandt = dbo.t370s_t.mandt AND 
      dbo.t370s.tplkz = dbo.t370s_t.tplkz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t399i
AS
SELECT dbo.t399i.mandt, dbo.t399i.iwerk, dbo.t001w.name1, dbo.t001w.name2
FROM dbo.t399i INNER JOIN
      dbo.t001w ON dbo.t399i.mandt = dbo.t001w.mandt AND 
      dbo.t399i.iwerk = dbo.t001w.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t500p
AS
SELECT mandt, persa, name1, molga, bukrs
FROM dbo.t500p

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t501
AS
SELECT dbo.t501.mandt, dbo.t501t.sprsl, dbo.t501.persg, dbo.t501t.ptext
FROM dbo.t501 INNER JOIN
      dbo.t501t ON dbo.t501.mandt = dbo.t501t.mandt AND dbo.t501.persg = dbo.t501t.persg

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t503k
AS
SELECT dbo.t503k.mandt, dbo.t503t.sprsl, dbo.t503k.persk, dbo.t503t.ptext
FROM dbo.t503k INNER JOIN
      dbo.t503t ON dbo.t503k.mandt = dbo.t503t.mandt AND 
      dbo.t503k.persk = dbo.t503t.persk

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t503z
AS
SELECT dbo.t503z.mandt, dbo.t503z.persg, dbo.t503z.persk, dbo.t503z.molga AS modif, 
      dbo.t501t.ptext AS pgtxt, dbo.t503t.ptext AS pktxt, dbo.t500t.ltext AS latxt, 
      dbo.t500t.spras
FROM dbo.t503z INNER JOIN
      dbo.t501 ON dbo.t503z.mandt = dbo.t501.mandt AND 
      dbo.t503z.persg = dbo.t501.persg INNER JOIN
      dbo.t503k ON dbo.t503z.mandt = dbo.t503k.mandt AND 
      dbo.t503z.persk = dbo.t503k.persk INNER JOIN
      dbo.t501t ON dbo.t501.mandt = dbo.t501t.mandt AND 
      dbo.t501.persg = dbo.t501t.persg INNER JOIN
      dbo.t503t ON dbo.t503k.mandt = dbo.t503t.mandt AND 
      dbo.t503k.persk = dbo.t503t.persk AND dbo.t501t.sprsl = dbo.t503t.sprsl INNER JOIN
      dbo.t500l ON dbo.t503z.molga = dbo.t500l.molga INNER JOIN
      dbo.t500t ON dbo.t500l.molga = dbo.t500t.molga AND 
      dbo.t503t.sprsl = dbo.t500t.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t522g
AS
SELECT dbo.t522g.mandt, dbo.t522g.anred, dbo.t522t.sprsl, dbo.t522g.gesch, 
      dbo.t522t.atext, dbo.t522t.anrlt
FROM dbo.t522g INNER JOIN
      dbo.t522t ON dbo.t522g.mandt = dbo.t522t.mandt AND 
      dbo.t522g.anred = dbo.t522t.anred

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t681
AS
SELECT dbo.t681.kvewe, dbo.t681.kotabnr, dbo.t681.kotab, dbo.dd02t.ddlanguage, 
      dbo.dd02t.ddtext, dbo.t681.kappl, dbo.t681.setyp, dbo.t681.ksdat, dbo.t681.gesta, 
      dbo.t681.idxak, dbo.t681.idxbd, dbo.t681.sapsy, dbo.t681.sysid, dbo.t681.saprl
FROM dbo.dd02t INNER JOIN
      dbo.dd02l ON dbo.dd02t.tabname = dbo.dd02l.tabname AND 
      dbo.dd02t.as4local = dbo.dd02l.as4local AND 
      dbo.dd02t.as4vers = dbo.dd02l.as4vers INNER JOIN
      dbo.t681 ON dbo.dd02l.tabname = dbo.t681.kotab

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t681a
AS
SELECT dbo.t681a.kappl, dbo.t681b.spras, dbo.t681b.vtext
FROM dbo.t681a INNER JOIN
      dbo.t681b ON dbo.t681a.kappl = dbo.t681b.kappl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t681f
AS
SELECT dbo.t681ft.spras, dbo.t681f.kvewe, dbo.t681f.kappl, dbo.t681f.kfgrp, 
      dbo.t681f.kfdna, dbo.t681ft.vtext
FROM dbo.t681f INNER JOIN
      dbo.t681ft ON dbo.t681f.kvewe = dbo.t681ft.kvewe AND 
      dbo.t681f.kappl = dbo.t681ft.kappl AND dbo.t681f.kfgrp = dbo.t681ft.kfgrp AND 
      dbo.t681f.kfdna = dbo.t681ft.kfdna

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t681v
AS
SELECT dbo.t681w.spras, dbo.t681v.kvewe, dbo.t681w.vtext, dbo.t681v.dikom, 
      dbo.t681v.mustr, dbo.t681v.muprg, dbo.t681v.kmpol, dbo.t681v.dnpvn, 
      dbo.t681v.dnpbs, dbo.t681v.zuprg, dbo.t681v.zukon
FROM dbo.t681v INNER JOIN
      dbo.t681w ON dbo.t681v.kvewe = dbo.t681w.kvewe

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t682
AS
SELECT dbo.t682.kvewe, dbo.t682.kappl, dbo.t682.kozgf, dbo.t682t.spras, dbo.t682t.vtxtm, 
      dbo.t682.kozgv, dbo.t682.kzakt, dbo.t682.kkmod, dbo.t682.rptsx, dbo.t682.mandt, 
      dbo.t682.sypsy, dbo.t682.sysid, dbo.t682.saprl
FROM dbo.t682 INNER JOIN
      dbo.t682t ON dbo.t682.kvewe = dbo.t682t.kvewe AND 
      dbo.t682.kappl = dbo.t682t.kappl AND dbo.t682.kozgf = dbo.t682t.kozgf

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t682i
AS
SELECT kvewe, kappl, kozgf, kolnr, kotabnr, kkidx, kzexl, kkmod, kobed, kkopf, gzugr
FROM dbo.t682i

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t685a
AS
SELECT dbo.t685.mandt, dbo.t685t.spras, dbo.t685.kvewe, dbo.t685.kappl, dbo.t685.kschl, 
      dbo.t685t.vtext, dbo.t685.kozgf, dbo.t685.datvo, dbo.t685.dtvob, dbo.t685a.krech, 
      dbo.t685a.kzbzg, dbo.t685a.kntyp, dbo.t685a.konau, dbo.t685a.kmanu, 
      dbo.t685a.grzgf, dbo.t685a.grlnr, dbo.t685a.grmen, dbo.t685a.ganzz, dbo.t685a.kgrpe, 
      dbo.t685a.kreli, dbo.t685a.kdupl, dbo.t685a.kruek, dbo.t685a.koupd, dbo.t685a.sosta, 
      dbo.t685a.kofrs, dbo.t685a.kalsm, dbo.t685a.kznep, dbo.t685a.koaid, dbo.t685a.kkopf, 
      dbo.t685a.kposi, dbo.t685a.knega, dbo.t685a.txprf, dbo.t685a.kstpr, 
      dbo.t685a.kaend_btr, dbo.t685a.kaend_wrt, dbo.t685a.kaend_ufk, 
      dbo.t685a.kaend_rch, dbo.t685a.kaend_loe, dbo.t685a.kaend_meh, dbo.t685a.idxup, 
      dbo.t685a.rukor, dbo.t685a.bover, dbo.t685a.kprdt, dbo.t685a.stfkz, dbo.t685a.prech, 
      dbo.t685a.bnkwe, dbo.t685a.aktko, dbo.t685a.vkkal, dbo.t685a.vkobl, 
      dbo.t685a.rkappl, dbo.t685a.rkschl, dbo.t685a.bergl, dbo.t685a.vhart, dbo.t685a.rdifa, 
      dbo.t685a.kfkiv, dbo.t685a.kvarc, dbo.t685a.kmeng, dbo.t685a.mdflg, dbo.t685a.ktrel, 
      dbo.t685a.bnktk, dbo.t685a.kfrst, dbo.t685a.txtgr, dbo.t685a.tdid
FROM dbo.t685 INNER JOIN
      dbo.t685t ON dbo.t685.mandt = dbo.t685t.mandt AND 
      dbo.t685.kvewe = dbo.t685t.kvewe AND dbo.t685.kappl = dbo.t685t.kappl AND 
      dbo.t685.kschl = dbo.t685t.kschl INNER JOIN
      dbo.t685a ON dbo.t685.mandt = dbo.t685a.mandt AND 
      dbo.t685.kappl = dbo.t685a.kappl AND dbo.t685.kschl = dbo.t685a.kschl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t687
AS
SELECT dbo.t687.mandt, dbo.t687.kappl, dbo.t687.kvsl1, dbo.t687t.spras, 
      dbo.t687t.vtext
FROM dbo.t687 INNER JOIN
      dbo.t687t ON dbo.t687.mandt = dbo.t687t.mandt AND 
      dbo.t687.kappl = dbo.t687t.kappl AND dbo.t687.kvsl1 = dbo.t687t.kvsl1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t691a
AS
SELECT dbo.t691a.mandt, dbo.t691a.ctlpc, dbo.t691a.kkber, dbo.t691t.spras, 
      dbo.t691t.rtext
FROM dbo.t691a INNER JOIN
      dbo.t691t ON dbo.t691a.mandt = dbo.t691t.mandt AND 
      dbo.t691a.ctlpc = dbo.t691t.ctlpc AND dbo.t691a.kkber = dbo.t691t.kkber

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t691d
AS
SELECT dbo.t691d.mandt, dbo.t691d.crmgr_cm, dbo.t691e.spras, 
      dbo.t691e.grbez_cm
FROM dbo.t691d INNER JOIN
      dbo.t691e ON dbo.t691d.mandt = dbo.t691e.mandt AND 
      dbo.t691d.crmgr_cm = dbo.t691e.crmgr_cm

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_t691f
AS
SELECT dbo.t691f.mandt, dbo.t691f.kkber, dbo.t691f.ctlpc, dbo.t691f.crmgr, 
      dbo.t691g.spras, dbo.t691g.prbez, dbo.t691f.crdbi, dbo.t691f.crprc, dbo.t691f.crcre, 
      dbo.t691f.dklim, dbo.t691f.seaaf, dbo.t691f.tagef, dbo.t691f.cmpaa, dbo.t691f.stvaw, 
      dbo.t691f.stvlw, dbo.t691f.strea, dbo.t691f.stset, dbo.t691f.cmpab, dbo.t691f.wovlw, 
      dbo.t691f.wswin, dbo.t691f.wsrea, dbo.t691f.wlset, dbo.t691f.cmpac, dbo.t691f.maval, 
      dbo.t691f.marea, dbo.t691f.maset, dbo.t691f.cmpad, dbo.t691f.fsrea, dbo.t691f.fsset, 
      dbo.t691f.cmpae, dbo.t691f.rvtol, dbo.t691f.rsrea, dbo.t691f.reset, dbo.t691f.cmpaf, 
      dbo.t691f.pdmax, dbo.t691f.pdtol, dbo.t691f.pdrea, dbo.t691f.pdset, dbo.t691f.cmpag, 
      dbo.t691f.oitol, dbo.t691f.oirea, dbo.t691f.oiset, dbo.t691f.cmpah, dbo.t691f.dunng, 
      dbo.t691f.dnrea, dbo.t691f.dnset, dbo.t691f.cmpai, dbo.t691f.spval, dbo.t691f.sprea, 
      dbo.t691f.spset, dbo.t691f.cmpaj, dbo.t691f.pjrea, dbo.t691f.pjset, dbo.t691f.cmpak, 
      dbo.t691f.pkrea, dbo.t691f.pkset, dbo.t691f.cmpal, dbo.t691f.plrea, dbo.t691f.usrpr0, 
      dbo.t691f.plset, dbo.t691f.usr0rea, dbo.t691f.usr0set, dbo.t691f.usrpr1, 
      dbo.t691f.usr1rea, dbo.t691f.usr1set, dbo.t691f.usrpr2, dbo.t691f.usr2rea, 
      dbo.t691f.usr2set, dbo.t691f.cecki, dbo.t691f.regul, dbo.t691f.grpno, dbo.t691f.dvsea, 
      dbo.t691f.dbsea, dbo.t691f.erlta, dbo.t691f.erlst, dbo.t691f.seaafpm
FROM dbo.t691f INNER JOIN
      dbo.t691g ON dbo.t691f.mandt = dbo.t691g.mandt AND 
      dbo.t691f.kkber = dbo.t691g.kkber AND dbo.t691f.ctlpc = dbo.t691g.ctlpc AND 
      dbo.t691f.crmgr = dbo.t691g.crmgr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tb01
AS
SELECT mandt, pcart, pctxt
FROM dbo.tb01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tb02
AS
SELECT mandt, mdnmr, mdtxt
FROM dbo.tb02

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tb033
AS
SELECT dbo.tb033.mandt, dbo.tb033.ccins, dbo.tb033t.spras, dbo.tb033t.bez30, 
      dbo.tb033.crule
FROM dbo.tb033 INNER JOIN
      dbo.tb033t ON dbo.tb033.mandt = dbo.tb033t.mandt AND 
      dbo.tb033.ccins = dbo.tb033t.ccins

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tbsl
AS
SELECT dbo.tbsl.mandt, dbo.tbsl.bschl, dbo.tbslt.spras, dbo.tbslt.umskz, dbo.tbsl.shkzg, 
      dbo.tbsl.koart, dbo.tbsl.xumsw, dbo.tbsl.faus1, dbo.tbsl.xzahl, dbo.tbsl.stbsl, 
      dbo.tbsl.xsonu, dbo.tbsl.faus2, dbo.tbslt.ltext
FROM dbo.tbsl INNER JOIN
      dbo.tbslt ON dbo.tbsl.mandt = dbo.tbslt.mandt AND dbo.tbsl.bschl = dbo.tbslt.bschl

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tc24
AS
SELECT mandt, werks, veran, ktext, isasn
FROM dbo.tc24

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tcmg
AS
SELECT dbo.tcmg.mandt, dbo.tcmg.atkla, dbo.tcmgt.spras, dbo.tcmgt.atklt
FROM dbo.tcmg INNER JOIN
      dbo.tcmgt ON dbo.tcmg.mandt = dbo.tcmgt.mandt AND 
      dbo.tcmg.atkla = dbo.tcmgt.atkla

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tcms
AS
SELECT dbo.tcms.mandt, dbo.tcmst.spras, dbo.tcms.atmst, dbo.tcmst.atstt, dbo.tcms.atfre, 
      dbo.tcms.atlvm, dbo.tcms.dlock, dbo.tcms.auprf
FROM dbo.tcms INNER JOIN
      dbo.tcmst ON dbo.tcms.mandt = dbo.tcmst.mandt AND 
      dbo.tcms.atmst = dbo.tcmst.atmst

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tcon
AS
SELECT dbo.tcon.mandt, dbo.tcon.connr, dbo.tcon.pcart, dbo.tb01.pctxt, dbo.tcon.contx, 
      dbo.tcon.ernam, dbo.tcon.erdat, dbo.tcon.aenam, dbo.tcon.aedat, dbo.tcon.tplnr, 
      dbo.tcon.fmdat, dbo.tcon.todat
FROM dbo.tb01 INNER JOIN
      dbo.tcon ON dbo.tb01.mandt = dbo.tcon.mandt AND dbo.tb01.pcart = dbo.tcon.pcart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tcurr
AS
SELECT mandt, kurst, fcurr, tcurr, gdatu, ukurs
FROM dbo.tcurr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tcurv
AS
SELECT dbo.tcurv.mandt, dbo.tcurv.kurst, dbo.tcurv.xinvr, dbo.tcurw.curvw, 
      dbo.tcurw.spras, dbo.tcurv.bwaer, dbo.tcurv.xbwrl, dbo.tcurv.gkuzu, dbo.tcurv.bkuzu, 
      dbo.tcurv.xfixd, dbo.tcurv.xeuro
FROM dbo.tcurv INNER JOIN
      dbo.tcurw ON dbo.tcurv.mandt = dbo.tcurw.mandt AND 
      dbo.tcurv.kurst = dbo.tcurw.kurst

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tfkb
AS
SELECT dbo.tfkb.mandt, dbo.tfkb.fkber, dbo.tfkbt.spras, dbo.tfkbt.fkbtx
FROM dbo.tfkb INNER JOIN
      dbo.tfkbt ON dbo.tfkb.mandt = dbo.tfkbt.mandt AND dbo.tfkb.fkber = dbo.tfkbt.fkber

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tgsb
AS
SELECT dbo.tgsb.mandt, dbo.tgsb.gsber, dbo.tgsbt.spras, dbo.tgsbt.gtext, 
      dbo.tgsb.gsber_kons, dbo.tgsb.gsber_glob
FROM dbo.tgsb INNER JOIN
      dbo.tgsbt ON dbo.tgsb.mandt = dbo.tgsbt.mandt AND dbo.tgsb.gsber = dbo.tgsbt.gsber

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tgsbk
AS
SELECT dbo.tgsbk.mandt, dbo.tgsbl.spras, dbo.tgsbk.gsber_kons, dbo.tgsbl.txt
FROM dbo.tgsbk INNER JOIN
      dbo.tgsbl ON dbo.tgsbk.mandt = dbo.tgsbl.mandt AND 
      dbo.tgsbk.gsber_kons = dbo.tgsbl.gsber_kons

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_ale
AS
SELECT mandt, kokrs, bezei, logsystem, alemt, md_logsystem
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_del
AS
SELECT mandt, kokrs, bezei
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_er
AS
SELECT mandt, kokrs, bezei, erkrs
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_fm
AS
SELECT mandt, kokrs, bezei, fikrs
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_gd
AS
SELECT mandt, kokrs, bezei, waers, ktopl, lmona, kokfik, khinr, xwbuk, blart, rclac, ctyp, 
      cvprof, cvact, vname, auth_use_no_std, auth_use_add1, auth_use_add2, 
      auth_ke_no_std, auth_ke_use_add1, auth_ke_use_add2
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_ka
AS
SELECT mandt, kokrs, bezei, kstar_fin, kstar_fid
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_pc
AS
SELECT mandt, kokrs, bezei, dprct, phinr, pcbel, pcacur, pcacurtp, pcatrcur, pca_valu, 
      pca_alemt
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_pca_ale
AS
SELECT mandt, kokrs, bezei, logsystem, pca_alemt, md_logsystem, dprct, phinr
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka01_pp
AS
SELECT mandt, kokrs, bezei, bphinr, xbpale
FROM dbo.tka01

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tka02
AS
SELECT dbo.tka02.mandt, dbo.tka02.bukrs, dbo.tka02.gsber, dbo.t001.butxt, 
      dbo.tka02.kokrs, dbo.tka01.bezei, dbo.tka01.kokfik, dbo.t001.kokfi
FROM dbo.tka02 INNER JOIN
      dbo.tka01 ON dbo.tka02.mandt = dbo.tka01.mandt AND 
      dbo.tka02.kokrs = dbo.tka01.kokrs INNER JOIN
      dbo.t001 ON dbo.tka02.mandt = dbo.t001.mandt AND dbo.tka02.bukrs = dbo.t001.bukrs

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tkb1a
AS
SELECT dbo.tkb1a.mandt, dbo.tkb1a.aprof, dbo.tkb1b.spras, dbo.tkb1b.ptext, 
      dbo.tkb1a.abrist, dbo.tkb1a.plvom, dbo.tkb1a.preis, dbo.tkb1a.epupd, dbo.tkb1a.hproz, 
      dbo.tkb1a.maxbr, dbo.tkb1a.betra, dbo.tkb1a.menge, dbo.tkb1a.proze, dbo.tkb1a.aqzif, 
      dbo.tkb1a.absch, dbo.tkb1a.ursch, dbo.tkb1a.kovor, dbo.tkb1a.absak, dbo.tkb1a.abkst, 
      dbo.tkb1a.abauf, dbo.tkb1a.abpro, dbo.tkb1a.abanl, dbo.tkb1a.abmat, 
      dbo.tkb1a.abnvg, dbo.tkb1a.abbob, dbo.tkb1a.abprc, dbo.tkb1a.abimm, 
      dbo.tkb1a.blart, dbo.tkb1a.resau, dbo.tkb1a.ersch, dbo.tkb1a.rifind, dbo.tkb1a.hbschl, 
      dbo.tkb1a.sbschl, dbo.tkb1a.varia, dbo.tkb1a.abktr, dbo.tkb1a.abprz, dbo.tkb1a.abaup, 
      dbo.tkb1a.ernam, dbo.tkb1a.erdat, dbo.tkb1a.aenam, dbo.tkb1a.aedat
FROM dbo.tkb1a INNER JOIN
      dbo.tkb1b ON dbo.tkb1a.mandt = dbo.tkb1b.mandt AND 
      dbo.tkb1a.aprof = dbo.tkb1b.aprof

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tkukl
AS
SELECT dbo.tkukl.mandt, dbo.tkukl.kukla, dbo.tkukt.spras, dbo.tkukt.vtext
FROM dbo.tkukl INNER JOIN
      dbo.tkukt ON dbo.tkukl.mandt = dbo.tkukt.mandt AND dbo.tkukl.kukla = dbo.tkukt.kukla

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tmabc
AS
SELECT dbo.tmabc.mandt, dbo.tmabc.maabc, dbo.tmabct.spras, dbo.tmabct.tmabc
FROM dbo.tmabc INNER JOIN
      dbo.tmabct ON dbo.tmabc.mandt = dbo.tmabct.mandt AND 
      dbo.tmabc.maabc = dbo.tmabct.maabc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tobc
AS
SELECT dbo.tobc.oclss, dbo.tobct.langu, dbo.tobct.ctext
FROM dbo.tobc INNER JOIN
      dbo.tobct ON dbo.tobc.oclss = dbo.tobct.oclss

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tobj
AS
SELECT dbo.tobj.objct, dbo.tobjt.langu, dbo.tobjt.ttext, dbo.tobj.fiel1, dbo.tobj.fiel2, 
      dbo.tobj.fiel3, dbo.tobj.fiel4, dbo.tobj.fiel5, dbo.tobj.fiel6, dbo.tobj.fiel7, dbo.tobj.fiel8, 
      dbo.tobj.fiel9, dbo.tobj.fiel0, dbo.tobj.oclss, dbo.tobj.bname, dbo.tobj.fblock, 
      dbo.tobj.conversion
FROM dbo.tobj INNER JOIN
      dbo.tobjt ON dbo.tobj.objct = dbo.tobjt.object

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tokba1
AS
SELECT dbo.tokba.mandt, dbo.tokba.ismtp, dbo.tokbat.spras, dbo.tokbat.ismtext, 
      dbo.tokba.ismart, dbo.tokba.artpr, dbo.tokba.ismcat, dbo.tokba.opcat, dbo.tokba.prcat, 
      dbo.tokba.cacat, dbo.tokba.rbnr, dbo.tokba.isltp, dbo.tokba.smnrg, 
      dbo.tokba.stsma
FROM dbo.tokba INNER JOIN
      dbo.tokbat ON dbo.tokba.mandt = dbo.tokbat.mandt AND 
      dbo.tokba.ismtp = dbo.tokbat.ismtp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tokbb
AS
SELECT dbo.tokbb.mandt, dbo.tokbb.isltp, dbo.tokbbt.spras, dbo.tokbbt.isoltx, 
      dbo.tokbb.islart, dbo.tokbb.actcat, dbo.tokbb.rbnr, dbo.tokbb.tkcat, dbo.tokbb.slnrg, 
      dbo.tokbb.ismtp, dbo.tokbb.stsma
FROM dbo.tokbb INNER JOIN
      dbo.tokbbt ON dbo.tokbb.mandt = dbo.tokbbt.mandt AND 
      dbo.tokbb.isltp = dbo.tokbbt.isltp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tokbc
AS
SELECT dbo.tokbc.mandt, dbo.tokbc.ismtp, dbo.tokbc.ismart, dbo.tokbct.spras, 
      dbo.tokbct.txtsmart
FROM dbo.tokbc INNER JOIN
      dbo.tokbct ON dbo.tokbc.mandt = dbo.tokbct.mandt AND 
      dbo.tokbc.ismtp = dbo.tokbct.ismtp AND dbo.tokbc.ismart = dbo.tokbct.ismart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tokbf
AS
SELECT dbo.tokbf.mandt, dbo.tokbf.isltp, dbo.tokbf.islart, dbo.tokbft.spras, 
      dbo.tokbft.txtslart
FROM dbo.tokbf INNER JOIN
      dbo.tokbft ON dbo.tokbf.mandt = dbo.tokbft.mandt AND 
      dbo.tokbf.isltp = dbo.tokbft.isltp AND dbo.tokbf.islart = dbo.tokbft.islart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tptm
AS
SELECT dbo.tptm.mandt, dbo.tptm.mtpos, dbo.tptmt.spras, dbo.tptmt.bezei
FROM dbo.tptm INNER JOIN
      dbo.tptmt ON dbo.tptm.mandt = dbo.tptmt.mandt AND 
      dbo.tptm.mtpos = dbo.tptmt.mtpos

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq15
AS
SELECT dbo.tq15.mandt, dbo.tq15.katalogart, dbo.tq15.kam, dbo.tq15.kamb, 
      dbo.tq15.ersteller, dbo.tq15.e_datum, dbo.tq15.aenderer, dbo.tq15.a_datum, 
      dbo.tq15.hist, dbo.tq15.qcatfoa, dbo.tq15t.spras, dbo.tq15t.katalogtxt, 
      dbo.tq15t.schlagwort
FROM dbo.tq15 INNER JOIN
      dbo.tq15t ON dbo.tq15.mandt = dbo.tq15t.mandt AND 
      dbo.tq15.katalogart = dbo.tq15t.katalogart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.stini, dbo.tq80.stdspa, dbo.tq80.stdsp, dbo.tq80.stadf, dbo.tq80.sthdn, 
      dbo.tq80.stacu, dbo.tq80.stcan, dbo.tq80.canca, dbo.tq80.ackca, dbo.tq80.qmtyp, 
      dbo.tq80.rbnr, dbo.tq80.herkz, dbo.tq80.bezzt, dbo.tq80.qmnuk, dbo.tq80.auart, 
      dbo.tq80.hscrtp, dbo.tq80.oscrtp, dbo.tq80.pscrtp, dbo.tq80.pargr, dbo.tq80.stsma, 
      dbo.tq80.smstsma, dbo.tq80.artpr, dbo.tq80.sdauart, dbo.tq80.coauart, 
      dbo.tq80.parvw_kund, dbo.tq80.parvw_ap, dbo.tq80.parvw_int, dbo.tq80.parvw_lief, 
      dbo.tq80.parvw_her, dbo.tq80.parvw_vera, dbo.tq80.parvw_auto, 
      dbo.tq80.parvw_qmsm, dbo.tq80.klakt, dbo.tq80.info_wind, dbo.tq80.serwi, 
      dbo.tq80.escal, dbo.tq80.fekat, dbo.tq80.urkat, dbo.tq80.makat, dbo.tq80.mfkat, 
      dbo.tq80.otkat, dbo.tq80.sakat, dbo.tq80.stafo, dbo.tq80.qmwaers, dbo.tq80.qmwert, 
      dbo.tq80.fbs_create, dbo.tq80.fbs_dynnr, dbo.tq80.tdformat, dbo.tq80.kzeile, 
      dbo.tq80.userscr1, dbo.tq80.userscr2, dbo.tq80.userscr3, dbo.tq80.userscr4, 
      dbo.tq80.userscr5, dbo.tq80.qmltxt01, dbo.tq80.qmltxt02, dbo.tq80.auart2, 
      dbo.tq80.early_num, dbo.tq80.autom_cont, dbo.tq80.matkz, dbo.tq80.kukz, 
      dbo.tq80.matkukz, dbo.tq80.likz, dbo.tq80.matlikz, dbo.tq80.mod, dbo.tq80.icon1, 
      dbo.tq80.icon2, dbo.tq80.vers, dbo.tq80.zeitraum, dbo.tq80.tdobject, dbo.tq80.tdname, 
      dbo.tq80.tdid, dbo.tq80.cmcheck_sm, dbo.tq80.cmgra, dbo.tq80.parvw_page
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_a
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.qmtyp, dbo.tq80.auart
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_art
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80.auart, dbo.t003p.spras, 
      dbo.t003p.txt
FROM dbo.tq80 INNER JOIN
      dbo.t003o ON dbo.tq80.mandt = dbo.t003o.mandt AND 
      dbo.tq80.auart = dbo.t003o.auart INNER JOIN
      dbo.t003p ON dbo.t003o.mandt = dbo.t003p.mandt AND 
      dbo.t003o.auart = dbo.t003p.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_c
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.fekat, dbo.tq80.urkat, dbo.tq80.makat, dbo.tq80.mfkat, dbo.tq80.otkat, 
      dbo.tq80.klakt, dbo.tq80.sakat, dbo.tq80.rbnr, dbo.tq80.mod
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_m
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.qmartx, dbo.tq80.qmtyp, 
      dbo.tq80.herkz, dbo.tq80.bezzt, dbo.tq80.rbnr, dbo.tq80.stafo, dbo.tq80.early_num, 
      dbo.tq80.qmnuk, dbo.tq80.pargr, dbo.tq80_t.spras
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_num
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.qmnuk
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_op
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.canca, dbo.tq80.ackca
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tq80_st
AS
SELECT dbo.tq80.mandt, dbo.tq80.qmart, dbo.tq80_t.spras, dbo.tq80_t.qmartx, 
      dbo.tq80.stini, dbo.tq80.stdspa, dbo.tq80.stdsp, dbo.tq80.stadf, dbo.tq80.sthdn, 
      dbo.tq80.stacu, dbo.tq80.stcan
FROM dbo.tq80 INNER JOIN
      dbo.tq80_t ON dbo.tq80.mandt = dbo.tq80_t.mandt AND 
      dbo.tq80.qmart = dbo.tq80_t.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tspa
AS
SELECT dbo.tspa.mandt, dbo.tspa.spart, dbo.tspat.spartx, dbo.tspa.dlflg, 
      dbo.tspat.spras
FROM dbo.tspa INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND dbo.tspa.spart = dbo.tspat.spart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tstc
AS
SELECT dbo.tstct.spras, dbo.tstc.tcode, dbo.tstct.ttext, dbo.tstc.pgmna, dbo.tstc.dypno, 
      dbo.tstc.menue, dbo.tstc.cinfo, dbo.tstc.arbgb
FROM dbo.tstc INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_ttds
AS
SELECT dbo.ttds.mandt, dbo.ttds.tplst, dbo.ttdst.spras, dbo.ttdst.bezei, dbo.ttds.fabkl, 
      dbo.ttds.adrnr, dbo.ttds.tpsid, dbo.ttds.vtparn, dbo.ttds.vtpart, dbo.ttds.kschl, 
      dbo.ttds.nketp, dbo.ttds.traend, dbo.ttds.sttrg, dbo.ttds.tpssf, dbo.ttds.tpstx1, 
      dbo.ttds.tpstx3, dbo.ttds.tpstx2, dbo.ttds.bukrs, dbo.ttds.stagew, dbo.ttds.stavol, 
      dbo.ttds.stadis
FROM dbo.ttds INNER JOIN
      dbo.ttdst ON dbo.ttds.mandt = dbo.ttdst.mandt AND dbo.ttds.tplst = dbo.ttdst.tplst

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvak
AS
SELECT dbo.tvak.mandt, dbo.tvak.auart, dbo.tvakt.spras, dbo.tvakt.auartx, dbo.tvak.numki, 
      dbo.tvak.numke, dbo.tvak.incpo, dbo.tvak.incup, dbo.tvak.ernam, dbo.tvak.faksk, 
      dbo.tvak.lined, dbo.tvak.lisof, dbo.tvak.lfarv, dbo.tvak.vbtyp, dbo.tvak.sperr, 
      dbo.tvak.klimp, dbo.tvak.cmgra, dbo.tvak.prbst, dbo.tvak.extnr_rma, dbo.tvak.lifsm, 
      dbo.tvak.lifsk, dbo.tvak.kopgr, dbo.tvak.fkarv, dbo.tvak.fkara, dbo.tvak.fkaiv, 
      dbo.tvak.bezob
FROM dbo.tvak INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND dbo.tvak.auart = dbo.tvakt.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvakz
AS
SELECT dbo.tvakz.mandt, dbo.tvakz.vkorg, dbo.tvakz.vtweg, dbo.tvakz.spart, 
      dbo.tvakz.auart, dbo.tvkot.vkorgx, dbo.tvtwt.vtwegx, dbo.tspat.spartx, 
      dbo.tvakt.auartx, dbo.tvkot.spras, dbo.tvakz.kopgr, dbo.tvak.lisof, dbo.tvak.lined
FROM dbo.tvakz INNER JOIN
      dbo.tvko ON dbo.tvakz.mandt = dbo.tvko.mandt AND 
      dbo.tvakz.vkorg = dbo.tvko.vkorg INNER JOIN
      dbo.tvkot ON dbo.tvko.vkorg = dbo.tvkot.vkorg AND 
      dbo.tvko.mandt = dbo.tvkot.mandt INNER JOIN
      dbo.tvtw ON dbo.tvakz.mandt = dbo.tvtw.mandt AND 
      dbo.tvakz.vtweg = dbo.tvtw.vtweg INNER JOIN
      dbo.tspa ON dbo.tvakz.mandt = dbo.tspa.mandt AND 
      dbo.tvakz.spart = dbo.tspa.spart INNER JOIN
      dbo.tvak ON dbo.tvakz.mandt = dbo.tvak.mandt AND 
      dbo.tvakz.auart = dbo.tvak.auart INNER JOIN
      dbo.tvtwt ON dbo.tvtw.mandt = dbo.tvtwt.mandt AND 
      dbo.tvtw.vtweg = dbo.tvtwt.vtweg AND dbo.tvkot.spras = dbo.tvtwt.spras INNER JOIN
      dbo.tspat ON dbo.tspa.mandt = dbo.tspat.mandt AND 
      dbo.tspa.spart = dbo.tspat.spart AND dbo.tvtwt.spras = dbo.tspat.spras INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart AND dbo.tspat.spras = dbo.tvakt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvap
AS
SELECT dbo.tvpt.mandt, dbo.tvpt.pstyv, dbo.tvapt.spras, dbo.tvapt.vtext, dbo.tvap.fkrel, 
      dbo.tvap.prsfd, dbo.tvap.posgr, dbo.tvap.pargr, dbo.tvap.psgrp, dbo.tvap.stuli, 
      dbo.tvap.aswpa, dbo.tvap.strum, dbo.tvap.evrwr, dbo.tvap.kowrr, dbo.tvap.feldp, 
      dbo.tvap.faksp, dbo.tvap.shell, dbo.tvap.erlre, dbo.tvap.txn08, dbo.tvap.diafm, 
      dbo.tvap.kderl, dbo.tvap.txtgr, dbo.tvap.posar, dbo.tvap.lfrel, dbo.tvap.ernam, 
      dbo.tvap.eterl, dbo.tvap.gwrel, dbo.tvap.kompp, dbo.tvap.fehgr, dbo.tvap.shkzg, 
      dbo.tvap.eqmat, dbo.tvap.stgap, dbo.tvap.kalsm, dbo.tvap.sobkz, dbo.tvap.cmpnt, 
      dbo.tvap.strat, dbo.tvap.stsma, dbo.tvap.meng1, dbo.tvap.alekz, dbo.tvap.fpart, 
      dbo.tvap.chaut, dbo.tvap.excop, dbo.tvap.svgng, dbo.tvap.typfd, dbo.tvap.ltypa, 
      dbo.tvap.ltypv, dbo.tvap.wkmat, dbo.tvap.rktio, dbo.tvap.segal, dbo.tvap.stdte, 
      dbo.tvap.rderl, dbo.tvap.effec, dbo.tvap.ffprf, dbo.tvap.scheme_rma, 
      dbo.tvap.scheme_vf, dbo.tvap.faktf, dbo.tvap.rrrel, dbo.tvap.acdatv, dbo.tvap.uveib, 
      dbo.tvap.revsp, dbo.tvpt.pstyo
FROM dbo.tvap INNER JOIN
      dbo.tvpt ON dbo.tvap.mandt = dbo.tvpt.mandt AND 
      dbo.tvap.pstyv = dbo.tvpt.pstyv INNER JOIN
      dbo.tvapt ON dbo.tvpt.mandt = dbo.tvapt.mandt AND dbo.tvpt.pstyv = dbo.tvapt.pstyv

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvas
AS
SELECT mandt, spras, aufsp, vtext
FROM dbo.tvast

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvau
AS
SELECT dbo.tvau.mandt, dbo.tvaut.spras, dbo.tvau.augru, dbo.tvaut.bezei
FROM dbo.tvau INNER JOIN
      dbo.tvaut ON dbo.tvau.mandt = dbo.tvaut.mandt AND dbo.tvau.augru = dbo.tvaut.augru

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvbur
AS
SELECT dbo.tvbur.mandt, dbo.tvbur.vkbur, dbo.tvkbt.spras, dbo.tvbur.ernam, 
      dbo.tvkbt.vkburx, dbo.tvbur.adrnr, dbo.tvbur.dlflg
FROM dbo.tvkbt INNER JOIN
      dbo.tvbur ON dbo.tvkbt.mandt = dbo.tvbur.mandt AND 
      dbo.tvkbt.vkbur = dbo.tvbur.vkbur

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpaak
AS
SELECT dbo.tvcpa.mandt, dbo.tvakt.spras, dbo.tvcpa.auarn, dbo.tvcpa.auarv, 
      dbo.tvcpa.fkarv, dbo.tvcpa.ettyv, dbo.tvcpa.pstyv, dbo.tvcpa.pstyn, dbo.tvcpa.grbed, 
      dbo.tvcpa.gruak, dbo.tvcpa.gruap, dbo.tvcpa.gruep, dbo.tvcpa.grukd, dbo.tvcpa.grupa, 
      dbo.tvcpa.posvo, dbo.tvcpa.ettyn, dbo.tvcpa.etkop, dbo.tvcpa.grpze, dbo.tvcpa.voref, 
      dbo.tvcpa.plmin, dbo.tvcpa.upflu, dbo.tvcpa.gruko, dbo.tvcpa.knprs, dbo.tvcpa.hineu, 
      dbo.tvcpa.gruve, dbo.tvcpa.chnew, dbo.tvcpa.pscop, dbo.tvcpa.stnew, 
      dbo.tvcpa.cpmen, dbo.tvcpa.psty2, dbo.tvcpa.knpr2, dbo.tvcpa.wkkop, 
      dbo.tvakt.auartx
FROM dbo.tvak INNER JOIN
      dbo.tvcpa ON dbo.tvak.mandt = dbo.tvcpa.mandt AND 
      dbo.tvak.auart = dbo.tvcpa.auarv INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND dbo.tvak.auart = dbo.tvakt.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpafk
AS
SELECT dbo.tvcpa.mandt, dbo.tvakt.spras, dbo.tvcpa.auarn, dbo.tvcpa.auarv, 
      dbo.tvcpa.fkarv, dbo.tvcpa.ettyv, dbo.tvcpa.pstyv, dbo.tvcpa.pstyn, dbo.tvcpa.grbed, 
      dbo.tvcpa.gruak, dbo.tvcpa.gruap, dbo.tvcpa.gruep, dbo.tvcpa.grukd, dbo.tvcpa.grupa, 
      dbo.tvcpa.posvo, dbo.tvcpa.ettyn, dbo.tvcpa.etkop, dbo.tvcpa.grpze, 
      dbo.tvcpa.spras AS Expr1, dbo.tvcpa.voref, dbo.tvcpa.plmin, dbo.tvcpa.upflu, 
      dbo.tvcpa.gruko, dbo.tvcpa.knprs, dbo.tvcpa.hineu, dbo.tvcpa.gruve, dbo.tvcpa.chnew, 
      dbo.tvcpa.pscop, dbo.tvcpa.stnew, dbo.tvcpa.cpmen, dbo.tvcpa.psty2, 
      dbo.tvcpa.knpr2, dbo.tvcpa.wkkop, dbo.tvakt.auartx, dbo.tvfkt.vtext
FROM dbo.tvcpa INNER JOIN
      dbo.tvak ON dbo.tvcpa.mandt = dbo.tvak.mandt AND 
      dbo.tvcpa.auarn = dbo.tvak.auart INNER JOIN
      dbo.tvfk ON dbo.tvcpa.mandt = dbo.tvfk.mandt AND 
      dbo.tvcpa.fkarv = dbo.tvfk.fkart INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvfkt ON dbo.tvakt.spras = dbo.tvfkt.spras AND 
      dbo.tvfk.mandt = dbo.tvfkt.mandt AND dbo.tvfk.fkart = dbo.tvfkt.fkart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpafp
AS
SELECT dbo.tvcpa.mandt, dbo.tvcpa.auarn, dbo.tvcpa.auarv, dbo.tvcpa.fkarv, 
      dbo.tvcpa.ettyv, dbo.tvcpa.pstyv, dbo.tvakt.spras, dbo.tvcpa.pstyn, dbo.tvcpa.grbed, 
      dbo.tvcpa.gruak, dbo.tvcpa.gruap, dbo.tvcpa.gruep, dbo.tvcpa.grukd, dbo.tvcpa.grupa, 
      dbo.tvcpa.posvo, dbo.tvcpa.ettyn, dbo.tvcpa.etkop, dbo.tvcpa.grpze, dbo.tvcpa.voref, 
      dbo.tvcpa.plmin, dbo.tvcpa.upflu, dbo.tvcpa.gruko, dbo.tvcpa.knprs, dbo.tvcpa.hineu, 
      dbo.tvcpa.gruve, dbo.tvcpa.chnew, dbo.tvcpa.psty2, dbo.tvcpa.knpr2, 
      dbo.tvakt.auartx AS vtexta, dbo.tvfkt.vtext AS vtextf, dbo.tvapt.vtext AS vtextp
FROM dbo.tvak INNER JOIN
      dbo.tvcpa ON dbo.tvak.mandt = dbo.tvcpa.mandt AND 
      dbo.tvak.auart = dbo.tvcpa.auarn INNER JOIN
      dbo.tvfk ON dbo.tvcpa.mandt = dbo.tvfk.mandt AND 
      dbo.tvcpa.fkarv = dbo.tvfk.fkart INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND 
      dbo.tvfk.fkart = dbo.tvfkt.fkart INNER JOIN
      dbo.tvpt ON dbo.tvcpa.mandt = dbo.tvpt.mandt AND 
      dbo.tvcpa.pstyv = dbo.tvpt.pstyv INNER JOIN
      dbo.tvapt ON dbo.tvpt.mandt = dbo.tvapt.mandt AND 
      dbo.tvpt.pstyv = dbo.tvapt.pstyv AND dbo.tvfkt.spras = dbo.tvapt.spras AND 
      dbo.tvakt.spras = dbo.tvapt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpfak
AS
SELECT dbo.tvcpf.mandt, dbo.tvcpf.fkarn, dbo.tvcpf.auarv, dbo.tvcpf.lfarv, dbo.tvcpf.fkarv, 
      dbo.tvakt.spras, dbo.tvcpf.pstyv, dbo.tvcpf.grbed, dbo.tvcpf.grurk, dbo.tvcpf.grurp, 
      dbo.tvcpf.gruko, dbo.tvcpf.knprs, dbo.tvcpf.plmin, dbo.tvcpf.fkmgk, dbo.tvcpf.posvo, 
      dbo.tvcpf.hineu, dbo.tvcpf.pfkur, dbo.tvcpf.expim, dbo.tvcpf.ordnr_fi, dbo.tvcpf.xblnr_fi, 
      dbo.tvcpf.prsqu, dbo.tvcpf.kvprs, dbo.tvcpf.pstyn, dbo.tvakt.auartx AS vtexta, 
      dbo.tvfkt.vtext AS vtextf
FROM dbo.tvak INNER JOIN
      dbo.tvcpf ON dbo.tvak.mandt = dbo.tvcpf.mandt AND 
      dbo.tvak.auart = dbo.tvcpf.auarv INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvfk ON dbo.tvcpf.mandt = dbo.tvfk.mandt AND 
      dbo.tvcpf.fkarn = dbo.tvfk.fkart INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND dbo.tvfk.fkart = dbo.tvfkt.fkart AND 
      dbo.tvakt.spras = dbo.tvfkt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpfap
AS
SELECT dbo.tvcpf.mandt, dbo.tvcpf.fkarn, dbo.tvcpf.auarv, dbo.tvcpf.lfarv, dbo.tvcpf.fkarv, 
      dbo.tvcpf.pstyv, dbo.tvakt.spras, dbo.tvcpf.grbed, dbo.tvcpf.grurk, dbo.tvcpf.grurp, 
      dbo.tvcpf.gruko, dbo.tvcpf.knprs, dbo.tvcpf.plmin, dbo.tvcpf.fkmgk, dbo.tvcpf.posvo, 
      dbo.tvcpf.hineu, dbo.tvcpf.pfkur, dbo.tvcpf.expim, dbo.tvcpf.ordnr_fi, dbo.tvcpf.xblnr_fi, 
      dbo.tvcpf.prsqu, dbo.tvcpf.kvprs, dbo.tvcpf.pstyn, dbo.tvakt.auartx AS vtexta, 
      dbo.tvfkt.vtext AS vtextf, dbo.tvapt.vtext AS vtextp
FROM dbo.tvapt INNER JOIN
      dbo.tvak INNER JOIN
      dbo.tvcpf ON dbo.tvak.mandt = dbo.tvcpf.mandt AND 
      dbo.tvak.auart = dbo.tvcpf.auarv INNER JOIN
      dbo.tvakt ON dbo.tvak.mandt = dbo.tvakt.mandt AND 
      dbo.tvak.auart = dbo.tvakt.auart INNER JOIN
      dbo.tvfk ON dbo.tvcpf.mandt = dbo.tvfk.mandt AND 
      dbo.tvcpf.fkarn = dbo.tvfk.fkart INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND 
      dbo.tvfk.fkart = dbo.tvfkt.fkart INNER JOIN
      dbo.tvpt ON dbo.tvcpf.mandt = dbo.tvpt.mandt AND 
      dbo.tvcpf.pstyv = dbo.tvpt.pstyv ON dbo.tvapt.mandt = dbo.tvpt.mandt AND 
      dbo.tvapt.pstyv = dbo.tvpt.pstyv AND dbo.tvapt.spras = dbo.tvakt.spras AND 
      dbo.tvapt.spras = dbo.tvfkt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.v_tvcpflk
AS
SELECT dbo.tvcpf.mandt, dbo.tvfkt.spras, dbo.tvcpf.fkarn, dbo.tvcpf.auarv, dbo.tvcpf.lfarv, 
      dbo.tvcpf.fkarv, dbo.tvcpf.pstyv, dbo.tvcpf.grbed, dbo.tvcpf.grurk, dbo.tvcpf.grurp, 
      dbo.tvcpf.gruko, dbo.tvcpf.knprs, dbo.tvcpf.plmin, dbo.tvcpf.fkmgk, dbo.tvcpf.posvo, 
      dbo.tvcpf.hineu, dbo.tvcpf.pfkur, dbo.tvcpf.expim, dbo.tvcpf.ordnr_fi, dbo.tvcpf.xblnr_fi, 
      dbo.tvcpf.prsqu, dbo.tvcpf.kvprs, dbo.tvcpf.pstyn, dbo.tvfkt.vtext AS fkartx, 
      dbo.tvlkt.vtext AS lfartx
FROM dbo.tvfkt INNER JOIN
      dbo.tvfk ON dbo.tvfkt.mandt = dbo.tvfk.mandt AND 
      dbo.tvfkt.fkart = dbo.tvfk.fkart INNER JOIN
      dbo.tvcpf INNER JOIN
      dbo.tvlk ON dbo.tvcpf.mandt = dbo.tvlk.mandt AND 
      dbo.tvcpf.lfarv = dbo.tvlk.lfart INNER JOIN
      dbo.tvlkt ON dbo.tvlk.mandt = dbo.tvlkt.mandt AND dbo.tvlk.lfart = dbo.tvlkt.lfart ON 
      dbo.tvfkt.spras = dbo.tvlkt.spras AND dbo.tvfk.mandt = dbo.tvcpf.mandt AND 
      dbo.tvfk.fkart = dbo.tvcpf.fkarn


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvcpflp
AS
SELECT dbo.tvcpf.mandt, dbo.tvcpf.fkarn, dbo.tvcpf.auarv, dbo.tvcpf.lfarv, dbo.tvcpf.fkarv, 
      dbo.tvcpf.pstyv, dbo.tvlkt.spras, dbo.tvcpf.grbed, dbo.tvcpf.grurk, dbo.tvcpf.grurp, 
      dbo.tvcpf.gruko, dbo.tvcpf.knprs, dbo.tvcpf.plmin, dbo.tvcpf.fkmgk, dbo.tvcpf.posvo, 
      dbo.tvcpf.hineu, dbo.tvcpf.pfkur, dbo.tvcpf.expim, dbo.tvcpf.ordnr_fi, dbo.tvcpf.xblnr_fi, 
      dbo.tvlkt.vtext AS vtextl, dbo.tvfkt.vtext AS vtextf, dbo.tvapt.vtext AS vtextp, 
      dbo.tvcpf.kvprs, dbo.tvcpf.prsqu, dbo.tvcpf.pstyn
FROM dbo.tvcpf INNER JOIN
      dbo.tvlk ON dbo.tvcpf.mandt = dbo.tvlk.mandt AND 
      dbo.tvcpf.lfarv = dbo.tvlk.lfart INNER JOIN
      dbo.tvlkt ON dbo.tvlk.mandt = dbo.tvlkt.mandt AND 
      dbo.tvlk.lfart = dbo.tvlkt.lfart INNER JOIN
      dbo.tvfk ON dbo.tvcpf.mandt = dbo.tvfk.mandt AND 
      dbo.tvcpf.fkarn = dbo.tvfk.fkart INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND dbo.tvfk.fkart = dbo.tvfkt.fkart AND 
      dbo.tvlkt.spras = dbo.tvfkt.spras INNER JOIN
      dbo.tvpt ON dbo.tvcpf.mandt = dbo.tvpt.mandt AND 
      dbo.tvcpf.pstyv = dbo.tvpt.pstyv INNER JOIN
      dbo.tvapt ON dbo.tvpt.mandt = dbo.tvapt.mandt AND 
      dbo.tvpt.pstyv = dbo.tvapt.pstyv AND dbo.tvlkt.spras = dbo.tvapt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvep
AS
SELECT dbo.tvep.mandt, dbo.tvept.ettyp, dbo.tvept.spras, dbo.tvept.vtext, dbo.tvep.aswet, 
      dbo.tvep.lfrel, dbo.tvep.bdart, dbo.tvep.plart, dbo.tvep.entlg, dbo.tvep.ebanf, 
      dbo.tvep.bsart, dbo.tvep.bstyp, dbo.tvep.bwart, dbo.tvep.aubst, dbo.tvep.lifsp, 
      dbo.tvep.txn08, dbo.tvep.ernam, dbo.tvep.fehgr, dbo.tvep.pstyp, dbo.tvep.knttp, 
      dbo.tvep.atppr, dbo.tvep.bedsd, dbo.tvep.bwa1s, dbo.tvep.quota
FROM dbo.tvep INNER JOIN
      dbo.tvept ON dbo.tvep.mandt = dbo.tvept.mandt AND dbo.tvep.ettyp = dbo.tvept.ettyp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvepz
AS
SELECT mandt, pstyv, dismm, ettyp, etty1, etty2, etty3, etty4, etty5, etty6, etty7, etty8, 
      etty9, ernam, bedae, bedhk
FROM dbo.tvepz

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvet
AS
SELECT dbo.tvet.mandt, dbo.tvet.etart, dbo.tvett.spras, dbo.tvett.bezei
FROM dbo.tvet INNER JOIN
      dbo.tvett ON dbo.tvet.mandt = dbo.tvett.mandt AND dbo.tvet.etart = dbo.tvett.etart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvfk
AS
SELECT dbo.tvfk.mandt, dbo.tvfk.fkart, dbo.tvfkt.spras, dbo.tvfkt.vtext, dbo.tvfk.kopgr, 
      dbo.tvfk.numki, dbo.tvfk.numke, dbo.tvfk.incpo, dbo.tvfk.uevor, dbo.tvfk.kunn0, 
      dbo.tvfk.umfng, dbo.tvfk.rfbfk, dbo.tvfk.trvog, dbo.tvfk.txn08, dbo.tvfk.vbtyp, 
      dbo.tvfk.ernam, dbo.tvfk.pargk, dbo.tvfk.pargp, dbo.tvfk.kalsmc, dbo.tvfk.txtgr, 
      dbo.tvfk.kappl, dbo.tvfk.fkarts, dbo.tvfk.kalsm, dbo.tvfk.kschl, dbo.tvfk.stafo, 
      dbo.tvfk.kvslv, dbo.tvfk.kalvg, dbo.tvfk.txtgr_p, dbo.tvfk.stati, dbo.tvfk.kalsmbp, 
      dbo.tvfk.borvf, dbo.tvfk.fkart_rl, dbo.tvfk.relep, dbo.tvfk.txtlf, dbo.tvfk.hityp_pr, 
      dbo.tvfk.fkart_ab, dbo.tvfk.grbed_s, dbo.tvfk.ordnr_fi_s, dbo.tvfk.xblnr_fi_s, 
      dbo.tvfk.j_1bnfrel, dbo.tvfk.j_1bmainpa, dbo.tvfk.j_1btdidh, dbo.tvfk.j_1btdcoh, 
      dbo.tvfk.j_1btdidl, dbo.tvfk.j_1btdcol, dbo.tvfk.fktyp, dbo.tvfk.xnegp, dbo.tvfk.blart, 
      dbo.tvfk.xfilkd, dbo.tvfk.xvalgs, dbo.tvfk.kalsmca, dbo.tvfk.kalsmcb, dbo.tvfk.kalsmcc, 
      dbo.tvfk.kalsmcd, dbo.tvfk.xkoiv, dbo.tvfk.j_1ainvref, dbo.tvfk.j_1acpdel
FROM dbo.tvfk INNER JOIN
      dbo.tvfkt ON dbo.tvfk.mandt = dbo.tvfkt.mandt AND dbo.tvfk.fkart = dbo.tvfkt.fkart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvfs
AS
SELECT dbo.tvfs.mandt, dbo.tvfs.faksp, dbo.tvfst.spras, dbo.tvfst.vtext
FROM dbo.tvfs INNER JOIN
      dbo.tvfst ON dbo.tvfs.mandt = dbo.tvfst.mandt AND dbo.tvfs.faksp = dbo.tvfst.faksp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkm
AS
SELECT dbo.tvkm.mandt, dbo.tvkm.ktgrm, dbo.tvkmt.spras, dbo.tvkmt.vtext
FROM dbo.tvkm INNER JOIN
      dbo.tvkmt ON dbo.tvkm.mandt = dbo.tvkmt.mandt AND 
      dbo.tvkm.ktgrm = dbo.tvkmt.ktgrm

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvko
AS
SELECT dbo.tvko.mandt, dbo.tvko.vkorg, dbo.tvko.waers, dbo.tvko.adrnr, 
      dbo.tvko.txnam_adr, dbo.tvko.txnam_kop, dbo.tvko.txnam_fus, dbo.tvko.txnam_gru, 
      dbo.tvko.vkoau, dbo.tvko.kunnr, dbo.tvko.boavo, dbo.tvko.vkokl, dbo.tvko.ekorg, 
      dbo.tvko.ekgrp, dbo.tvko.lifnr, dbo.tvko.werks, dbo.tvko.bsart, dbo.tvko.bstyp, 
      dbo.tvko.bwart, dbo.tvko.lgort, dbo.tvko.txnam_sdb, dbo.tvko.dlflg, dbo.tvkot.spras, 
      dbo.tvko.bukrs, dbo.tvko.asflg, dbo.tvkot.vkorgx
FROM dbo.tvko INNER JOIN
      dbo.tvkot ON dbo.tvko.mandt = dbo.tvkot.mandt AND dbo.tvko.vkorg = dbo.tvkot.vkorg

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkswz
AS
SELECT dbo.tvkwz.mandt, dbo.tvkwz.vkorg, dbo.tvkwz.vtweg, dbo.tvkwz.werks, 
      dbo.tvswz.vstel
FROM dbo.tvkwz INNER JOIN
      dbo.tvswz ON dbo.tvkwz.mandt = dbo.tvswz.mandt AND 
      dbo.tvkwz.werks = dbo.tvswz.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkt
AS
SELECT dbo.tvkt.mandt, dbo.tvkt.ktgrd, dbo.tvktt.spras, dbo.tvktt.vtext
FROM dbo.tvkt INNER JOIN
      dbo.tvktt ON dbo.tvkt.mandt = dbo.tvktt.mandt AND dbo.tvkt.ktgrd = dbo.tvktt.ktgrd

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkwz
AS
SELECT dbo.tvkwz.mandt, dbo.tvkwz.vkorg, dbo.tvkwz.vtweg, dbo.tvkwz.werks, 
      dbo.v_t001w1.name1, dbo.v_t001w1.name2, dbo.v_t001w1.dlflg
FROM dbo.tvkwz INNER JOIN
      dbo.v_t001w1 ON dbo.tvkwz.mandt = dbo.v_t001w1.mandt AND 
      dbo.tvkwz.werks = dbo.v_t001w1.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvlk
AS
SELECT dbo.tvlk.mandt, dbo.tvlk.lfart, dbo.tvlkt.spras, dbo.tvlkt.vtext, dbo.tvlk.kopgr, 
      dbo.tvlk.uevor, dbo.tvlk.txn08, dbo.tvlk.umfng, dbo.tvlk.numki, dbo.tvlk.numke, 
      dbo.tvlk.incpo, dbo.tvlk.aufer, dbo.tvlk.daart, dbo.tvlk.pobed, dbo.tvlk.vbtyp, 
      dbo.tvlk.pargr, dbo.tvlk.reglg, dbo.tvlk.routf, dbo.tvlk.txtgr, dbo.tvlk.kappl, 
      dbo.tvlk.kalsm, dbo.tvlk.kschl, dbo.tvlk.stgak, dbo.tvlk.trspg, dbo.tvlk.tdiix, 
      dbo.tvlk.excok, dbo.tvlk.neute, dbo.tvlk.fehgr, dbo.tvlk.lnspl, dbo.tvlk.averp, 
      dbo.tvlk.cmgrl, dbo.tvlk.cmgrk, dbo.tvlk.cmgrw, dbo.tvlk.qherk, dbo.tvlk.profidnetz, 
      dbo.tvlk.kalsp, dbo.tvlk.regtb, dbo.tvlk.bzops, dbo.tvlk.excbc, dbo.tvlk.excem, 
      dbo.tvlk.exclg, dbo.tvlk.j_1adoccls, dbo.tvlk.rfpl_sw, dbo.tvlk.tsegtp, dbo.tvlk.dbtch, 
      dbo.tvlk.dsfad, dbo.tvlk.blart
FROM dbo.tvlk INNER JOIN
      dbo.tvlkt ON dbo.tvlk.mandt = dbo.tvlkt.mandt AND dbo.tvlk.lfart = dbo.tvlkt.lfart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvlp
AS
SELECT dbo.tvlp.mandt, dbo.tvlp.pstyv, dbo.tvapt.spras, dbo.tvlp.matno, dbo.tvlp.spemt, 
      dbo.tvlp.matpr, dbo.tvlp.mngop, dbo.tvlp.minmg, dbo.tvlp.uebpr, dbo.tvlp.verpr, 
      dbo.tvlp.komrl, dbo.tvlp.lgozw, dbo.tvlp.belgo, dbo.tvlp.posgr, dbo.tvlp.feldp, 
      dbo.tvlp.bildp, dbo.tvlp.txn08, dbo.tvlp.pargr, dbo.tvlp.aswpl, dbo.tvlp.txtgr, 
      dbo.tvlp.ernam, dbo.tvlp.stgap, dbo.tvlp.chpkz, dbo.tvlp.kalsm, dbo.tvlp.pckpf, 
      dbo.tvlp.chaut, dbo.tvlp.excop, dbo.tvlp.tprel, dbo.tvlp.chhpv, dbo.tvlp.rndsg, 
      dbo.tvlp.fehgr, dbo.tvlp.segal, dbo.tvlp.rules, dbo.tvlp.bwart, dbo.tvlp.vbtyp, 
      dbo.tvlp.uveib, dbo.tvlp.podkz, dbo.tvlp.lgokz, dbo.tvlp.bacre, dbo.tvlp.podau, 
      dbo.tvapt.vtext
FROM dbo.tvlp INNER JOIN
      dbo.tvpt ON dbo.tvlp.mandt = dbo.tvpt.mandt AND 
      dbo.tvlp.pstyv = dbo.tvpt.pstyv INNER JOIN
      dbo.tvapt ON dbo.tvpt.mandt = dbo.tvapt.mandt AND dbo.tvpt.pstyv = dbo.tvapt.pstyv

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvlp_bac
AS
SELECT dbo.tvpt.mandt, dbo.tvpt.pstyv, dbo.tvapt.spras, dbo.tvapt.vtext, 
      dbo.tvlp.bacre
FROM dbo.tvlp INNER JOIN
      dbo.tvpt ON dbo.tvlp.mandt = dbo.tvpt.mandt AND 
      dbo.tvlp.pstyv = dbo.tvpt.pstyv INNER JOIN
      dbo.tvapt ON dbo.tvpt.mandt = dbo.tvapt.mandt AND dbo.tvpt.pstyv = dbo.tvapt.pstyv

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvls
AS
SELECT dbo.tvls.mandt, dbo.tvlst.spras, dbo.tvls.lifsp, dbo.tvlst.vtext, dbo.tvls.spelf, 
      dbo.tvls.speko, dbo.tvls.spewa, dbo.tvls.speft, dbo.tvls.spebe, dbo.tvls.speau, 
      dbo.tvls.spedr, dbo.tvls.spevi, dbo.tvls.mbdif
FROM dbo.tvls INNER JOIN
      dbo.tvlst ON dbo.tvls.mandt = dbo.tvlst.mandt AND dbo.tvls.lifsp = dbo.tvlst.lifsp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvst
AS
SELECT dbo.tvst.mandt, dbo.tvstt.spras, dbo.tvst.vstel, dbo.tvstt.vtext, dbo.tvst.fabkl, 
      dbo.tvst.vtrzt, dbo.tvst.adrnr, dbo.tvst.aland, dbo.tvst.azone, dbo.tvst.txnam_adr, 
      dbo.tvst.txnam_kop, dbo.tvst.txnam_fus, dbo.tvst.txnam_gru, dbo.tvst.kschl, 
      dbo.tvst.sprasl, dbo.tvst.anzal, dbo.tvst.vstzp, dbo.tvst.nacha, dbo.tvst.lazbs, 
      dbo.tvst.rizbs, dbo.tvst.lazzt, dbo.tvst.rizzt, dbo.tvst.koqui, dbo.tvst.komsu, 
      dbo.tvst.imess, dbo.tvst.txnam_sdb, dbo.tvst.alw_sw, dbo.tvst.loadtg, dbo.tvst.loadtn, 
      dbo.tvst.pipatg, dbo.tvst.pipatn, dbo.tvst.tstrid, dbo.tvst.roundg, dbo.tvst.roundn
FROM dbo.tvst INNER JOIN
      dbo.tvstt ON dbo.tvst.mandt = dbo.tvstt.mandt AND dbo.tvst.vstel = dbo.tvstt.vstel

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvswz
AS
SELECT dbo.tvswz.mandt, dbo.tvswz.werks, dbo.tvswz.vstel, dbo.tvstt.spras, 
      dbo.tvstt.vtext
FROM dbo.tvswz INNER JOIN
      dbo.tvst ON dbo.tvswz.mandt = dbo.tvst.mandt AND 
      dbo.tvswz.vstel = dbo.tvst.vstel INNER JOIN
      dbo.tvstt ON dbo.tvst.mandt = dbo.tvstt.mandt AND dbo.tvst.vstel = dbo.tvstt.vstel

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvtw
AS
SELECT dbo.tvtw.mandt, dbo.tvtw.vtweg, dbo.tvtwt.spras, dbo.tvtwt.vtwegx, 
      dbo.tvtw.dlflg
FROM dbo.tvtw INNER JOIN
      dbo.tvtwt ON dbo.tvtw.mandt = dbo.tvtwt.mandt AND dbo.tvtw.vtweg = dbo.tvtwt.vtweg

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvvw
AS
SELECT dbo.tvvw.mandt, dbo.tvvw.vwpos, dbo.tvvwt.spras, dbo.tvvwt.bezei
FROM dbo.tvvwt INNER JOIN
      dbo.tvvw ON dbo.tvvwt.mandt = dbo.tvvw.mandt AND 
      dbo.tvvwt.vwpos = dbo.tvvw.vwpos

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tw80
AS
SELECT mandt, qmsta, statx, ifasn, ifack, ifhdn, ifacu, ifcan, ifedt, ifbl1, ifbl2, ifbl3, ifbl4, 
      ifbl5, stlvm
FROM dbo.tw80

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usr12
AS
SELECT dbo.usr12.mandt, dbo.usr12.objct, dbo.usr12.auth, dbo.usr12.aktps, 
      dbo.usr12t.spras, dbo.usr12t.ltext, dbo.usr12.modda, dbo.usr12.modti, 
      dbo.usr12.modbe, dbo.usr12.typ, dbo.usr12.lng, dbo.usr12.vals
FROM dbo.usr12 INNER JOIN
      dbo.usr12t ON dbo.usr12.mandt = dbo.usr12t.mandt AND 
      dbo.usr12.objct = dbo.usr12t.objct AND dbo.usr12.auth = dbo.usr12t.auth AND 
      dbo.usr12.aktps = dbo.usr12t.aktps

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usr12_c
AS
SELECT dbo.usr12.mandt, dbo.usr12.objct, dbo.usr12.auth, dbo.usr12.aktps, 
      dbo.usr12t.spras, dbo.usr12t.ltext, dbo.tobjt.ttext, dbo.tobct.ctext, dbo.tobc.oclss, 
      dbo.usr12.modda, dbo.usr12.modti, dbo.usr12.modbe, dbo.usr12.typ, dbo.usr12.lng, 
      dbo.usr12.vals
FROM dbo.tobct INNER JOIN
      dbo.usr12 INNER JOIN
      dbo.usr12t ON dbo.usr12.mandt = dbo.usr12t.mandt AND 
      dbo.usr12.objct = dbo.usr12t.objct AND dbo.usr12.auth = dbo.usr12t.auth AND 
      dbo.usr12.aktps = dbo.usr12t.aktps INNER JOIN
      dbo.tobj ON dbo.usr12.objct = dbo.tobj.objct INNER JOIN
      dbo.tobjt ON dbo.tobj.objct = dbo.tobjt.object AND 
      dbo.usr12t.spras = dbo.tobjt.langu INNER JOIN
      dbo.tobc ON dbo.tobj.oclss = dbo.tobc.oclss ON dbo.tobct.oclss = dbo.tobc.oclss AND 
      dbo.tobct.langu = dbo.tobjt.langu

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_usr_aut
AS
SELECT dbo.usrrol.mandt, dbo.usrrol.bname, dbo.rlaut.objct, dbo.rlaut.auth, dbo.rlaut.aktps, 
      dbo.rlaut.rolnr, dbo.ust12.field, dbo.ust12.von, dbo.ust12.bis
FROM dbo.rlaut INNER JOIN
      dbo.usrrol ON dbo.rlaut.mandt = dbo.usrrol.mandt AND 
      dbo.rlaut.rolnr = dbo.usrrol.rolnr INNER JOIN
      dbo.ust12 ON dbo.rlaut.mandt = dbo.ust12.mandt AND 
      dbo.rlaut.objct = dbo.ust12.objct AND dbo.rlaut.auth = dbo.ust12.auth AND 
      dbo.rlaut.aktps = dbo.ust12.aktps

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbak
AS
SELECT dbo.vbak.mandt, dbo.vbak.vbeln, dbo.vbak.auart, dbo.vbak.vkorg, dbo.vbak.vtweg, 
      dbo.vbak.spart, dbo.vbak.vkbur, dbo.vbak.vkgrp, dbo.vbak.kunnr, dbo.vbak.waerk, 
      dbo.vbak.vdatu, dbo.vbak.bname, dbo.vbak.telf1, dbo.vbak.erzet, dbo.vbak.erdat, 
      dbo.vbak.ernam, dbo.vbak.aedat, dbo.tvakt.spras, dbo.tvakt.auartx
FROM dbo.vbak INNER JOIN
      dbo.tvakt ON dbo.vbak.mandt = dbo.tvakt.mandt AND dbo.vbak.auart = dbo.tvakt.auart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbak_a
AS
SELECT dbo.vbak.mandt, dbo.tvakt.spras, dbo.vbak.vbeln, dbo.tvakt.auartx, 
      dbo.tvkot.vkorgx, dbo.tvtwt.vtwegx, dbo.tvgrt.vkgrpx, dbo.vbak.vkgrp2, 
      dbo.tvkbt.vkburx, dbo.tspat.spartx, dbo.vbak.kunnr, dbo.vbak.waerk, dbo.vbak.vdatu, 
      dbo.vbak.ktext, dbo.vbak.auart, dbo.vbak.vkorg, dbo.vbak.vtweg, dbo.vbak.vkgrp, 
      dbo.vbak.vkbur, dbo.vbak.spart, dbo.vbak.erzet, dbo.vbak.erdat, dbo.vbak.ernam, 
      dbo.vbak.aedat
FROM dbo.tvakt INNER JOIN
      dbo.vbak ON dbo.tvakt.mandt = dbo.vbak.mandt AND 
      dbo.tvakt.auart = dbo.vbak.auart INNER JOIN
      dbo.tvkot ON dbo.vbak.mandt = dbo.tvkot.mandt AND 
      dbo.vbak.vkorg = dbo.tvkot.vkorg AND dbo.tvakt.spras = dbo.tvkot.spras INNER JOIN
      dbo.tvtwt ON dbo.vbak.mandt = dbo.tvtwt.mandt AND 
      dbo.vbak.vtweg = dbo.tvtwt.vtweg AND dbo.tvakt.spras = dbo.tvtwt.spras INNER JOIN
      dbo.tvgrt ON dbo.vbak.mandt = dbo.tvgrt.mandt AND 
      dbo.vbak.vkgrp = dbo.tvgrt.vkgrp AND dbo.tvakt.spras = dbo.tvgrt.spras INNER JOIN
      dbo.tvkbt ON dbo.vbak.mandt = dbo.tvkbt.mandt AND 
      dbo.vbak.vkbur = dbo.tvkbt.vkbur AND dbo.tvakt.spras = dbo.tvkbt.spras INNER JOIN
      dbo.tspat ON dbo.vbak.mandt = dbo.tspat.mandt AND 
      dbo.vbak.spart = dbo.tspat.spart AND dbo.tvakt.spras = dbo.tspat.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vbap_a
AS
SELECT dbo.vbap.mandt, dbo.vbap.vbeln, dbo.vbap.posnr, dbo.makt.maktg, 
      dbo.vbap.netpr, dbo.vbap.waerk, dbo.vbap.smeng, dbo.t006a.mseht, dbo.vbap.matkl, 
      dbo.vbap.spart, dbo.vbap.vkgrp, dbo.vbap.vkgrp2, dbo.vbap.werks, dbo.vbap.lgort, 
      dbo.vbap.matnr, dbo.vbap.erdat, dbo.vbap.ernam, dbo.makt.spras
FROM dbo.vbap INNER JOIN
      dbo.makt ON dbo.vbap.mandt = dbo.makt.mandt AND 
      dbo.vbap.matnr = dbo.makt.matnr INNER JOIN
      dbo.t006a ON dbo.vbap.mandt = dbo.t006a.mandt AND 
      dbo.vbap.vrkme = dbo.t006a.msehi AND dbo.makt.spras = dbo.t006a.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_vertc
AS
SELECT dbo.tstct.spras, dbo.vertc.version, dbo.vertc.tcode, dbo.tstct.ttext
FROM dbo.tstc INNER JOIN
      dbo.vertc ON dbo.tstc.tcode = dbo.vertc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w001
AS
SELECT dbo.w001.mandt, dbo.w001.gugnr, dbo.w001t.spras, dbo.w001t.vtext
FROM dbo.w001 INNER JOIN
      dbo.w001t ON dbo.w001.mandt = dbo.w001t.mandt AND 
      dbo.w001.gugnr = dbo.w001t.gugnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w002
AS
SELECT dbo.w002.mandt, dbo.w002.clrnr, dbo.w002t.spras, dbo.w002t.vtext
FROM dbo.w002 INNER JOIN
      dbo.w002t ON dbo.w002.mandt = dbo.w002t.mandt AND 
      dbo.w002.clrnr = dbo.w002t.clrnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w100_1
AS
SELECT dbo.w100.mandt, dbo.w100.lifnr, dbo.w100.mtart, dbo.t134t.spras, 
      dbo.t134t.mtbez
FROM dbo.w100 INNER JOIN
      dbo.t134 ON dbo.w100.mandt = dbo.t134.mandt AND 
      dbo.w100.mtart = dbo.t134.mtart INNER JOIN
      dbo.t134t ON dbo.t134.mandt = dbo.t134t.mandt AND dbo.t134.mtart = dbo.t134t.mtart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w101_a
AS
SELECT dbo.w101.mandt, dbo.w101.lifnr, dbo.w101.mtart, dbo.w101.gugnr, 
      dbo.w001t.spras, dbo.w001t.vtext
FROM dbo.w101 INNER JOIN
      dbo.w001 ON dbo.w101.mandt = dbo.w001.mandt AND 
      dbo.w101.gugnr = dbo.w001.gugnr INNER JOIN
      dbo.w001t ON dbo.w001.mandt = dbo.w001t.mandt AND 
      dbo.w001.gugnr = dbo.w001t.gugnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w102_a
AS
SELECT dbo.w102.mandt, dbo.w102.lifnr, dbo.w102.mtart, dbo.w102.clrnr, dbo.w002t.spras, 
      dbo.w002t.vtext
FROM dbo.w102 INNER JOIN
      dbo.w002 ON dbo.w102.mandt = dbo.w002.mandt AND 
      dbo.w102.clrnr = dbo.w002.clrnr INNER JOIN
      dbo.w002t ON dbo.w002.mandt = dbo.w002t.mandt AND 
      dbo.w002.clrnr = dbo.w002t.clrnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w156c
AS
SELECT dbo.w156c.mandt, dbo.tstct.spras, dbo.w156c.bwart, dbo.w156c.tcode, 
      dbo.tstct.ttext
FROM dbo.w156c INNER JOIN
      dbo.tstc ON dbo.w156c.tcode = dbo.tstc.tcode INNER JOIN
      dbo.tstct ON dbo.tstc.tcode = dbo.tstct.tcode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w156f
AS
SELECT dbo.w156f.mandt, dbo.w156pt.spras, dbo.w156f.bwart, dbo.w156f.sobkz, 
      dbo.w156f.tcode, dbo.w156f.param, dbo.w156pt.ltext, dbo.w156f.hide
FROM dbo.w156f INNER JOIN
      dbo.w156pt ON dbo.w156f.param = dbo.w156pt.param

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_w156s
AS
SELECT dbo.w156s.mandt, dbo.w156pt.spras, dbo.w156s.bwart, dbo.w156pt.param, 
      dbo.w156pt.ltext
FROM dbo.w156pt INNER JOIN
      dbo.w156s ON dbo.w156pt.param = dbo.w156s.param

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wauba_s
AS
SELECT dbo.wauba.mandt, dbo.skat.spras, dbo.wauba.bukrs, dbo.wauba.qmart, 
      dbo.wauba.aprof, dbo.wauba.makat, dbo.wauba.magrp, dbo.wauba.macod, 
      dbo.wauba.ktopl, dbo.wauba.saknr, dbo.skat.txt20
FROM dbo.wauba INNER JOIN
      dbo.skat ON dbo.wauba.mandt = dbo.skat.mandt AND 
      dbo.wauba.ktopl = dbo.skat.ktopl AND dbo.wauba.saknr = dbo.skat.saknr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_waufk
AS
SELECT dbo.waufk.mandt, dbo.t003p.spras, dbo.waufk.aufnr, dbo.waufk.auart, dbo.t003p.txt, 
      dbo.waufk.autyp, dbo.waufk.refnr, dbo.waufk.ernam, dbo.waufk.erdat, 
      dbo.waufk.aenam, dbo.waufk.aedat, dbo.waufk.ktext, dbo.waufk.qmart, 
      dbo.waufk.qmnum, dbo.waufk.bukrs, dbo.waufk.werks, dbo.waufk.kostv, 
      dbo.waufk.waers, dbo.waufk.srvag, dbo.waufk.argnm, dbo.waufk.astnr, 
      dbo.waufk.stdat, dbo.waufk.loekz, dbo.waufk.ewerks, dbo.waufk.etplnr, 
      dbo.waufk.dtplnr, dbo.waufk.veran, dbo.waufk.aprof, dbo.qpgt.kurztext AS urgrpx, 
      dbo.qpct.kurztextc AS urcodx, qpgt_1.kurztext AS magrpx, 
      qpct_1.kurztextc AS macodx, dbo.waufk.urkat, dbo.waufk.urgrp, dbo.waufk.urcod, 
      dbo.waufk.makat, dbo.waufk.magrp, dbo.waufk.macod
FROM dbo.waufk INNER JOIN
      dbo.t003p ON dbo.waufk.mandt = dbo.t003p.mandt AND 
      dbo.waufk.auart = dbo.t003p.auart INNER JOIN
      dbo.qpgt ON dbo.waufk.mandt = dbo.qpgt.mandt AND 
      dbo.waufk.urkat = dbo.qpgt.katalogart AND 
      dbo.waufk.urgrp = dbo.qpgt.codegruppe AND 
      dbo.t003p.spras = dbo.qpgt.spras INNER JOIN
      dbo.qpct ON dbo.waufk.mandt = dbo.qpct.mandt AND 
      dbo.waufk.urkat = dbo.qpct.katalogart AND 
      dbo.waufk.urgrp = dbo.qpct.codegruppe AND dbo.waufk.urcod = dbo.qpct.code AND 
      dbo.t003p.spras = dbo.qpct.spras INNER JOIN
      dbo.qpgt qpgt_1 ON dbo.waufk.mandt = qpgt_1.mandt AND 
      dbo.waufk.makat = qpgt_1.katalogart AND dbo.waufk.magrp = qpgt_1.codegruppe AND 
      dbo.t003p.spras = qpgt_1.spras INNER JOIN
      dbo.qpct qpct_1 ON dbo.waufk.mandt = qpct_1.mandt AND 
      dbo.t003p.spras = qpct_1.spras AND dbo.waufk.makat = qpct_1.katalogart AND 
      dbo.waufk.magrp = qpct_1.codegruppe AND dbo.waufk.macod = qpct_1.code

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wiflot
AS
SELECT dbo.wiflot.mandt, dbo.wiflot.tplnr, dbo.wiflot.tplkz, dbo.wiflot.fltyp, dbo.wiflot.tplma, 
      dbo.wiflot.lgwid, dbo.wiflot.swerk, dbo.wiflot.bukrs, dbo.wiflot.kostl, dbo.wiflot.land1, 
      dbo.wiflot.regio, dbo.wiflot.pstlz, dbo.wiflot.ort01, dbo.wiflot.stras, dbo.wiflot.psncg, 
      dbo.wiflot.telf1, dbo.wiflot.telf2, dbo.wiflot.telfx, dbo.wiflot.telbx, dbo.wiflot.erdat, 
      dbo.wiflot.ernam, dbo.wiflot.aedat, dbo.wiflot.aenam, dbo.wiflot.lvorm, 
      dbo.wiflotx.spras, dbo.wiflotx.pltxt, dbo.wiflotx.kzltx, dbo.wiflotx.kzmla, 
      dbo.wiflotx.pltxu
FROM dbo.wiflot INNER JOIN
      dbo.wiflotx ON dbo.wiflot.mandt = dbo.wiflotx.mandt AND 
      dbo.wiflot.tplnr = dbo.wiflotx.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wiflot_a
AS
SELECT dbo.wiflot.mandt, dbo.wiflot.tplnr, dbo.wiflot.swerk, dbo.wiflot.bukrs, 
      dbo.wiflot.kostl, dbo.wiflot.land1, dbo.wiflot.regio, dbo.wiflot.tplkz, dbo.wiflot.fltyp, 
      dbo.wiflot.pstlz, dbo.wiflot.tplma, dbo.wiflotx.spras, dbo.wiflotx.pltxt
FROM dbo.wiflot INNER JOIN
      dbo.wiflotx ON dbo.wiflot.mandt = dbo.wiflotx.mandt AND 
      dbo.wiflot.tplnr = dbo.wiflotx.tplnr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wiflot_ver
AS
SELECT dbo.wiflot.mandt, dbo.wiflot.tplnr, dbo.tc24.veran, dbo.tc24.ktext, dbo.tc24.isasn, 
      dbo.tc24.werks
FROM dbo.wiflot INNER JOIN
      dbo.tc24 ON dbo.wiflot.mandt = dbo.tc24.mandt AND dbo.wiflot.swerk = dbo.tc24.werks

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmak_a
AS
SELECT dbo.wqmak.mandt, dbo.cq16t.spras, dbo.wqmak.qmnum, dbo.wqmak.ackno, 
      dbo.wqmak.akdat, dbo.wqmak.aktim, dbo.wqmak.aknam, dbo.cq16t.kalafx, 
      dbo.wqmak.svdat, dbo.wqmak.svtim, dbo.wqmak.rmktx
FROM dbo.wqmak INNER JOIN
      dbo.cq16t ON dbo.wqmak.mandt = dbo.cq16t.mandt AND 
      dbo.wqmak.akkat = dbo.cq16t.katat AND dbo.wqmak.akkal = dbo.cq16t.kalaf

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel
AS
SELECT dbo.wqmel.mandt, dbo.tq80_t.spras, dbo.wqmel.qmnum, dbo.wqmel.qmart, 
      dbo.wqmel.qmsta, dbo.wqmel.ernam, dbo.wqmel.erdat, dbo.wqmel.ertim, 
      dbo.wqmel.ewerks, dbo.wqmel.etplnr, dbo.wqmel.ausvn, dbo.wqmel.auztv, 
      dbo.wqmel.strmn, dbo.wqmel.strur, dbo.wqmel.ltrmn, dbo.wqmel.ltrur, 
      dbo.wqmel.ausbs, dbo.wqmel.auztb, dbo.wqmel.mzeit, dbo.wqmel.qmdat, 
      dbo.wqmel.srvcode, dbo.wqmel.serialnr, dbo.wqmel.linkman, dbo.wqmel.ltel1, 
      dbo.wqmel.ltel2, dbo.wqmel.laddr, dbo.wqmel.qmkat, dbo.wqmel.qmgrp, 
      dbo.wqmel.qmcod, dbo.wqmel.kunum, dbo.wqmel.qmtxt, dbo.wqmel.aenam, 
      dbo.wqmel.aedat, dbo.wqmel.qmdab, dbo.wqmel.qmzab, dbo.wqmel.qwrnum, 
      dbo.wqmel.kzloesch, dbo.wqmel.dsdat, dbo.wqmel.dstim, dbo.wqmel.dsnam, 
      dbo.wqmel.dwerks, dbo.wqmel.dtplnr, dbo.wqmel.twerks, dbo.wqmel.ttplnr, 
      dbo.wqmel.veran, dbo.wqmel.hddat, dbo.wqmel.hdtim, dbo.wqmel.hdnam, 
      dbo.wqmel.clcnt, dbo.wqmel.akcnt, dbo.wqmel.aufnr, dbo.wqmel.bydat, 
      dbo.wqmel.model, dbo.tq80_t.qmartx, dbo.wqmel.artpr, dbo.wqmel.priok, 
      dbo.wqmel.erpanr, dbo.qpct.kurztextc, dbo.qpct.version, dbo.qpgt.kurztext, 
      dbo.wiflotx.pltxt
FROM dbo.wqmel INNER JOIN
      dbo.tq80_t ON dbo.wqmel.mandt = dbo.tq80_t.mandt AND 
      dbo.wqmel.qmart = dbo.tq80_t.qmart INNER JOIN
      dbo.qpct ON dbo.wqmel.mandt = dbo.qpct.mandt AND 
      dbo.wqmel.qmkat = dbo.qpct.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpct.codegruppe AND dbo.wqmel.qmcod = dbo.qpct.code AND 
      dbo.tq80_t.spras = dbo.qpct.spras INNER JOIN
      dbo.qpgt ON dbo.qpct.spras = dbo.qpgt.spras AND 
      dbo.qpct.mandt = dbo.qpgt.mandt AND dbo.qpct.katalogart = dbo.qpgt.katalogart AND 
      dbo.qpct.codegruppe = dbo.qpgt.codegruppe INNER JOIN
      dbo.wiflotx ON dbo.wqmel.mandt = dbo.wiflotx.mandt AND 
      dbo.wqmel.etplnr = dbo.wiflotx.tplnr AND dbo.tq80_t.spras = dbo.wiflotx.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_a
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.tq80_t.spras, 
      dbo.tq80_t.qmartx, dbo.wqmel.qmsta, dbo.tw80.statx, dbo.wqmel.ernam, 
      dbo.wqmel.erdat, dbo.wqmel.ewerks, dbo.wqmel.etplnr, dbo.wqmel.erpanr, 
      dbo.wqmel.dsdat, dbo.wqmel.dstim, dbo.wqmel.dsnam, dbo.wqmel.dtplnr, 
      dbo.wqmel.aenam, dbo.wqmel.aedat, dbo.wqmel.artpr, dbo.wqmel.priok, 
      dbo.wqmel.ausvn, dbo.wqmel.auztv, dbo.wqmel.mzeit, dbo.wqmel.qmdat, 
      dbo.wqmel.strmn, dbo.wqmel.strur, dbo.wqmel.ltrmn, dbo.wqmel.ltrur, 
      dbo.wqmel.serialnr, dbo.wqmel.linkman, dbo.wqmel.ltel1, dbo.wqmel.ltel2, 
      dbo.wqmel.laddr, dbo.wqmel.qmkat, dbo.wqmel.qmgrp, dbo.wqmel.qmcod, 
      dbo.wqmel.fekat, dbo.wqmel.fegrp, dbo.wqmel.fecod, dbo.wqmel.kunum, 
      dbo.wqmel.qmtxt, dbo.wqmel.qmdab, dbo.wqmel.qmzab, dbo.wqmel.qwrnum, 
      dbo.wqmel.kzloesch, dbo.wqmel.hddat, dbo.wqmel.hdtim, dbo.wqmel.hdnam, 
      dbo.wqmel.ausbs, dbo.wqmel.auztb, dbo.wqmel.veran, dbo.wqmel.ertim, 
      dbo.wqmel.dwerks, dbo.qpgt.kurztext AS qmgrpx, dbo.qpct.kurztextc AS qmcodx, 
      qpgt_1.kurztext AS fegrpx, qpct_1.kurztextc AS fecodx, qpct_1.version, 
      dbo.wqmel.bydat, dbo.wqmel.model, dbo.wqmel.twerks, dbo.wqmel.ttplnr
FROM dbo.tq80_t INNER JOIN
      dbo.tq80 ON dbo.tq80_t.mandt = dbo.tq80.mandt AND 
      dbo.tq80_t.qmart = dbo.tq80.qmart INNER JOIN
      dbo.wqmel ON dbo.tq80.mandt = dbo.wqmel.mandt AND 
      dbo.tq80.qmart = dbo.wqmel.qmart INNER JOIN
      dbo.tw80 ON dbo.wqmel.mandt = dbo.tw80.mandt AND 
      dbo.wqmel.qmsta = dbo.tw80.qmsta INNER JOIN
      dbo.qpgt ON dbo.wqmel.mandt = dbo.qpgt.mandt AND 
      dbo.wqmel.qmkat = dbo.qpgt.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpgt.codegruppe AND 
      dbo.tq80_t.spras = dbo.qpgt.spras INNER JOIN
      dbo.qpct ON dbo.wqmel.mandt = dbo.qpct.mandt AND 
      dbo.wqmel.qmkat = dbo.qpct.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpct.codegruppe AND dbo.wqmel.qmcod = dbo.qpct.code AND 
      dbo.tq80_t.spras = dbo.qpct.spras INNER JOIN
      dbo.qpgt qpgt_1 ON dbo.tq80_t.spras = qpgt_1.spras AND 
      dbo.wqmel.mandt = qpgt_1.mandt AND dbo.wqmel.fekat = qpgt_1.katalogart AND 
      dbo.wqmel.fegrp = qpgt_1.codegruppe INNER JOIN
      dbo.qpct qpct_1 ON dbo.tq80_t.spras = qpct_1.spras AND 
      dbo.qpct.version = qpct_1.version AND dbo.wqmel.mandt = qpct_1.mandt AND 
      dbo.wqmel.fekat = qpct_1.katalogart AND dbo.wqmel.fegrp = qpct_1.codegruppe AND 
      dbo.wqmel.fecod = qpct_1.code

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_ack
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.wqmel.qmsta, 
      dbo.tw80.statx, dbo.tq80.ackca, dbo.tw80.ifack
FROM dbo.wqmel INNER JOIN
      dbo.tw80 ON dbo.wqmel.mandt = dbo.tw80.mandt AND 
      dbo.wqmel.qmsta = dbo.tw80.qmsta INNER JOIN
      dbo.tq80 ON dbo.wqmel.mandt = dbo.tq80.mandt AND 
      dbo.wqmel.qmart = dbo.tq80.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_ackv
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.tq80_t.spras, 
      dbo.tq80_t.qmartx, dbo.c001l.rpatx, dbo.wqmel.qmsta, dbo.tw80.statx, 
      dbo.wqmel.ernam, dbo.wqmel.erdat, dbo.wqmel.ewerks, dbo.wqmel.etplnr, 
      dbo.wqmel.erpanr, dbo.wqmel.dsdat, dbo.wqmel.dstim, dbo.wqmel.dsnam, 
      dbo.wqmel.dwerks, dbo.wqmel.dtplnr, dbo.wqmel.artpr, dbo.wqmel.priok, 
      dbo.wqmel.ausvn, dbo.wqmel.auztv, dbo.wqmel.mzeit, dbo.wqmel.qmdat, 
      dbo.wqmel.strmn, dbo.wqmel.strur, dbo.wqmel.serialnr, dbo.wqmel.linkman, 
      dbo.wqmel.ltel1, dbo.wqmel.ltel2, dbo.wqmel.laddr, dbo.wqmel.qmkat, 
      dbo.wqmel.qmgrp, dbo.wqmel.qmcod, dbo.wqmel.fekat, dbo.wqmel.fegrp, 
      dbo.wqmel.fecod, dbo.wqmel.kunum, dbo.wqmel.qmtxt, dbo.wqmel.kzloesch, 
      dbo.wqmel.ltrur, dbo.wqmel.ltrmn, dbo.qpgt.kurztext AS qmgrpx, 
      dbo.qpct.kurztextc AS qmcodx, dbo.qpct.version, qpct_1.kurztextc AS fecodx, 
      qpgt_1.kurztext AS fegrpx, dbo.wqmel.veran, dbo.wqmel.ertim, dbo.tw80.ifack, 
      dbo.wqmel.twerks, dbo.wqmel.ttplnr
FROM dbo.tw80 INNER JOIN
      dbo.wqmel ON dbo.tw80.mandt = dbo.wqmel.mandt AND 
      dbo.tw80.qmsta = dbo.wqmel.qmsta INNER JOIN
      dbo.qpgt ON dbo.wqmel.qmgrp = dbo.qpgt.codegruppe AND 
      dbo.wqmel.qmkat = dbo.qpgt.katalogart AND 
      dbo.wqmel.mandt = dbo.qpgt.mandt INNER JOIN
      dbo.tq80_t ON dbo.wqmel.mandt = dbo.tq80_t.mandt AND 
      dbo.wqmel.qmart = dbo.tq80_t.qmart AND 
      dbo.qpgt.spras = dbo.tq80_t.spras INNER JOIN
      dbo.qpct ON dbo.wqmel.mandt = dbo.qpct.mandt AND 
      dbo.wqmel.qmkat = dbo.qpct.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpct.codegruppe AND dbo.wqmel.qmcod = dbo.qpct.code AND 
      dbo.tq80_t.spras = dbo.qpct.spras INNER JOIN
      dbo.qpct qpct_1 ON dbo.wqmel.fekat = qpct_1.katalogart AND 
      dbo.wqmel.mandt = qpct_1.mandt AND dbo.wqmel.fegrp = qpct_1.codegruppe AND 
      dbo.wqmel.fecod = qpct_1.code AND dbo.tq80_t.spras = qpct_1.spras AND 
      dbo.qpct.version = qpct_1.version INNER JOIN
      dbo.qpgt qpgt_1 ON dbo.wqmel.mandt = qpgt_1.mandt AND 
      dbo.wqmel.fekat = qpgt_1.katalogart AND dbo.wqmel.fegrp = qpgt_1.codegruppe AND 
      dbo.tq80_t.spras = qpgt_1.spras INNER JOIN
      dbo.c001l ON dbo.wqmel.mandt = dbo.c001l.mandt AND 
      dbo.wqmel.ewerks = dbo.c001l.werks AND dbo.wqmel.erpanr = dbo.c001l.rpanr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_cnl
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.wqmel.qmsta, 
      dbo.tw80.statx, dbo.tw80.ifcan, dbo.tq80.canca
FROM dbo.wqmel INNER JOIN
      dbo.tw80 ON dbo.wqmel.mandt = dbo.tw80.mandt AND 
      dbo.wqmel.qmsta = dbo.tw80.qmsta INNER JOIN
      dbo.tq80 ON dbo.wqmel.mandt = dbo.tq80.mandt AND 
      dbo.wqmel.qmart = dbo.tq80.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_dsp
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.tq80_t.spras, 
      dbo.tq80_t.qmartx, dbo.c001l.rpatx, dbo.wqmel.qmsta, dbo.tw80.statx, dbo.tw80.ifasn, 
      dbo.wqmel.ernam, dbo.wqmel.erdat, dbo.wqmel.ewerks, dbo.wqmel.etplnr, 
      dbo.wqmel.erpanr, dbo.wqmel.dsdat, dbo.wqmel.dstim, dbo.wqmel.dsnam, 
      dbo.wqmel.dwerks, dbo.wqmel.dtplnr, dbo.wqmel.artpr, dbo.wqmel.priok, 
      dbo.wqmel.ausvn, dbo.wqmel.auztv, dbo.wqmel.mzeit, dbo.wqmel.qmdat, 
      dbo.wqmel.strmn, dbo.wqmel.strur, dbo.wqmel.serialnr, dbo.wqmel.linkman, 
      dbo.wqmel.ltel1, dbo.wqmel.ltel2, dbo.wqmel.laddr, dbo.wqmel.qmkat, 
      dbo.wqmel.qmgrp, dbo.wqmel.qmcod, dbo.wqmel.fekat, dbo.wqmel.fegrp, 
      dbo.wqmel.fecod, dbo.wqmel.kunum, dbo.wqmel.qmtxt, dbo.wqmel.kzloesch, 
      dbo.wqmel.ltrur, dbo.wqmel.ltrmn, dbo.qpgt.kurztext AS qmgrpx, 
      dbo.qpct.kurztextc AS qmcodx, dbo.qpct.version, qpct_1.kurztextc AS fecodx, 
      qpgt_1.kurztext AS fegrpx, dbo.wqmel.veran, dbo.wqmel.ertim, dbo.wqmel.bydat, 
      dbo.wqmel.model, dbo.wqmel.twerks, dbo.wqmel.ttplnr
FROM dbo.tw80 INNER JOIN
      dbo.wqmel ON dbo.tw80.mandt = dbo.wqmel.mandt AND 
      dbo.tw80.qmsta = dbo.wqmel.qmsta INNER JOIN
      dbo.qpgt ON dbo.wqmel.qmgrp = dbo.qpgt.codegruppe AND 
      dbo.wqmel.qmkat = dbo.qpgt.katalogart AND 
      dbo.wqmel.mandt = dbo.qpgt.mandt INNER JOIN
      dbo.tq80_t ON dbo.wqmel.mandt = dbo.tq80_t.mandt AND 
      dbo.wqmel.qmart = dbo.tq80_t.qmart AND 
      dbo.qpgt.spras = dbo.tq80_t.spras INNER JOIN
      dbo.qpct ON dbo.wqmel.mandt = dbo.qpct.mandt AND 
      dbo.wqmel.qmkat = dbo.qpct.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpct.codegruppe AND dbo.wqmel.qmcod = dbo.qpct.code AND 
      dbo.tq80_t.spras = dbo.qpct.spras INNER JOIN
      dbo.qpct qpct_1 ON dbo.wqmel.fekat = qpct_1.katalogart AND 
      dbo.wqmel.mandt = qpct_1.mandt AND dbo.wqmel.fegrp = qpct_1.codegruppe AND 
      dbo.wqmel.fecod = qpct_1.code AND dbo.tq80_t.spras = qpct_1.spras AND 
      dbo.qpct.version = qpct_1.version INNER JOIN
      dbo.qpgt qpgt_1 ON dbo.wqmel.mandt = qpgt_1.mandt AND 
      dbo.wqmel.fekat = qpgt_1.katalogart AND dbo.wqmel.fegrp = qpgt_1.codegruppe AND 
      dbo.tq80_t.spras = qpgt_1.spras INNER JOIN
      dbo.c001l ON dbo.wqmel.mandt = dbo.c001l.mandt AND 
      dbo.wqmel.ewerks = dbo.c001l.werks AND dbo.wqmel.erpanr = dbo.c001l.rpanr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_hd
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.wqmel.qmsta, 
      dbo.tq80.sthdn, dbo.tw80.ifhdn, dbo.tw80.statx
FROM dbo.wqmel INNER JOIN
      dbo.tq80 ON dbo.wqmel.mandt = dbo.tq80.mandt AND 
      dbo.wqmel.qmart = dbo.tq80.qmart INNER JOIN
      dbo.tw80 ON dbo.wqmel.mandt = dbo.tw80.mandt AND 
      dbo.wqmel.qmsta = dbo.tw80.qmsta

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_hdv
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmart, dbo.tq80_t.spras, 
      dbo.tq80_t.qmartx, dbo.c001l.rpatx, dbo.wqmel.qmsta, dbo.tw80.statx, 
      dbo.wqmel.ernam, dbo.wqmel.erdat, dbo.wqmel.ewerks, dbo.wqmel.etplnr, 
      dbo.wqmel.erpanr, dbo.wqmel.dsdat, dbo.wqmel.dstim, dbo.wqmel.dsnam, 
      dbo.wqmel.dwerks, dbo.wqmel.dtplnr, dbo.wqmel.artpr, dbo.wqmel.priok, 
      dbo.wqmel.ausvn, dbo.wqmel.auztv, dbo.wqmel.mzeit, dbo.wqmel.qmdat, 
      dbo.wqmel.strmn, dbo.wqmel.strur, dbo.wqmel.serialnr, dbo.wqmel.linkman, 
      dbo.wqmel.ltel1, dbo.wqmel.ltel2, dbo.wqmel.laddr, dbo.wqmel.qmkat, 
      dbo.wqmel.qmgrp, dbo.wqmel.qmcod, dbo.wqmel.fekat, dbo.wqmel.fegrp, 
      dbo.wqmel.fecod, dbo.wqmel.kunum, dbo.wqmel.qmtxt, dbo.wqmel.kzloesch, 
      dbo.wqmel.ltrur, dbo.wqmel.ltrmn, dbo.qpgt.kurztext AS qmgrpx, 
      dbo.qpct.kurztextc AS qmcodx, dbo.qpct.version, qpct_1.kurztextc AS fecodx, 
      qpgt_1.kurztext AS fegrpx, dbo.wqmel.veran, dbo.wqmel.ertim, dbo.tw80.ifhdn, 
      dbo.wqmel.twerks, dbo.wqmel.ttplnr
FROM dbo.tw80 INNER JOIN
      dbo.wqmel ON dbo.tw80.mandt = dbo.wqmel.mandt AND 
      dbo.tw80.qmsta = dbo.wqmel.qmsta INNER JOIN
      dbo.qpgt ON dbo.wqmel.qmgrp = dbo.qpgt.codegruppe AND 
      dbo.wqmel.qmkat = dbo.qpgt.katalogart AND 
      dbo.wqmel.mandt = dbo.qpgt.mandt INNER JOIN
      dbo.tq80_t ON dbo.wqmel.mandt = dbo.tq80_t.mandt AND 
      dbo.wqmel.qmart = dbo.tq80_t.qmart AND 
      dbo.qpgt.spras = dbo.tq80_t.spras INNER JOIN
      dbo.qpct ON dbo.wqmel.mandt = dbo.qpct.mandt AND 
      dbo.wqmel.qmkat = dbo.qpct.katalogart AND 
      dbo.wqmel.qmgrp = dbo.qpct.codegruppe AND dbo.wqmel.qmcod = dbo.qpct.code AND 
      dbo.tq80_t.spras = dbo.qpct.spras INNER JOIN
      dbo.qpct qpct_1 ON dbo.wqmel.fekat = qpct_1.katalogart AND 
      dbo.wqmel.mandt = qpct_1.mandt AND dbo.wqmel.fegrp = qpct_1.codegruppe AND 
      dbo.wqmel.fecod = qpct_1.code AND dbo.tq80_t.spras = qpct_1.spras AND 
      dbo.qpct.version = qpct_1.version INNER JOIN
      dbo.qpgt qpgt_1 ON dbo.wqmel.mandt = qpgt_1.mandt AND 
      dbo.wqmel.fekat = qpgt_1.katalogart AND dbo.wqmel.fegrp = qpgt_1.codegruppe AND 
      dbo.tq80_t.spras = qpgt_1.spras INNER JOIN
      dbo.c001l ON dbo.wqmel.mandt = dbo.c001l.mandt AND 
      dbo.wqmel.ewerks = dbo.c001l.werks AND dbo.wqmel.erpanr = dbo.c001l.rpanr

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmel_sta
AS
SELECT dbo.wqmel.mandt, dbo.wqmel.qmnum, dbo.wqmel.qmsta, dbo.tw80.statx, 
      dbo.tw80.ifasn, dbo.tw80.ifack, dbo.tw80.ifhdn, dbo.tw80.ifacu, dbo.tw80.ifcan, 
      dbo.tw80.ifedt, dbo.wqmel.ewerks, dbo.wqmel.etplnr, dbo.tq80.stdsp, 
      dbo.wqmel.qmart
FROM dbo.tw80 INNER JOIN
      dbo.wqmel ON dbo.tw80.mandt = dbo.wqmel.mandt AND 
      dbo.tw80.qmsta = dbo.wqmel.qmsta INNER JOIN
      dbo.tq80 ON dbo.wqmel.mandt = dbo.tq80.mandt AND 
      dbo.wqmel.qmart = dbo.tq80.qmart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wqmfe
AS
SELECT dbo.wqmfe.mandt, dbo.qpct.spras, dbo.qpct.version, dbo.wqmfe.qmnum, 
      dbo.wqmfe.fenum, dbo.wqmfe.fetxt, dbo.wqmfe.fekat, dbo.wqmfe.fegrp, 
      dbo.wqmfe.fecod, dbo.qpct.kurztextc, dbo.qpgt.kurztext
FROM dbo.wqmfe INNER JOIN
      dbo.qpct ON dbo.wqmfe.mandt = dbo.qpct.mandt AND 
      dbo.wqmfe.fekat = dbo.qpct.katalogart AND 
      dbo.wqmfe.fegrp = dbo.qpct.codegruppe AND 
      dbo.wqmfe.fecod = dbo.qpct.code INNER JOIN
      dbo.qpgt ON dbo.qpct.mandt = dbo.qpgt.mandt AND 
      dbo.qpct.katalogart = dbo.qpgt.katalogart AND 
      dbo.qpct.codegruppe = dbo.qpgt.codegruppe AND dbo.qpct.spras = dbo.qpgt.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_wt158v
AS
SELECT dbo.wt158v.mandt, dbo.wt158v.vgart, dbo.t158w.spras, dbo.t158w.ltext, 
      dbo.wt158v.nrrangenr
FROM dbo.t158v INNER JOIN
      dbo.t158w ON dbo.t158v.vgart = dbo.t158w.vgart INNER JOIN
      dbo.wt158v ON dbo.t158v.vgart = dbo.wt158v.vgart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvbvk
AS
SELECT dbo.tvbvk.mandt, dbo.tvbvk.vkbur, dbo.tvbvk.vkgrp, dbo.v_tvkgr.spras, 
      dbo.v_tvkgr.vkgrpx, dbo.v_tvbur.vkburx
FROM dbo.tvbvk INNER JOIN
      dbo.v_tvbur ON dbo.tvbvk.mandt = dbo.v_tvbur.mandt AND 
      dbo.tvbvk.vkbur = dbo.v_tvbur.vkbur INNER JOIN
      dbo.v_tvkgr ON dbo.tvbvk.mandt = dbo.v_tvkgr.mandt AND 
      dbo.v_tvbur.spras = dbo.v_tvkgr.spras AND dbo.tvbvk.vkgrp = dbo.v_tvkgr.vkgrp

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkbz
AS
SELECT dbo.tvkbz.mandt, dbo.tvkbz.vkorg, dbo.tvkbz.vtweg, dbo.tvkbz.spart, 
      dbo.tvkbz.vkbur, dbo.v_tvbur.spras, dbo.v_tvbur.vkburx, dbo.v_tvbur.dlflg
FROM dbo.tvkbz INNER JOIN
      dbo.v_tvbur ON dbo.tvkbz.mandt = dbo.v_tvbur.mandt AND 
      dbo.tvkbz.vkbur = dbo.v_tvbur.vkbur

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkos
AS
SELECT dbo.tvkos.mandt, dbo.tvkos.spart, dbo.v_tspa.spartx, dbo.v_tspa.spras, 
      dbo.tvkos.vkorg, dbo.v_tspa.dlflg
FROM dbo.tvkos INNER JOIN
      dbo.v_tspa ON dbo.tvkos.mandt = dbo.v_tspa.mandt AND 
      dbo.tvkos.spart = dbo.v_tspa.spart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvkov
AS
SELECT dbo.tvkov.mandt, dbo.tvkov.vkorg, dbo.tvkov.vtweg, dbo.v_tvtw.spras, 
      dbo.v_tvtw.dlflg, dbo.v_tvko.vkorgx, dbo.v_tvtw.vtwegx
FROM dbo.tvkov INNER JOIN
      dbo.v_tvtw ON dbo.tvkov.mandt = dbo.v_tvtw.mandt AND 
      dbo.tvkov.vtweg = dbo.v_tvtw.vtweg INNER JOIN
      dbo.v_tvko ON dbo.tvkov.mandt = dbo.v_tvko.mandt AND 
      dbo.tvkov.vkorg = dbo.v_tvko.vkorg AND dbo.v_tvtw.spras = dbo.v_tvko.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvta
AS
SELECT dbo.tvta.mandt, dbo.tvta.vkorg, dbo.tvta.vtweg, dbo.tvta.spart, dbo.v_tspa.dlflg, 
      dbo.v_tspa.spras, dbo.v_tspa.spartx
FROM dbo.tvta INNER JOIN
      dbo.v_tspa ON dbo.tvta.mandt = dbo.v_tspa.mandt AND 
      dbo.tvta.spart = dbo.v_tspa.spart

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_tvta_a
AS
SELECT dbo.tvta.mandt, dbo.tvta.vkorg, dbo.tvta.vtweg, dbo.tvta.spart, dbo.v_tspa.spartx, 
      dbo.v_tvko.vkorgx, dbo.v_tvtw.vtwegx, dbo.v_tspa.dlflg, dbo.v_tspa.spras
FROM dbo.tvta INNER JOIN
      dbo.v_tspa ON dbo.tvta.mandt = dbo.v_tspa.mandt AND 
      dbo.tvta.spart = dbo.v_tspa.spart INNER JOIN
      dbo.v_tvko ON dbo.tvta.mandt = dbo.v_tvko.mandt AND 
      dbo.tvta.vkorg = dbo.v_tvko.vkorg AND 
      dbo.v_tspa.spras = dbo.v_tvko.spras INNER JOIN
      dbo.v_tvtw ON dbo.tvta.mandt = dbo.v_tvtw.mandt AND 
      dbo.tvta.vtweg = dbo.v_tvtw.vtweg AND dbo.v_tspa.spras = dbo.v_tvtw.spras

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

