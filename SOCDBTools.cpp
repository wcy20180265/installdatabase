// SOCDBTools.cpp: implementation of the CSOCDBTools class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "installdatabase.h"
#include "SOCDBTools.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSOCDBTools::CSOCDBTools()
{

}

CSOCDBTools::~CSOCDBTools()
{

}



CString CSOCDBTools::GetErrorString()
{
	return m_strError;
}
#include "odbcinst.h"
#pragma comment(lib,"odbccp32")
//在控制面板里创建一个ODBC
BOOL CSOCDBTools::AddDSN(CString sDsn, CString sServer, CString sDatabase)
{
	char szDesc[1024];
	int mlen;
	sprintf(szDesc,"DSN=%s? SERVER=%s? DATABASE=%s??",sDsn,sServer,sDatabase);
	mlen = strlen(szDesc);
	for(int i=0; i<mlen; i++)
	{
		if (szDesc[i] == '?')
			szDesc[i] = '\0';
	}
	return SQLConfigDataSource(NULL,ODBC_ADD_DSN, "SQL Server",(LPCSTR)(LPCTSTR)szDesc);

}
