// SQLFileParser.cpp: implementation of the CSQLFileParser class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "installdatabase.h"
#include "SQLFileParser.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSQLFileParser::CSQLFileParser()
{

}

CSQLFileParser::~CSQLFileParser()
{

}

BOOL CSQLFileParser::ParseFileToArray(CString strFile, CStringArray *pArray)
{
	CStdioFile file;
	CString line,tmp;
	CString sql;
	
	CFileException fe;
	try
	{
		if(FALSE == file.Open(strFile,CFile::modeRead,&fe/**/) )
		{
			TCHAR err[512];
			//CString err;
			fe.GetErrorMessage(err,512);
			m_strError=err;
			
			return FALSE;
		}
	}
	catch (CFileException* e)
	{
		m_strError="file open cause CFileException!";
		e->Delete();
		return FALSE;
		
	}
	
	//char buf[1024];
	CString buf;
	try
	{
//		while(file.ReadString(line))
		while(file.ReadString(buf/*,1024*/))
		{
			line=buf;
			tmp=line;
			tmp.TrimLeft();
			tmp.TrimRight();
			int len;
			len=tmp.GetLength();

			if(tmp.CompareNoCase(_T("go"))==0)
			{
				
				pArray->Add(sql);
				sql=_T("");
			}
			else
			{
				sql+=line;
				sql+=_T(" ");
			}
			
		}
	}
	catch(CFileException* e)
	{
		e->ReportError();
		e->Delete();
	}
	sql.TrimLeft();
	sql.TrimRight();
	if(!sql.IsEmpty())
		pArray->Add(sql);
	
	file.Close();
	
	return TRUE;
}

CString CSQLFileParser::GetErrorString()
{
	return m_strError;
}
