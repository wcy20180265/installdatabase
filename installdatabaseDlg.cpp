// installdatabaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "installdatabase.h"
#include "installdatabaseDlg.h"
#include "MyRegKey.h"
#include "SQLFileParser.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
#define REGKEYSERVER "SOFTWARE\\SOCSoft\\SOCServer"

static CString MSDESERVER = "soc";    //MSDE在控制面板系统中注册的服务名称 
static CString SERVERNAME = ".\\soc";  //数据源通过该服务器连接数据库
static CString DBNAME = "socdb";  //要创建的数据库名称,如果名称改变，则要相应地改变createdb.sql中的名称
static CString DBSA = "sa";       //登陆数据库的账户
static CString SAPWD = "20031015";  //登录数据库的密码
static CString DBDSN = "soc";     //要创建的数据源名，服务器通过该数据源连接数据库
static CString PROGRAMFILES="C:\\Program Files";


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInstalldatabaseDlg dialog

CInstalldatabaseDlg::CInstalldatabaseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInstalldatabaseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInstalldatabaseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pThread=NULL;
}

void CInstalldatabaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInstalldatabaseDlg)
	DDX_Control(pDX, IDC_STATIC1, m_static1);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CInstalldatabaseDlg, CDialog)
	//{{AFX_MSG_MAP(CInstalldatabaseDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_USER_THREAD_PROGRESS,OnThreadProgress)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInstalldatabaseDlg message handlers

BOOL CInstalldatabaseDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	

	if(::AfxMessageBox("将安装数据库，继续吗？",MB_YESNO)!=IDYES)
	{
		CDialog::OnCancel();
		return FALSE;
	}

	m_progress1.SetRange(0,100);




	CMyRegKey key;
	CString dsn,uid,pwd,strInstallPath;
	//////////////////////////////////////////////////////////////////////////
	key.RegOpen(HKEY_LOCAL_MACHINE,_T(REGKEYSERVER));
	key.RegRead("sDsn",dsn);
	key.RegRead("sUser",uid);
	key.RegRead("sPwd",pwd);
	key.RegRead("sInstallPath",strInstallPath);
	key.RegClose();



	dsn="master";
	uid=DBSA;
	pwd=SAPWD;
	
	//先创建一个指向MASTER数据的数据源，以便在里面创建数据库
	if(!m_dsn.AddDSNToControlPanel(dsn,SERVERNAME,"master"))
	{
		::AfxMessageBox(m_dsn.GetErrorString());
		CDialog::OnCancel();
		return FALSE;
	}
	if(!m_dsn.OpenDSN(dsn,uid,pwd))
	{
		CDialog::OnCancel();
		return FALSE;
	}
	// TODO: Add extra initialization here
	static INSDBPARAM param;
	param.strInstallPath=strInstallPath;
	param.pDSN=&m_dsn;
	param.hWndDlg=this->GetSafeHwnd();

	m_pThread=::AfxBeginThread(CInstalldatabaseDlg::CreateDatabaseThread,&param);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CInstalldatabaseDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CInstalldatabaseDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CInstalldatabaseDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}





void CInstalldatabaseDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here

}

UINT CInstalldatabaseDlg::CreateDatabaseThread(LPVOID pParam)
{
	CSQLFileParser parser;

	CString  sFile;
	INSDBPARAM* pPam=(INSDBPARAM*)pParam;
	INSCONTEXT ctx;
	INSMSGSTRU wpm;

	ctx.hWndDlg=pPam->hWndDlg;
	CSQLFilesInstaller ins(&ctx);


	sFile=pPam->strInstallPath+"\\Database\\log.txt";
	CStdioFile file;
	file.Open(sFile,CFile::modeCreate);
	file.Close();
	ins.SetLogFile(sFile);

///////创建数据库，支持多个SQL文件
	ins.SetDatabase(pPam->pDSN->GetDatabase());
	sFile=pPam->strInstallPath+"\\Database\\createdb.sql";
	ins.AddFile(sFile);

	wpm.pos=0;
	wpm.flag=0;
	wpm.msg="正在创建数据库";
	::SendMessage(ctx.hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&wpm,NULL);

	if(!ins.Install())
	{
		::SendMessage(pPam->hWndDlg,WM_CLOSE,0,0);
		return 1;
	}
////////////创建表格、视图
	pPam->pDSN->AddDSNToControlPanel(DBDSN,SERVERNAME,DBNAME);
	pPam->pDSN->OpenDSN(DBDSN,DBSA,SAPWD);

	//创建表格
	wpm.pos=0;
	wpm.flag=0;
	wpm.msg="正在创建数据库表格";
	::SendMessage(ctx.hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&wpm,NULL);

	ins.RemoveAllFiles();
	sFile=pPam->strInstallPath+"\\Database\\createtable.sql";
	ins.AddFile(sFile);
	if(!ins.Install())
	{
		::SendMessage(pPam->hWndDlg,WM_CLOSE,0,0);
		return 1;
	}
	//创建视图
	wpm.pos=0;
	wpm.flag=0;
	wpm.msg="正在创建数据库视图";
	::SendMessage(ctx.hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&wpm,NULL);

	ins.RemoveAllFiles();
	sFile=pPam->strInstallPath+"\\Database\\createview.sql";
	ins.AddFile(sFile);
	if(!ins.Install())
	{
		::SendMessage(pPam->hWndDlg,WM_CLOSE,0,0);
		return 1;
	}
	////////////导入数据
	wpm.pos=0;
	wpm.flag=0;
	wpm.msg="正在导入数据";
	::SendMessage(ctx.hWndDlg,WM_USER_THREAD_PROGRESS,(WPARAM)&wpm,NULL);

	ins.RemoveAllFiles();
	sFile=pPam->strInstallPath+"\\Database\\datasql.sql";
	ins.AddFile(sFile);

	pPam->pDSN->GetDatabase()->BeginTrans();
	if(!ins.Install())
	{
		pPam->pDSN->GetDatabase()->Rollback();
		::SendMessage(pPam->hWndDlg,WM_CLOSE,0,0);
		return 1;
	}
	pPam->pDSN->GetDatabase()->CommitTrans();

/**/
	//把createdb.sql文件改名，以防用户误操作删除数据库，应为该文
	//sFile=pPam->strInstallPath+"\\Database\\createdb.sql";

	::SendMessage(pPam->hWndDlg,WM_CLOSE,0,0);
	return 0;

}


LRESULT CInstalldatabaseDlg::OnThreadProgress(WPARAM wParam, LPARAM lParam)
{

	INSMSGSTRU* p=(INSMSGSTRU*)wParam;

	if(p->flag==0)
	{
		m_static1.SetWindowText(p->msg);
	}
	else if(p->flag==1)
	{
		m_progress1.SetRange32(p->low,p->high);
	}
	else if(p->flag==2)
	{
		m_progress1.SetPos(p->pos);
	}

	return 1;
}

int CInstalldatabaseDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	ModifyStyleEx(WS_EX_APPWINDOW,WS_EX_TOOLWINDOW);
	return 0;
}
