// installdatabase.h : main header file for the INSTALLDATABASE application
//

#if !defined(AFX_INSTALLDATABASE_H__41F98740_4861_4300_AF8A_0EC4BEF620C9__INCLUDED_)
#define AFX_INSTALLDATABASE_H__41F98740_4861_4300_AF8A_0EC4BEF620C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CInstalldatabaseApp:
// See installdatabase.cpp for the implementation of this class
//

class CInstalldatabaseApp : public CWinApp
{
public:
	CInstalldatabaseApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInstalldatabaseApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CInstalldatabaseApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTALLDATABASE_H__41F98740_4861_4300_AF8A_0EC4BEF620C9__INCLUDED_)
